const FtpDeploy = require('ftp-deploy');
const archiver = require('archiver');
const version = require('./package.json').version;
const Credentials = require('./credentials');
const creds = new Credentials();
const ftpDeploy = new FtpDeploy();
//const date = new Date().toLocaleDateString();
const uploadProject = {
  user: creds.user,
  // Password optional, prompted if none given
  password: creds.password,
  host: creds.server,
  port: 21,
  localRoot: __dirname + '/dist/golowinskiy-admin',
  remoteRoot: '',
  include: ['*', '**/*'],
  exclude: [
    'dist/**/*.map',
    'node_modules/**',
    'node_modules/**/.*',
    '.git/**',
  ],
  deleteRemote: true,
  forcePasv: true,
  sftp: false,
};

const uploadBuildVersion = {
  user: creds.user,
  password: creds.password,
  host: creds.server,
  port: 21,
  localRoot: __dirname + '/dist/',
  remoteRoot: '',
  include: [`golowinskiy-admin-${version}.zip`],
  exclude: [
    'dist/**/*.map',
    'node_modules/**',
    'node_modules/**/.*',
    '.git/**',
  ],
  deleteRemote: false,
  forcePasv: true,
  sftp: false,
};

function zipDirectory(sourceDir, outPath) {
  const fs = require('fs');
  const archive = archiver('zip', { zlib: { level: 9 } });
  const stream = fs.createWriteStream(outPath);

  return new Promise((resolve, reject) => {
    archive
      .directory(sourceDir, false)
      .on('error', (err) => reject(err))
      .pipe(stream);

    stream.on('close', () => resolve());
    archive.finalize();
  });
}

zipDirectory(
  './dist/golowinskiy-admin',
  `./dist/golowinskiy-admin-${version}.zip`
)
  .then(() => ftpDeploy.deploy(uploadProject))
  .then((r) => {
    console.log(`golowinskiy-admin v${version} deployed successfully!`);
    return r;
  })
  .then(() => ftpDeploy.deploy(uploadBuildVersion))
  .then((r) => {
    console.log(`${creds.web}/golowinskiy-admin-${version}.zip`);
    return r;
  })
  .catch((err) => console.log(err));

import { Component, OnInit, ViewChild } from '@angular/core';
import { DictionaryService } from '@app/core/services/dictionary.service';
import { EnvService } from '@app/core/services/env.service';
import { MainService } from '@app/core/services/main.service';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';

@Component({
  selector: 'app-dropzone',
  templateUrl: './dropzone.component.html',
  styleUrls: ['./dropzone.component.scss']
})
export class DropzoneComponent implements OnInit {

  constructor(
    public mainService: MainService,
    public environment: EnvService,
    public dictionary: DictionaryService
  ) { }

  @ViewChild('dropzoner', { static: true }) componentRef: any;
  public uploadedImages = [
    'https://d3lkkdthll7bhu.cloudfront.net/19139/images/Screenshot 2021-10-11 at 21.18.31.png',
    'https://d3lkkdthll7bhu.cloudfront.net/19139/images/Screenshot 2021-11-15 at 10.30.22.png',
    'https://d3lkkdthll7bhu.cloudfront.net/19139/images/eng_flag.png',
    'https://d3lkkdthll7bhu.cloudfront.net/19139/images/mikael-gustafsson-amongtrees-2-8.jpg'
  ];
  ngOnInit(): void {
  }

  public config: DropzoneConfigInterface = {
    // Change this to your upload POST address:
     url: 'https://api.xn--e1arkeckp8bt.xn--p1ai/api/FileS3/UploadImage?shopId=19139',
     maxFilesize: 50,
     maxFiles: 9,
     acceptedFiles: 'image/*',
     addRemoveLinks: true,
     dictRemoveFile: 'Удалить',
     dictMaxFilesExceeded: 'Превышен лимит загружаемых картинок!',
   }


  onUploadError($event) {
    alert($event[1]);
  }

  onRemovedFile($event){
    const index = this.uploadedImages.findIndex((link)=> link === $event.link);
    this.uploadedImages.splice(index,1);
  }

  maxfilesreached($event){

  }

  onUploadSuccess($event) {

   this.onclick()
   const {success, data:link} = $event[1];
   if(success){
     this.uploadedImages.push(link)

   }

  }

  test(e){

  }

  onclick(){
    const items= [...this.componentRef.directiveRef.elementRef.nativeElement.childNodes];
    function  handler(){

    }
    items.forEach((el)=> el.removeEventListener('click',handler))
    items.forEach((el)=>{
      el.addEventListener("click",handler)
    })
  }

  ngAfterViewInit(): void {

    this.componentRef.directiveRef.DZ_DRAGEND.subscribe((r)=>{

    })
    this.componentRef.directiveRef.DZ_DRAGSTART.subscribe((r)=>{

    })
    this.componentRef.directiveRef.DZ_DROP.subscribe((r)=>{

    })
    const dropzone = this.componentRef.directiveRef.dropzone();
    this.uploadedImages.forEach((img,i)=>{
      const mockFile = { name: i+1, size: "0", link: img };
      dropzone.emit( "addedfile", mockFile );
      dropzone.emit( "thumbnail", mockFile, img );
      dropzone.emit( "complete", mockFile);
    })
    setTimeout(()=> this.onclick(),1000)
  }

}

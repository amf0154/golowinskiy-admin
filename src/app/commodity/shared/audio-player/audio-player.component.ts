import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { DictionaryService } from '@app/core/services/dictionary.service';
import {Howl, Howler} from 'howler';

@Component({
  selector: 'audio-player',
  templateUrl: './audio-player.component.html',
  styleUrls: ['./audio-player.component.scss']
})
export class AudioPlayerComponent implements OnInit, OnDestroy {
  @Output() getRecord = new EventEmitter<Blob>();
  @Input() file: any = null;
  @Input() autoPlay: any = false;
  @Input() public short: any = false;
  public vocal_playing: boolean = false;
  public currentAudio: any = null;
  public isRepeat: boolean = true;
  public errorLoading: boolean = false;
  private timeout;
  constructor(
    public dictionary: DictionaryService
  ) {
    this.errorLoading = false;
  }

  ngOnInit() {
  //  this.vocal_playing = this.isRepeat = true;
     this.vocal_playing = this.autoPlay ? true : false;
    this.isRepeat = this.autoPlay;
    this.currentAudio = new Howl({
      src: [this.file],
      ext: ['ogg'],
      autoplay: this.autoPlay,
      loop: false,
      preload: true,
      html5: true,
      onloaderror: (msg) => {
      //  this.errorLoading = true;
      },
 //     onload: ()=> log(this.currentAudio._duration),
      onend: () => {
        if(this.isRepeat && this.autoPlay){
          this.repeat();
        }else{
          this.vocal_playing = false;
        }
      }
    });
    this.currentAudio.once('loaderror', () => {
      Howler.unload();
    });

    this.currentAudio.once('playerror', () => {
      Howler.unload();
    });

  }

  public playAudio(){
    this.currentAudio.play();
    this.vocal_playing = true;
  }
  public stopAudio(){
    this.currentAudio.stop();
    this.vocal_playing = false;
  }
  public pauseAudio(){
    this.currentAudio.pause();
    clearTimeout(this.timeout);
    this.vocal_playing = false;
  }
  public switchRepeat = () => this.isRepeat = !this.isRepeat;
  public repeat(){
   this.timeout = setTimeout(()=>this.playAudio(),3000); 
  }
  public ngOnDestroy(){
    this.isRepeat = false;
    this.stopAudio();
    clearTimeout(this.timeout);
  }
} 

import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DictionaryService } from '@app/core/services/dictionary.service';
import { MainService } from '@app/core/services/main.service';
import { SharedService } from '@app/core/services/shared.service';
import { ImageUploaderService } from '../image-uploader/image-uploader.service';
import { VoiceRecorderComponent } from '../voice-recorder/voice-recorder.component';
const Swal = require('sweetalert2');

@Component({
  selector: 'audio-uploader',
  templateUrl: './audio-uploader.component.html',
  styleUrls: ['./audio-uploader.component.scss']
})
export class AudioUploaderComponent implements OnInit {

  @Output() isAudioUploaded = new EventEmitter();
  @Input() fileInfos = [];
  @Output() deleteStack = new EventEmitter();
  @Input() isEditMode? : boolean = false;
  selectedFiles: FileList;
  progressInfos = [];
  message = '';
  indexMainImg: number  = 0;

  constructor(
    private uploadService: ImageUploaderService,
    private shared: SharedService,
    private mainService: MainService,
    public dictionary: DictionaryService,
    public dialog: MatDialog
    ) { }

  ngOnInit(): void {
  }

  selectFiles(event): void {
  //  this.progressInfos = [];
    if(event.target.files.length < (3 - this.fileInfos.length)){
      this.selectedFiles = event.target.files;
      if(this.selectedFiles.length)
      this.uploadFiles()
    }else{
      Swal.fire({
        position: 'center',
        icon: 'warning',
        title: 'Максимальное количество картинок не может быть больше 9',
        showConfirmButton: false,
        timer: 2000
      });
    }
  }

  uploadFiles(): void {
    this.message = '';
  
    for (let i = 0; i < this.selectedFiles.length; i++) {
      this.upload(i, this.selectedFiles[i]);
    }
  }

  upload(idx, file): void {
    this.isAudioUploaded.emit(false);   
    this.progressInfos[idx] = { value: 0, fileName: null };
    const imgName = this.shared.uuidv4()+'.mp3';
    this.uploadService.upload(file,imgName,2).subscribe(
      (event) => {
        if (event.type === HttpEventType.UploadProgress) {
          this.progressInfos[idx].value = Math.round(100 * event.loaded / event.total);
        } else if (event instanceof HttpResponse) {
            const {data, success} = event.body;
          if(success){

            this.fileInfos.push({
              link: data
            }); 
            this.progressInfos.splice(idx,1);  
            this.isAudioUploaded.emit(this.progressInfos.length === 0);   
          }
        }
      },
      err => {
        this.progressInfos[idx].value = 0;
        this.message = 'Could not upload the file:' + file.name;
      });

  }

  fullLink(link){
    return link.startsWith('https://') ? link : 'https://' + link;
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.fileInfos, event.previousIndex, event.currentIndex);
  }

  getMainImg(){
    if(this.indexMainImg !== null)
      return this.fileInfos[this.indexMainImg]
  }

  delete(i: number){
    const {link} = this.fileInfos[0];
    if(!this.isEditMode){
    if(link){
      Swal.queue([{
        title: 'Удаление аудиозаписи',
        confirmButtonText: 'Удалить',
        text: 'Вы действительно хотите удалить аудиозапись?',
        cancelButtonText: 'Отмена',
        showCancelButton: true,
        showLoaderOnConfirm: true,
        preConfirm: () => {
          return this.mainService.deleteCloudContent(link)  
            .subscribe((response: any) => {
                Swal.fire({
                  icon: 'success',
                  title: "Аудиозапись успешно удалена",
                  showConfirmButton: false,
                  timer: 2000,
                  willClose: () => {
                    this.fileInfos.splice(i,1);
                  }
                });
            })
        }
      }])
    }
    }else{
      const file = this.fileInfos[0];
      this.deleteStack.emit(file);
      this.fileInfos.splice(i,1);
    }
  }

  record(){
    this.dialog.open(VoiceRecorderComponent, {
      width: '250px',
      data: [],
    }).afterClosed()
    .subscribe((file)=>{
      if(file instanceof Blob){
        this.selectFiles({
          target: {
            files: [file]
          }
        })
      }
    })
  }
}


import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CommodityService } from '@app/commodity/commodity.service';
import { DictionaryService } from '@app/core/services/dictionary.service';
import { AudioImageService } from './audio-image.service';
@Component({
  selector: 'attach-audio-img',
  templateUrl: './attach-audio-to-img.component.html',
  styleUrls: ['./attach-audio-to-img.component.scss']
})
export class AttachAudioToImgComponent implements OnInit{
  constructor(
    public dialogRef: MatDialogRef<AttachAudioToImgComponent>,
    public dictionary: DictionaryService,
    private audioState: AudioImageService,
    public commodity: CommodityService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { 
    this.dialogRef.disableClose = true;
  }
  audi = [];

  ngOnInit(): void {
    this.isEditMode = this.data.isEdit;
    if (this.data.link)
      this.audi.push(this.data)
  }

  isEditMode = false;
  deleteStack = []
  isLoaded = true
  $isAudioUploaded($event){
    this.isLoaded = $event;
    if(this.isLoaded)
      setTimeout(()=> this.dialogRef.close(this.audi[0]),200);
  }

  $audioDeleteStack($event){
    if(!this.isEditMode)
      this.deleteStack.push($event)
    else{
      this.audioState.deleteStack.push($event);
    }
  }

  save(){
    this.clearStack();
    if(this.audi){
      this.dialogRef.close(this.audi[0]);
    }
  }

  close(){
    this.clearStack();
    this.dialogRef.close(this.audi[0]);
  }

  clearStack(){
    if(this.deleteStack.length && !this.isEditMode){
      this.commodity.deleteAudioStack(this.deleteStack).subscribe()
    }
  }

}

import { Injectable } from '@angular/core';
import { AdvertDetail } from '@app/core/models/advert-list';
import { AuthenticationService } from '@app/core/services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AudioImageService {

  constructor(
    public authService: AuthenticationService) { }

  initialAudioState = new Map();
  advert: AdvertDetail = null;
  deleteStack = [];

  setArticle(advert){
    this.advert = advert;
    this.setInitialAudioState(advert.additionalImages);
  }  
  
  setInitialAudioState(state){
    state.filter((a)=> a.audio).forEach((res) => {
      this.initialAudioState.set(this.removeProtocol(res.audio),res.imageOrder);
    });
  }
  
  resetData(){
    this.advert = null;
    this.initialAudioState.clear();
  }

  removeProtocol(url){
    return url.startsWith('https://') ? url.split('https://')[1] : url;
  }

  // check deleted initial images/audios and build body for delete additional audios before update with new links too audio;
  additAudioRemover(){
    const deletingAudiosImageBody = this.deleteStack
    .reduce((delAddImgs,curr) => {
      if(this.initialAudioState.has(this.removeProtocol(curr.link))){
        delAddImgs.push({
          "prc_ID": this.advert.prc_ID,
          "mediaOrder": this.initialAudioState.get(curr.link),
          "mediaLink": this.removeProtocol(curr.link),
          "appcode": this.authService.getCustId(),
          "cid": this.authService.getCustId(),
        });
        this.deleteStack.splice(this.deleteStack.indexOf(curr),1);
      }
      return delAddImgs;
    },[]);
    return {
      beforeUpd: deletingAudiosImageBody,
      deleteStack: this.deleteStack
    }
  }
}

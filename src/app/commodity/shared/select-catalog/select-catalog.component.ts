import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { CategoryItem } from '@app/core/models/category-item';
import { AuthenticationService } from '@app/core/services/authentication.service';
import { CategoriesService } from '@app/core/services/categories.service';
import { DictionaryService } from '@app/core/services/dictionary.service';

@Component({
  selector: 'select-catalog',
  templateUrl: './select-catalog.component.html',
  styleUrls: ['./select-catalog.component.scss']
})
export class SelectCatalogComponent implements OnInit {

  public selectedItem = null;
  public initialCategories: CategoryItem[] = [];
  @Output() selectedCategoryId = new EventEmitter();
  constructor(
    public categoriesService: CategoriesService,
    private authService: AuthenticationService,
    public dictionary: DictionaryService
  ) { 
    this.categoriesService.fetchCategoriesAll(this.authService.getCustId(),this.authService.getUserId(),"1");
  }

  public categories = [];
  ngOnInit(): void {
    this.categoriesService.mainCategories$.subscribe((res)=>{
      this.categories = res;
    });
  }

  reset(){
    this.selectedItem  = null;
    this.selectedCategoryId.emit(null)
  }

  select(cat: [{cust_id: string; id: string}]){
    this.selectedItem = cat[0];
    this.selectedCategoryId.emit(cat[0]);
  }

}

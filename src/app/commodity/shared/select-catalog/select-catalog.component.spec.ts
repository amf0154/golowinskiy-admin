import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SelectCatalogComponent } from './select-catalog.component';

describe('SelectCatalogComponent', () => {
  let component: SelectCatalogComponent;
  let fixture: ComponentFixture<SelectCatalogComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectCatalogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectCatalogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

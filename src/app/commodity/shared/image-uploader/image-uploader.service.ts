import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SharedService } from '@app/core/services/shared.service';
import { EnvService } from '@app/core/services/env.service';
import { AuthenticationService } from '@app/core/services/authentication.service';
import { DictionaryService } from '@app/core/services/dictionary.service';

@Injectable({
  providedIn: 'root'
})
export class ImageUploaderService {

  private imgUrl = this.env.apiUrl + '/FileS3/UploadImage?shopId='+this.authService.getCustId();
  private filesUrl = this.env.apiUrl + '/FileS3/UploadFile?shopId='+this.authService.getCustId();

  constructor(
    private http: HttpClient,
    private shared: SharedService,
    private env: EnvService,
    public dictionary: DictionaryService,
    private authService: AuthenticationService
    ) { }

  upload(file: File| Blob,filename: string = this.shared.uuidv4(),type = 1): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    formData.append('file', new File([file],filename));
    const req = new HttpRequest('POST', `${type === 1 ? this.imgUrl : this.filesUrl}`, formData, {
      reportProgress: true,
      responseType: 'json'
    });
    return this.http.request(req);
  }


}

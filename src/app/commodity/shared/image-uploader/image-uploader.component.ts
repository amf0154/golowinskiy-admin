import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Component, EventEmitter, HostListener, Input, OnInit, Output } from '@angular/core';
import { ImageUploaderService } from './image-uploader.service';
import { getBase64Strings } from 'exif-rotate-js/lib';
import { SharedService } from '@app/core/services/shared.service';
import { MainService } from '@app/core/services/main.service';
import { DictionaryService } from '@app/core/services/dictionary.service';
import { MatDialog } from '@angular/material/dialog';
import { AttachAudioToImgComponent } from '../attach-audio-to-img/attach-audio-to-img.component';
import { AudioImageService } from '../attach-audio-to-img/audio-image.service';
const Swal = require('sweetalert2');

@Component({
  selector: 'image-uploader',
  templateUrl: './image-uploader.component.html',
  styleUrls: ['./image-uploader.component.scss']
})
export class ImageUploaderComponent implements OnInit {
  @Output() isUploadsDone = new EventEmitter();
  @Output() deleteStack = new EventEmitter();
  @Input('onPaste') $onPaste?: boolean = false;
  @Input() audioAttach? : boolean = true;
  @Input() isEditMode?: boolean = false;
  @Input() fileInfos = [];
  @Input() singleMode? = false;
  selectedFiles: FileList;
  progressInfos = [];
  message = '';
  indexMainImg: number  = 0;
  constructor(
    private uploadService: ImageUploaderService,
    private shared: SharedService,
    public dictionary: DictionaryService,
    private audioImageState: AudioImageService,
    private mainService: MainService,
    private dialog: MatDialog
    ) { }

  ngOnInit(): void {

  }

  selectFiles(event, i = null): void {
      this.update(i)
  //  this.progressInfos = [];
    if(event.target.files.length < (8 - this.fileInfos.length)){
      this.selectedFiles = event.target.files;
      if(this.selectedFiles.length)
      this.uploadFiles()
    }else{
      Swal.fire({
        position: 'center',
        icon: 'warning',
        title: this.dictionary.getTranslate('exceed_max_img','Максимальное количество картинок не может быть больше') +' 9',
        showConfirmButton: false,
        timer: 2000
      });
    }
  }

  @HostListener('window:paste', ['$event'])
  onPaste($event) {
    if(this.$onPaste){
      const dt = $event.clipboardData;
      const file = dt.files[0];
      let obj = {
        target: {
          files: [file]
        }
      }
      if(file && file.type.includes('image'))
        this.selectFiles(obj);      
    };
  };

  uploadFiles(): void {
    this.message = '';
  
    for (let i = 0; i < this.selectedFiles.length; i++) {
      this.upload(i, this.selectedFiles[i]);
    }
  }

  attachAudio(index: number,audio: string){
    this.dialog.open(AttachAudioToImgComponent, {
      width: '300px',
      data: {
        link: audio, 
        isEdit: !!this.isEditMode
      },
    })
    .afterClosed().subscribe(result => {
      if(result){
        this.fileInfos[index].audio = result['link'];        
      }else{
        this.fileInfos[index].audio = null;
      }
    });
  }

  update(i){
    if(i !== null){
      const {link} = this.fileInfos[i]
      if(link){
        this.deleteStack.emit(link);
        this.fileInfos.splice(i,1);
      }      
    }
  }

  upload(idx, file): void {
    this.isUploadsDone.emit(false);   
    getBase64Strings([file], { maxSize: 2400 }).then(res=>{
      fetch(res[0])
      .then(res => res.blob())
      .then($file=>{
        this.progressInfos[idx] = { value: 0, fileName: null };
        const imgName = this.shared.uuidv4()+'.png';
        this.uploadService.upload($file,imgName).subscribe(
          event => {
            if (event.type === HttpEventType.UploadProgress) {
              this.progressInfos[idx].value = Math.round(100 * event.loaded / event.total);
            } else if (event instanceof HttpResponse) {
                const {data, success} = event.body;
              if(success){
                if(this.isEditMode){
                  const indexes = this.fileInfos.map((i)=> i.imageOrder);
                  this.fileInfos.push({
                    link: data,
                    imageOrder: indexes.length ? Math.max(...indexes) + 1 : 0
                  });
                }else{
                this.fileInfos.push({
                  link: data,
                }); 
              }
                this.progressInfos.splice(idx,1);  
                this.isUploadsDone.emit(this.progressInfos.length === 0);   
              }
            }
          },
          err => {
            this.progressInfos[idx].value = 0;
            this.message = 'Could not upload the file:' + file.name;
          });
      })
    })
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.fileInfos, event.previousIndex, event.currentIndex);
  }

  // setMainImg(i: number){
  //   this.fileInfos.forEach((el,index)=>{
  //     el.isMain = index === i ? true : false
  //   });
  // }

  getMainImg(){
    if(this.indexMainImg !== null)
      return this.fileInfos[this.indexMainImg]
  }

  delete(i: number){
    const {link} = this.fileInfos[i]
    if(link){
      if(!this.isEditMode){
        this.mainService.deleteCloudContent(link).subscribe();
        // if(this.fileInfos[i].audio)
        //   this.commodity.deleteAudioStack([{link: this.fileInfos[i].audio}])
      }else{
        this.deleteStack.emit(link);
        if (this.fileInfos[i].audio){
          this.audioImageState.deleteStack.push({link: this.fileInfos[i].audio});
        }
          
          
      }
      this.fileInfos.splice(i,1);
    }
  }


}

import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthenticationService } from '@app/core/services/authentication.service';
import { DictionaryService } from '@app/core/services/dictionary.service';
import { EnvService } from '@app/core/services/env.service';
import { MainService } from '@app/core/services/main.service';
import { SharedService } from '@app/core/services/shared.service';
import { SpinnerService } from '@app/core/services/spinner.service';
import { ToastrService } from 'ngx-toastr';
import { CommodityService } from '../../commodity.service';
import { AudioImageService } from '../attach-audio-to-img/audio-image.service';
@Component({
  selector: 'base-commodity',
  templateUrl: './base-commodity.component.html',
  styleUrls: ['./base-commodity.component.scss']
})
export class BaseCommodityComponent implements OnInit {

  constructor(
    public mainService: MainService,
    public commodity: CommodityService,
    public authService: AuthenticationService,
    public environment: EnvService,
    public shared: SharedService,
    public toastr: ToastrService,
    public audioImageService: AudioImageService,
    public route: ActivatedRoute,
    public router: Router,
    public spinner: SpinnerService,
    public fb: FormBuilder,
    public dictionary: DictionaryService
  ) {}

  ngOnInit(): void {
    this.checkIfEditMode();
  }

  @Input() data?: any = null;
  public images = []; // additional images;
  public baseImage = []; // main image;
  public attachedAudio? = []; // audioRecord;
  private allImagesUploaded: boolean = true; // is additional images uploaded;
  private baseImageUploaded: boolean = true; // is base image uploaded;
  private audioUploaded: boolean = true; // is audio uploaded;
  public selectedCategory: any = null; // is category selected;
  public submitted: boolean = false; // is form submitted;
  public isEditMode: boolean = false;
  public advForm: FormGroup = this.fb.group({
    Appcode: [this.authService.getCustId()],
    Catalog: [this.authService.getCustId()],
    CID: [this.authService.getCustId(), [Validators.required]],
    Ctlg_Name: ['', [Validators.required]],
    Id: [null,[Validators.required]],
    PrcNt: [null],
    TArticle: [null],
    TCost: [null],
    TDescription: [''],
    TName: [null, [Validators.required]],
    TransformMech: [null],
    TypeProd: [null],
    audio: [null],
    video: [null],
    TImageprev: [null]
  });

    get f() {
      return this.advForm.controls;
    }

    checkIfEditMode(){
      if(this.data){
        this.isEditMode = true;
        this.advForm.setValue(this.data.form);
        if(this.data.info){
          if(this.data.info.t_imageprev){
            this.baseImage = [{link: this.data.info.t_imageprev}];
          }
          if(this.data.info.additionalImages != 0){
            this.images = this.data.info.additionalImages.map((e)=>({...e,link: e.t_image}));
          }
          if(this.data.info.mediaLink && this.data.info.mediaLink.trim().length){
            const protocol = 'https://';
            const mediaLink = this.data.info.mediaLink
            const link = mediaLink.startsWith(protocol) ? mediaLink.split(protocol)[1] : mediaLink;
            this.attachedAudio = [{link: link}]
          }
        }
      }
    }

    setMainImage(){
      const mainImage = this.baseImage[0];
      this.advForm.controls['TImageprev'].setValue(mainImage ? mainImage.link : "");
    }

    setAudio(){
      if(this.attachedAudio.length){
        this.advForm.controls['audio'].setValue("https://"+ this.attachedAudio[0].link);
      }else{
        this.advForm.controls['audio'].setValue("");
      }
    }

    $isAllImgsUploaded(status: boolean){
      this.allImagesUploaded = status;
    }

    $isBaseUploaded(status: boolean){
      this.baseImageUploaded = status;
    }

    private audioStackForDelting = [];
    $audioDeleteStack($event){
      this.audioStackForDelting.push($event);
    }

    $isAudioUploaded(status: boolean){
      this.audioUploaded = status;
    }

    $selectedCategoryId(cat){
      this.selectedCategory = cat;
      this.setSelectedCategory();
    }

    // FOR EDIT MODE (DELETE ADDITIONAL IMAGES AFTER SAVING CHANGES)
    private imgStackForDelting = []; // delete array of these images only after press Save button (edit commodity)
    $imgDeleteStack(image){
      this.imgStackForDelting.push(image);
    }

    private setSelectedCategory(){
      this.advForm.controls['Ctlg_Name'].setValue(this.selectedCategory ? this.selectedCategory.txt : null);
      this.advForm.controls['Id'].setValue(this.selectedCategory ? this.selectedCategory.id : null);
    }

    isInvalid(ctrName: string){ 
      const isInvalid = this.advForm.controls[ctrName].invalid;
      return {
        'is-invalid': this.submitted && isInvalid,
        'is-valid': this.submitted && !isInvalid 
      }
    }

    countAdditionalImgs: number = 0;
    submit(){
      this.submitted = true;
      this.setAudio();
      this.setMainImage();
      if(this.advForm.invalid){
        if(this.advForm.controls['Id'].invalid){
          this.toastr.error(this.dictionary.getTranslate('cat_not_selected','Вы не выбрали категорию каталога'));
        }else if(this.advForm.controls['TImageprev'].invalid){
          this.toastr.error(this.dictionary.getTranslate('no_adv_img','Вы не загрузили основную картинку!'));
        }else if(this.advForm.controls['TName'].invalid){
          this.toastr.error(this.dictionary.getTranslate('no_adv_title','Вы не ввели наименование!'));
        }
      }else{
        this.spinner.display(true);
        let intervalChecker = setInterval(()=>{
          if(this.allImagesUploaded && this.baseImageUploaded && this.audioUploaded){
            clearInterval(intervalChecker);
            if (this.data) {
              this.saveEditting();
            }else {
              this.uploadAdvert();
            }
          }
        },1000);
      }
    };

    uploadAdvert(){
      this.commodity.createCommodity(this.advForm.value,this.images,this.selectedCategory.id)
        .subscribe((prc_id) => {
          this.resetData();
          // setTimeout(()=>{
          //   if(prc_id)
          //   this.router.navigate(['/commodity/edit'], { queryParams: { id: prc_id } });
          // },500);
       },
       () => this.spinner.display(false));
    };

    private resetData(){
      this.spinner.display(false);
      this.toastr.success(this.dictionary.getTranslate('adv_created','Обьявление успешно добавлено!'));
      this.images = [];
      this.baseImage = [];
      this.attachedAudio = [];
      this.submitted = false;
      this.imgStackForDelting.length = 0;
      this.advForm.reset();
      this.setSelectedCategory();
      this.advForm.controls['Appcode'].setValue(this.authService.getCustId());
      this.advForm.controls['Catalog'].setValue(this.authService.getCustId());
      this.advForm.controls['CID'].setValue(this.authService.getCustId());
    }

    getAdditImgsForUpd(){
      let qIndexes = this.images.map((i)=>i.imageOrder);
      const getMinAndRemoveIndex = () =>{
        const min = Math.min(...qIndexes);
        qIndexes.splice(qIndexes.indexOf(min),1);
        return min;
      }
      return this.images.map((img,index)=>({
        "Catalog": this.authService.getUserId(),
        "Id": this.data.form.Id,
        "Prc_ID": this.route.snapshot.queryParams['id'],
        "ImageOrder": getMinAndRemoveIndex(),
        "TImage": img.link,
        "Appcode": this.authService.getUserId(),
        "audio": img?.audio,
        "CID": this.authService.getUserId()
      }));
    }


    // FOR EDIT MODE ONLY (link to requestBody for deleting)
    convDelImagesToView(){
      const getOrderId =(link: string) =>{
        const image = this.data.info.additionalImages.find((t) => t.t_image == link);
        return image ? image.imageOrder.toString() : null;
      }
      return this.imgStackForDelting.map((l)=> ({
        link: l,
        ImageOrder: getOrderId(l),
        Prc_ID: this.route.snapshot.queryParams['id'],
        appCode: this.authService.getUserId(),
        cid: this.authService.getUserId(),
        cust_ID: this.authService.getUserId(),
      }));
    }

    saveEditting(){
      const images = this.getAdditImgsForUpd();
      const prcId = this.route.snapshot.queryParams['id'];
      this.setAudio();
      this.setMainImage();
      this.commodity.updateCommodity(
        this.advForm.value,
        images,
        this.convDelImagesToView(),
        this.audioStackForDelting,
        prcId
      )
      .subscribe((resp: any)=>{
        this.submitted = false;
        this.toastr.success(this.dictionary.getTranslate('adv_updated','Обьявление успешно обновлено!'));
        this.commodity.updateAudioImages(images,prcId);
        this.spinner.display(false);      
      })
    }
}

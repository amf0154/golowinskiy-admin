import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DictionaryService } from '@app/core/services/dictionary.service';
import {Howl} from 'howler';
import { AudioRecordingService } from './audio-recording.service';
@Component({
  selector: 'voice-recorder',
  templateUrl: './voice-recorder.component.html',
  styleUrls: ['./voice-recorder.component.scss']
})
export class VoiceRecorderComponent implements OnInit {

  public blobUrl: Blob = null;
  public vocal_playing: boolean = false;
  public isRecording: boolean = false;
  public currentAudio: any = null;
  public showRecorder: boolean = true;

  constructor(
    private audioRecordingService: AudioRecordingService,
    public dictionary: DictionaryService,
    public dialogRef: MatDialogRef<VoiceRecorderComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    this.audioRecordingService.getRecordedBlob().subscribe((data) => {
      this.blobUrl = data.blob;
      const way = URL.createObjectURL(data.blob);
      this.currentAudio = new Howl({
        src: [way],
        html5: true,
        ext: ['ogg']
      });
    });

  }

  close(): void {
    this.dialogRef.close();
  }

  save(){
    this.dialogRef.close(this.blobUrl);
  }

  timer: number = 0;
  startSec: any = null;
  startCountSec() {
    this.timer++;
  }

  stopSec = () => clearInterval(this.startSec);

  startRec() {
    this.timer = 0;
    this.startSec = setInterval(() => this.startCountSec(), 1000);
    this.audioRecordingService.startRecording();
    this.isRecording = true;
  }

  stopRec() {
    this.stopSec();
    this.isRecording = false;
    this.audioRecordingService.stopRecording();
    this.isRecording = false;
  }

  async playRec() {
    this.vocal_playing = true;
    this.reductButton(this.timer);
    this.currentAudio.play();
  }

  reductingProcess;
  reductButton = (time) => this.reductingProcess = setTimeout(() => this.vocal_playing = false, Number(time + '000'));

  async pauseRec() {
    this.currentAudio.pause();
    this.currentAudio.currentTime = 0;
    this.vocal_playing = false;
    clearTimeout(this.reductingProcess);
  }

  clearRecordedData() {
    this.blobUrl = null;
  }
}

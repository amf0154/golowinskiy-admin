import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '@app/core/services/authentication.service';
import { DictionaryService } from '@app/core/services/dictionary.service';
import { MainService } from '@app/core/services/main.service';
import { SpinnerService } from '@app/core/services/spinner.service';
import { CommodityService } from '../commodity.service';
import { AudioImageService } from '../shared/attach-audio-to-img/audio-image.service';

@Component({
  selector: 'app-edit-commodity',
  templateUrl: './edit-commodity.component.html',
  styleUrls: ['./edit-commodity.component.scss']
})
export class EditCommodityComponent implements OnInit, OnDestroy{

  constructor(
    public fb: FormBuilder,
    public authService: AuthenticationService,
    public route: ActivatedRoute,
    private audioImageState: AudioImageService,
    public spinner: SpinnerService,
    public commodity: CommodityService,
    public mainService: MainService,
    public dictionary: DictionaryService
  ) {
    
  }
  public advId = this.route.snapshot.queryParams['id'];
  public data: any = null; // advertData
  public isLoaded = false;

  ngOnInit(): void {
    this.spinner.display(true);
    this.mainService.retailPrice(this.advId,this.authService.getCustId()).subscribe((retail: {retail: number,isChange:boolean})=>{
      this.commodity.getProduct(this.advId, this.authService.getUserId(), this.authService.getCustId()).subscribe( (res: any) => {
        if(Number(res.id)){
          this.audioImageState.setArticle(res);
          this.data = {
            form: {
              Appcode: this.authService.getCustId(),
              Catalog: this.authService.getCustId(),
              Id: res.id,
              CID: this.authService.getCustId(),
              TArticle: res.ctlg_No,
              TName: res.tName,
              TDescription: res.tDescription ? res.tDescription.replace(/<br>/g,'\n') : res.tDescription,
              TCost: retail.retail, //res.prc_Br,
              TImageprev: '',
              TypeProd: '',
              PrcNt: '',
              TransformMech: '',
              Ctlg_Name: res.ctlg_Name,
              video: res.youtube,
              audio: res.mediaLink,
            },
            info: res
          }; 
        }
        this.isLoaded = true
        this.spinner.display(false);
      });
    });
  }

  ngOnDestroy(): void {
    this.audioImageState.resetData();
  }

}

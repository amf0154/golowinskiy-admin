import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommodityRoutingModule } from './commodity-routing.module';
import { CommodityComponent } from './commodity.component';
import { NewCommodityComponent } from './new-commodity/new-commodity.component';
import { DropzoneConfigInterface, DropzoneModule, DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';
import { DropzoneComponent } from './shared/dropzone/dropzone.component';
import { ImageUploaderComponent } from './shared/image-uploader/image-uploader.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { SelectCatalogComponent } from './shared/select-catalog/select-catalog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AudioUploaderComponent } from './shared/audio-uploader/audio-uploader.component';
import { VoiceRecorderComponent } from './shared/voice-recorder/voice-recorder.component';
import { AudioPlayerComponent } from './shared/audio-player/audio-player.component';
import { AudioRecordingService } from './shared/voice-recorder/audio-recording.service';
import { AppModule } from '@app/app.module';
import { SharedModule } from '@app/shared.module';
import { BaseCommodityComponent } from './shared/base-commodity/base-commodity.component';
import { EditCommodityComponent } from './edit-commodity/edit-commodity.component';
import { AttachAudioToImgComponent } from './shared/attach-audio-to-img/attach-audio-to-img.component';

const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
   url: 'https://httpbin.org/post',
   maxFilesize: 50,
   acceptedFiles: 'image/*,.mp4,.mkv,.avi',
   addRemoveLinks: true,
 };

@NgModule({
    declarations: [
        CommodityComponent,
        NewCommodityComponent,
        DropzoneComponent,
        ImageUploaderComponent,
        SelectCatalogComponent,
        AudioUploaderComponent,
        VoiceRecorderComponent,
        AudioPlayerComponent,
        BaseCommodityComponent,
        EditCommodityComponent,
        AttachAudioToImgComponent
    ],
    imports: [
        CommonModule,
        CommodityRoutingModule,
        DragDropModule,
        DropzoneModule,
        MatButtonModule,
        MatCheckboxModule,
        MatTooltipModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
        FormsModule,
        SharedModule
    ],
    providers: [
        {
            provide: DROPZONE_CONFIG,
            useValue: DEFAULT_DROPZONE_CONFIG
        },
        AudioRecordingService
    ]
})
export class CommodityModule { }

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommodityComponent } from './commodity.component';
import { EditCommodityComponent } from './edit-commodity/edit-commodity.component';
import { NewCommodityComponent } from './new-commodity/new-commodity.component';

const routes: Routes = [
  { path: '', component: CommodityComponent },
  { path: 'new', component: NewCommodityComponent },
  { path: 'edit', component: EditCommodityComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommodityRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DictionaryService } from '@app/core/services/dictionary.service';

@Component({
  selector: 'app-commodity',
  templateUrl: './commodity.component.html',
  styleUrls: ['./commodity.component.scss']
})
export class CommodityComponent implements OnInit {

  constructor(
    public dictionary: DictionaryService,
    public router: Router
  ) { }

  ngOnInit(): void {
  }

  showInputForm = false;
  prcId = 47678502;
  edit(){
    this.showInputForm = true;
  }

}

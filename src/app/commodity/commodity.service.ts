import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EnvService } from '../../../src/app/core/services/env.service';
import { map, catchError, tap, mergeMap, switchMap } from 'rxjs/operators';
import { forkJoin, from, observable, of, throwError } from 'rxjs';
import { AuthenticationService } from '@app/core/services/authentication.service';
import { TransitionCheckState } from '@angular/material/checkbox';
import { AdvertDetail } from '@app/core/models/advert-list';
import { AudioImageService } from './shared/attach-audio-to-img/audio-image.service';

@Injectable({
  providedIn: 'root'
})
export class CommodityService {

  constructor(
    private http: HttpClient,
    private environment: EnvService,
    private audioImgService: AudioImageService,
    private authService: AuthenticationService
  ) {}

  createCommodity(data: any,additionalImages,categoryId) {
    return this.http.post(`${this.environment.apiUrl}/product`, data).pipe(
      switchMap((res: {prc_id: number}) => additionalImages.length ? 
      this.additionalImageUpload(additionalImages,res.prc_id,categoryId) : 
      this.moveToMainAdditionalPicture(res.prc_id))
    )
  }

  editAdditionalImg(data: any) {
    return this.http.put(`${this.environment.apiUrl}/AdditionalPicture/Update`, {pictures: data});
  }

  updateCommodity(data: any,images,deletingImages,deletingAudio,prc_ID) {
    return this.http.put(`${this.environment.apiUrl}/product`, data)
      .pipe(
        switchMap(() => this.deleteAudioForImages(images)), // additional images
        switchMap(() => this.deleteAddImagesArr(deletingImages)), // additional images
        switchMap(() => this.deleteAddCloudFilesArr(deletingAudio)), // base audio 
//        switchMap(() => this.deleteAddCloudFilesArr(deletingImages)), // a
        switchMap(() => this.editAdditionalImg(images)),
        switchMap(() => this.updateAudioImages(images,prc_ID)), // additional images
        switchMap(() => this.moveToMainAdditionalPicture(prc_ID))
      );
  }

  updateAudioImages(images,prc_ID){
    const audioImgBody = images.map((a)=>({
      "prc_ID": prc_ID,
      "mediaOrder": a.ImageOrder,
      "mediaLink": a.audio ? this.removeProtocol(a.audio) : null,
      "appcode": this.authService.getCustId(),
      "cid": this.authService.getCustId()
    }));
    return this.http.post(`${this.environment.apiUrl}/Media/AddFile`,{mediaFiles: audioImgBody}) 
  }

  removeProtocol(url){
    return url.startsWith('https://') ? url.split('https://')[1] : url;
  }

  async deleteAudioForImages(images){
    const { beforeUpd, deleteStack } = this.audioImgService.additAudioRemover();
    if(beforeUpd.length){
      const deletedAudioIndexes = beforeUpd.map((a)=> a.mediaOrder);
      const deletedAudioLinks = beforeUpd.map((a)=> a.mediaLink);
      deletedAudioIndexes.forEach(index => {
        if(images[index]?.audio && deletedAudioLinks.includes(images[index].audio)){
          images[index].audio = null;
        }
      });
    }
    const getOptions = () => {
      return {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        }),
        body: {mediaFiles: beforeUpd}
      };
    }

    try{
      if(!beforeUpd.length && !deleteStack.length){
        return Promise.resolve();
      }
  
      if(beforeUpd.length)
      await this.http.delete(`${this.environment.apiUrl}/Media/DeleteFile`,getOptions()).toPromise();
  
  
      if (deleteStack.length) {
        await Promise.all(deleteStack.map((n: any) => 
          this.http.delete(`${this.environment.apiUrl}/FileS3/DeleteImage?file=${n.link}`).toPromise()
        ));
      }
  
    return await Promise.resolve(true);

    }catch(e){
      return Promise.reject(false)
    }


    // if(beforeUpd.length && deleteStack.length){
    //   return Promise.all([
    //     this.http.delete(`${this.environment.apiUrl}/Media/DeleteFile`,getOptions()).toPromise(),
    //     Promise.all(deleteStack.map((n: any) => 
    //       this.http.delete(`${this.environment.apiUrl}/FileS3/DeleteImage?file=${n.link}`)
    //     ))
    //   ])
    // }

    // const request = beforeUpd.length ? 
    // this.http.delete(`${this.environment.apiUrl}/Media/DeleteFile`,getOptions()) 
    // : of(true);
    // return request.pipe(
    //   switchMap(() => this.deleteAddCloudFilesArr(deleteStack))
    // ).toPromise();
  }

  deleteAddCloudFilesArr(data: any){
    return data.length ? forkJoin(
      data.map((n: any) => 
        this.http.delete(`${this.environment.apiUrl}/FileS3/DeleteImage?file=${n.link}`)
    )) : of(true)
  }

  deleteAudioStack(data: any){
    return forkJoin(
      data.map((n: any) => 
        this.http.delete(`${this.environment.apiUrl}/FileS3/DeleteImage?file=${n.link}`)
    ))
  }


  // deleteAudioCloudFilesArr(data: any){
  //   return data.length ? forkJoin(
  //     data.map((n: any) => 
  //       this.http.delete(`${this.environment.apiUrl}/Media/DeleteFile`,n)
  //   )) : of(true)
  // }

  getAttachedAudioImg(advData: AdvertDetail) {
    const getAudioByOrder = (orderId,audios) => {
      const audio = audios.find((a)=> a.mediaOrder == orderId);
      return audio ? audio.mediaFileName : null;
    }
    const preparedBody  = advData.additionalImages.map((_,i)=>({
      "prc_ID": advData.prc_ID,
      "mediaOrder": _.imageOrder,
      "appcode": this.authService.getCustId(),
      "cid": this.authService.getCustId()
    }))
    return this.http.post(`${this.environment.apiUrl}/Media/GetFileLink`, {mediaFiles: preparedBody})
    .pipe(
      map((audios:Array<any>)=> {
        const existedAudio = audios.filter((a)=> a);
        advData.additionalImages = advData.additionalImages.map((img)=>({
          ...img,
          audio: getAudioByOrder(img.imageOrder,existedAudio)
        }));
        return advData;
      })
    );
  }


  getProduct(prc_ID: any, cust_id, appCode) {
    return this.http.post(`${this.environment.apiUrl}/Img`,
      {
        prc_ID: prc_ID,
        cust_ID: cust_id,
        appCode: appCode
      }
    ).pipe(
      switchMap((data: AdvertDetail) => (data.additionalImages.length) ? this.getAttachedAudioImg(data) : of(data))
    );
  }


  // this.commodity.getAttachedAudioImg(
  //   [
  //   {
  //     "prc_ID": this.route.snapshot.queryParams['id'],
  //     "mediaOrder": 0,
  //     "appcode": 24442,
  //     "cid": 24442
  //   }
  // ]).subscribe((r)=>{
  // })
  

  // {
  //   "prc_ID": 0,
  //   "mediaOrder": "string",
  //   "mediaLink": "string",
  //   "appcode": 0,
  //   "cid": "string"
  // }

  // deleteMediaLink(prc_ID){
  //   let options = {
  //     headers: new HttpHeaders({
  //       'Content-Type': 'application/json',
  //       'Authorization': `Bearer ${localStorage.getItem('token')}`
  //     }),
  //     body: {
  //       prc_ID: prc_ID,
  //       appCode: this.authService.getCustId()
  //     }
  //   };
  //   return this.http.delete(`${this.environment.apiUrl}/Product/MediaLink`, options)
  //   .pipe(
  //     map(response => {
  //       return response
  //     }),
  //     catchError((error: any) => {
  //       return throwError(error);
  //     })
  //   );
  // }
  

  deleteAddImagesArr(data: any){
    const getOptions = () =>{
      return {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        }),
        body: {
          files: data.filter(i => i.ImageOrder !== null)
        }
      };
    }
    return data.length ? this.http.delete(`${this.environment.apiUrl}/AdditionalPicture/Delete`,getOptions()) : of(true)
  }

  additionalImageUpload(additionalImages: Array<{link: string}>,prc_id,categoryId) {
    const additionalImgBody = {
      pictures: additionalImages.map((_,i) =>({
        catalog: this.authService.getCustId(),
        id: categoryId,
        imageOrder: i,
        prc_ID: prc_id,
        tImage: additionalImages[i].link,
        appcode: this.authService.getCustId(),
        cid: this.authService.getCustId()
      }))
    };
    return this.http.post(`${this.environment.apiUrl}/AdditionalPicture/Create`,additionalImgBody).pipe(
      switchMap(() => this.audioRecordsForAddImages(prc_id,additionalImages)),
      switchMap(() => this.moveToMainAdditionalPicture(prc_id)),
    )
  }

    // {
    //   "prc_ID": 0,
    //   "mediaOrder": "string",
    //   "content": "string",
    //   "appcode": 0,
    //   "cid": "string"
    // }
  public audioRecordsForAddImages(prc_id,additionalImages){
    const audiosImgBody = {
      mediaFiles: additionalImages.map((_,i) =>({
        prc_ID: prc_id,
        mediaOrder: i,
        mediaLink: additionalImages[i].audio,
        appcode: this.authService.getCustId(),
        cid: this.authService.getCustId()
      })).filter((m) => m.mediaLink)
    };
    return this.http.post(`${this.environment.apiUrl}/Media/AddFile`,audiosImgBody);
    // return forkJoin(
    //   audiosImgBody.audios.map(function attachAudioForImg(body: any) {
    //     return this.http.post(`${this.environment.apiUrl}/Media/AddFile`,body)
    //   })
    // )
  }

  public moveToMainAdditionalPicture(prc_id){
    return this.http.put(`${this.environment.apiUrl}/AdditionalPicture/MoveToMain`, {
      "appCode": this.authService.getCustId(),
      "prc_ID": prc_id
    }).pipe(map(()=> prc_id));
  }

}


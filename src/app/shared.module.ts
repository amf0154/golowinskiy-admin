import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MobileCategoriesComponent } from './components/categories/mobile-categories/mobile-categories.component';
import { EditClientsListComponent } from './components/shared/edit-clients-list/edit-clients-list.component';
import { MaterialModule } from './material.module';

@NgModule({
  declarations: [MobileCategoriesComponent, EditClientsListComponent],
  imports: [CommonModule, MaterialModule],
  exports: [MobileCategoriesComponent],
})
export class SharedModule {}

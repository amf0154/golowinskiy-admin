import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LocaleComponent } from './locale.component';

const routes: Routes = [
  { path: 'admin', component: LocaleComponent },
  { path: 'shop', component: LocaleComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocaleRoutingModule { }

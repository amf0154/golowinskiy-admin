import { HttpClient } from "@angular/common/http";
import { AfterViewInit, Attribute, Component, ElementRef, HostListener, OnInit, ViewChild } from "@angular/core";
import { AbstractControl, FormArray, FormBuilder } from "@angular/forms";
import { MainService } from "@app/core/services/main.service";
import Swal from "sweetalert2";
import deepClone from "@techuila/deep-clone";
import { BehaviorSubject, fromEvent, of } from "rxjs";
import { debounceTime, map, startWith, switchMap } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";
import { SpinnerService } from "@app/core/services/spinner.service";
import { ActivatedRoute } from "@angular/router";
import { DictionaryService } from "@app/core/services/dictionary.service";
@Component({
    selector: "app-locale",
    templateUrl: "./locale.component.html",
    styleUrls: ["./locale.component.scss"]
})
export class LocaleComponent implements OnInit {
    filterLangColumnsOnly = (attr: string) => !["ALIAS", "PageName", "match"].includes(attr);
    listOfLanguages = [];
    initialListOfLanguages = [];
    initialData: any = { data: [] };
    dataLoaded: boolean = false;
    langForm = this.fb.group({
        languages: new FormArray([])
    });
    searchForm = this.fb.group({
      text: ""
  });
    localizationForm = this.fb.group({
        table: new FormArray([])
    });
    table = this.localizationForm.get("table") as FormArray;
    language = this.langForm.get("languages") as FormArray;
    lib: number = 1;
    constructor(
        public requestService: MainService,
        private fb: FormBuilder,
        public toastr: ToastrService,
        public dictionary: DictionaryService,
        public spinner: SpinnerService,
        public $route: ActivatedRoute
    ) {
        this.$route.queryParams.subscribe((r) => {
            if (r.hasOwnProperty("lib")) {
                this.lib = r.lib;
            }
        });
    }

    getLanguages(locales) {
        return locales.reduce((languages, el) => {
            languages[el] = { value: "", disabled: !this.editMode };
            return languages;
        }, {});
    }

    addFuncional = false;
    showHideAddFunc() {
        this.addFuncional = !this.addFuncional;
    }

    genDynamicGroup(data) {
        return this.fb.group(
            [data].reduce(
                (obj, item, i) => {
                 //   item.hightlight = false;
                    item.match = false
                    const keys = Object.keys(item);
                    keys.forEach((k) => {
                        obj[k] = { value: item[k], disabled: !this.editMode };
                        // if(['Alias','Component','PageName'].includes(k)){
                        //   obj[k] = {value: item[k], disabled: true}
                        // }else{
                        //   obj[k] = [item[k]];
                        // }
                    });
                    return obj;
                },
                { ...this.listOfLanguages }
            )
        );
    }

    onUpdate(row, attribute) {
        const { ALIAS, [attribute]: value } = row.value;
        const key = ALIAS + ":" + attribute;
        this.edittedAliases.next(key);
        this.addOrUpdateAlias(ALIAS, attribute, value);
        this.clearSearchText();
    }

    public clearSearchText(): void {
        this.searchForm.get("text").setValue("");
    }

    getAliasByRow(row) {
        return row.value.ALIAS;
    }

    addOrUpdateAlias(alias, locale, translate) {
        const preparedBody = {
            alias: alias,
            dz: locale,
            txt: translate,
            mark: true,
            library: this.lib
        };
        this.requestService.locale.updateLocale(preparedBody).subscribe(({ result }: any) => {
            if (result) this.toastr.success(this.dictionary.getTranslate('settings_saved_suss','Настройки сохранены!'));
            else this.toastr.error("Ошибка сохранения");
        });
    }

    ngOnInit(): void {
        this.spinner.display(true);
        this.language.clear();
        this.table.clear();
        this.dataLoaded = false;
        this.requestService.locale.getLocales(this.lib).subscribe((response: any) => {
            this.spinner.display(false);
            this.initConfiguration(response);
            Object.keys(this.listOfLanguages).forEach((lng) => {
                if (lng !== "RU") {
                    this.language.push(
                        this.fb.group({
                            [lng]: lng == "EN"
                        })
                    );
                }
                if (lng == "RU") {
                    this.language.push(
                        this.fb.group({
                            [lng]: { value: true, disabled: true }
                        })
                    );
                }
            });
            this.triggerCheckboxes();
            this.dataLoaded = true;
            this.langForm.valueChanges.subscribe((res) => {
                this.triggerCheckboxes();
            });
        });

        this.searchForm.get("text").valueChanges.pipe(
          map((el) => el.toLowerCase())
        )
        .subscribe((word: string)=>{
          this.tableForm.forEach((k: any,v)=>{
           const str = k.get('RU').value.toLowerCase();
           const alias = k.get('ALIAS').value.toLowerCase();
           k.get('match').setValue(!!(word.length && (str.includes(word) || alias.includes(word))));
          });
        })
    }

    get tableForm() {
      return (this.table as FormArray).controls;
    }


    triggerCheckboxes() {
        const showData = Object.assign([], ...this.langForm.value.languages);
        const selectedLanguages = Object.entries(showData).reduce((obj, [key, val]) => {
            if (!val) obj.push(key);
            return obj;
        }, []);
        this.filter = [...["ALIAS", "PageName", "match"], ...selectedLanguages];
    }

    filter = ["ALIAS", "PageName", "match"];
    showHideConf = {};

    initConfiguration({ data, locales }) {
        this.initialData = deepClone({ table: data });
        this.listOfLanguages = this.getLanguages(locales);
        data.forEach((lng: any) => {
            this.table.push(this.genDynamicGroup(lng));
        });
    }

    getAttributes(el,v = false) {
      if(v && this.searchForm.get("text").value.length) {
        if(el.getRawValue().match){
          return Object.keys(el.getRawValue()).filter((attr) => !this.filter.includes(attr));
        }else {
          return []
        }
      }
        return Object.keys(el.getRawValue()).filter((attr) => !this.filter.includes(attr));
    }

    getLangAttr(el) {
        return Object.keys(el.getRawValue());
    }

    add() {
        Swal.fire({
            title: "Введите локаль языка (например: JP)",
            input: "text",
            inputAttributes: {
                autocapitalize: "off"
            },
            showCancelButton: true,
            confirmButtonText: "Добавить",
            cancelButtonText: `Отмена`,
            showLoaderOnConfirm: true,
            preConfirm: (lng) => {
                return this.requestService.locale
                    .addDeleteLocale({
                        dz: lng.toUpperCase(),
                        mark: true,
                        library: this.lib
                    })
                    .toPromise()
                    .then((res) => {
                        this.updateTable();
                        Swal.fire({
                            icon: "success",
                            title: `Локаль успешно добавлена`,
                            showConfirmButton: false,
                            timer: 1000
                        });
                        this.ngOnInit();
                        return res;
                    })
                    .catch((error) => {
                        Swal.showValidationMessage(`Ошибка: ${this.translateError(error)}`);
                    });
            },
            allowOutsideClick: () => !Swal.isLoading()
        });
    }

    private translateError(errorResponse: any): string {
        const { error } = errorResponse;
        if (error.startsWith("Column names in each table must be unique")) {
            return "Такая локаль уже существует";
        }
        return error;
    }

    delete(attribute) {
        Swal.queue([
            {
                title: "Удаление локали",
                confirmButtonText: "Удалить",
                icon: "warning",
                text: `Вы действительно хотите удалить ${attribute} локаль?`,
                cancelButtonText: "Отмена",
                showCancelButton: false,
                showDenyButton: true,
                denyButtonText: `Отмена`,
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return this.requestService.locale
                        .addDeleteLocale({
                            dz: attribute,
                            mark: false,
                            library: this.lib
                        })
                        .toPromise()
                        .then(({ result }: any) => {
                            if (result)
                                Swal.fire({
                                    icon: "success",
                                    title: "Локаль успешно удалена",
                                    showConfirmButton: false,
                                    timer: 2000,
                                    willClose: () => this.ngOnInit()
                                });
                            else
                                Swal.fire({
                                    icon: "error",
                                    title: "Ошибка удаления",
                                    showConfirmButton: false,
                                    timer: 2000,
                                    willClose: () => this.ngOnInit()
                                });
                        })
                        .catch((error) => {
                            Swal.fire({
                                icon: "error",
                                title: "Локаль не удалена",
                                showConfirmButton: false,
                                timer: 2000
                            });
                        });
                }
            }
        ]);
    }

    edittedAliases = new BehaviorSubject(null);

    isEditted(row, attribute) {
        const alias = this.getAliasByRow(row);
        return this.edittedAliases.value === alias + ":" + attribute;
    }

    deleteAlias(row) {
        const alias = this.getAliasByRow(row);
        Swal.queue([
            {
                title: `Вы действительно хотите удалить ${alias} аляс?`,
                confirmButtonText: "Удалить",
                icon: "warning",
                text: `Действие необратимо!`,
                cancelButtonText: "Отмена",
                showCancelButton: false,
                showDenyButton: true,
                denyButtonText: `Отмена`,
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return this.requestService.locale
                        .updateLocale({
                            alias: alias,
                            dz: "EN",
                            txt: "",
                            mark: false,
                            library: this.lib
                        })
                        .toPromise()
                        .then(({ result }: any) => {
                            if (result)
                                Swal.fire({
                                    icon: "success",
                                    title: "Аляс успешно удален",
                                    showConfirmButton: false,
                                    timer: 2000,
                                    willClose: () => this.ngOnInit()
                                });
                            else
                                Swal.fire({
                                    icon: "error",
                                    title: "Ошибка удаления",
                                    showConfirmButton: false,
                                    timer: 2000,
                                    willClose: () => this.ngOnInit()
                                });
                        })
                        .catch((error) => {
                            Swal.fire({
                                icon: "error",
                                title: "Локаль не удалена",
                                showConfirmButton: false,
                                timer: 2000
                            });
                        });
                }
            }
        ]);
    }

    addAlias() {
        Swal.fire({
            title: "Создание аляса",
            cancelButtonText: "Отмена",
            showCancelButton: true,
            html:
                '<input id="alias-input1" type="text" placeholder="имя аляса(латиница)" class="swal2-input">' +
                '<input id="translate-input3" type="text" placeholder="перевод (RU)" class="swal2-input">' +
                '<input id="translate-input2" type="text" placeholder="перевод (EN)" class="swal2-input">',
            didOpen: function () {
                document.getElementById("alias-input1").focus();
            },
            preConfirm: () => {
                return Promise.all([
                    this.requestService.locale
                    .updateLocale({
                        alias: (document.getElementById("alias-input1") as HTMLInputElement).value,
                        dz: "EN",
                        txt: (document.getElementById("translate-input2") as HTMLInputElement).value,
                        mark: true,
                        library: this.lib
                    })
                    .toPromise(),
                    this.requestService.locale
                    .updateLocale({
                        alias: (document.getElementById("alias-input1") as HTMLInputElement).value,
                        dz: "RU",
                        txt: (document.getElementById("translate-input3") as HTMLInputElement).value,
                        mark: true,
                        library: this.lib
                    })
                    .toPromise()
                ])
                    .then((res) => {
                        Swal.fire({
                            title: `Аляс успешно добавлен`,
                            showConfirmButton: false,
                            timer: 1200,
                            willClose: () => this.ngOnInit()
                        });
                        return res;
                    })
                    .catch((error) => {
                        Swal.showValidationMessage(`Ошибка: ${error}`);
                    });
            },
            allowOutsideClick: () => !Swal.isLoading()
        });
    }

    updateTable(isReset = false) {
        const currentData = isReset ? this.initialData : this.localizationForm.getRawValue();
        if (isReset) {
            this.listOfLanguages = this.getLanguages(this.initialData.table);
        }
        this.table.clear();
        currentData.table.forEach((lng: any) => {
            this.table.push(this.genDynamicGroup(lng));
        });
    }

    cancel() {
        this.editMode = false;
        this.updateTable(true);
    }

    editMode = false;
    save() {
        this.editMode = false;
        this.updateTable();
    }

    edit(row) {
        if (row) {
            switch (row.status) {
                case "DISABLED": {
                    row.enable();
                    break;
                }
                case "VALID": {
                    row.disable();
                    break;
                }
                case "INVALID":
                    alert("wrong label");
                default:
                    null;
            }
        } else {
            this.editMode = !this.editMode;
            this.updateTable();
        }
    }

    getIconByStatus({ status }) {
        return status === "DISABLED" ? "fa-edit edit-btn" : "fa-check-circle save-btn";
    }
}

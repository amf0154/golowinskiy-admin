import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LocaleRoutingModule } from './locale-routing.module';
import { LocaleComponent } from './locale.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [LocaleComponent],
  imports: [
    CommonModule,
    LocaleRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class LocaleModule { }

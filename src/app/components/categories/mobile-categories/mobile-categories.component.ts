import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { Router } from '@angular/router';
import { CategoryItem } from 'src/app/core/models/category-item';
import { StorageService } from 'src/app/core/services/storage.service';

@Component({
  selector: 'app-mobile-categories',
  templateUrl: './mobile-categories.component.html',
  styleUrls: ['./mobile-categories.component.scss']
})
export class MobileCategoriesComponent implements OnInit {
  private selectedCategories: CategoryItem[] = [];
  @Input() initialCategories: CategoryItem[] = [];
  @Input() categories: CategoryItem[] = [];
  @Output() lastChildAction = new EventEmitter<CategoryItem[]>();
  @Output() clickAction = new EventEmitter<number>();
  @Input() subscribeMode: boolean = false;
  constructor(
    private storageService: StorageService,
  ) {
    this.storageService.selectedCategories.subscribe(value => {
      this.selectedCategories = value;
    });
  }

  ngOnInit() {
    // load selected categories if redirect over breadcrumbs
    this.selectedCategories = this.initialCategories;
  }

  click(level: number, item: CategoryItem) {
    this.clickAction.emit(level);
    const oldCategory = this.selectedCategories[level]
    if (oldCategory !== undefined) {
      this.selectedCategories.splice(level, 10);
    } else {
      this.selectedCategories.splice(level, 10, item);
    }
    if (item.listInnerCat.length === 0) {
    //  if(!this.isEqual(oldCategory, item))
        this.lastChildAction.emit(this.selectedCategories);
    }
    // share categories
    this.storageService.selectedCategories.next(this.selectedCategories);
    setTimeout( ()=> window.scrollTo(0,0) , 500); 
  }

  firstLoad: boolean  = false;
  isShowItem(level: number, item: CategoryItem): boolean {  
    const selected = this.selectedCategories[level];
    const res = selected === undefined || this.isEqual(selected, item);
  //  if(level !==0){
      return res
  /*  }else{
      return true  
    } */
  }

  isShowSubitems(level: number, item: CategoryItem): boolean {
    return this.isEqual(this.selectedCategories[level], item);
  }

  isSelected(level: number, item: CategoryItem): boolean {
    return true //this.isEqual(this.selectedCategories[level], item);
  }

  isEqual(item1?: CategoryItem, item2?: CategoryItem): boolean {
    return (item1 && item1.id) === (item2 && item2.id);
  }

  subscribeResult(result: string){
    if(Boolean(result) === false){
      alert('subscription error')
    }
  }
}


import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MobileCategoriesComponent } from './mobile-categories.component';

describe('MobileCategoriesComponent', () => {
  let component: MobileCategoriesComponent;
  let fixture: ComponentFixture<MobileCategoriesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

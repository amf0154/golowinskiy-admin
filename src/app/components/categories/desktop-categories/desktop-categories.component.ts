import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import {StorageService} from '../../../core/services/storage.service';
import {CategoriesService} from '../../../core/services/categories.service';
import { CategoryItem } from "../../../core/models/category-item";
import { SelectedItem } from "../../../core/models/selected-item";
import { MatDialog } from "@angular/material/dialog";
import { ModifyCategoryComponent } from '../modify-category/modify-category.component';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { MainService } from '@app/core/services/main.service';
import { DictionaryService } from '@app/core/services/dictionary.service';
import { Enums } from '@app/core/models/enums';
const Swal = require('sweetalert2');
@Component({
  selector: 'desktop-categories',
  templateUrl: './desktop-categories.component.html',
  styleUrls: ['./desktop-categories.component.scss']
})
export class DesktopCategoriesComponent implements OnInit {
  loadingImage = "data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==";
  isCategoriesLoaded: Observable<boolean>;
  _categories: CategoryItem[] = []
  @Input() set categories(categories: CategoryItem[]) {
    this._categories = categories;
    this.recalculate = true;
  }
  selected: SelectedItem = {}
  selectedCategories: CategoryItem[] = []
  catalogCheckboxType = 1;
  recalculate = true;
  loaded: boolean = false;
  checked: boolean = true;
  @Input() selectionMode: boolean = false;
  @Input() selectionTypeMode: Enums.CatalogShowCheckboxType = Enums.CatalogShowCheckboxType.LAST_LEVEL_CATEGORY;
  @Input() showCatalog = true
  @Output() click = new EventEmitter();
  @Input() initialCategories: CategoryItem[] = []
  @Output() lastChildAction = new EventEmitter<CategoryItem[]>()
  @Input() subscribeMode: boolean = false;
  @Input() editMode: boolean = false;
  @Input() contextMenuMode: boolean = false;
  @Output() contextMenuEvents = new EventEmitter<{action: string; data: any}>();
  constructor(
              private storageService: StorageService,
              private categoryService: CategoriesService,
              public authService: AuthenticationService,
              public dictionary: DictionaryService,
              public mainService: MainService,
              public dialog: MatDialog,
            ){
      storageService.selectedCategories.subscribe(value => {
        for (let i = 0; i < value.length; i++) {
          this.selected['lavel' + (i + 1)] = value[i]
        }
      });
      this.isCategoriesLoaded = this.categoryService.isCategoriesLoaded();
      this.categoryService.isCategoriesLoaded().subscribe(res=>this.loaded = !res);
  }

  onRedraw() {
    if (this.recalculate) {
      this.recalculateHeight()
    }
  }

  modifyOpened(val){
    this.isModifyPressed = val;
  }

  isModifyPressed: boolean = false;
  modifyCat(type: number, categoryObject, level?,event?: Event){
    if(level !== null && level !== undefined && level != 10){
      this.select(level,categoryObject);
    }
    const data = {
      actionType: type,
      ...categoryObject
    }
    if(level == 10){
      data.id = 0
    }
    this.modalModifyCat(data);
  }

  modalModifyCat(data) {
    let dialogRef = this.dialog.open(ModifyCategoryComponent, {
      width: '500px', disableClose: true,
      data: data
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result === true){
        this.categoryService.fetchCategoriesAll(this.authService.getCustId(),null,"1");
      }else if(typeof result === "object" && result != null){
        const deleteAllCats = Array.isArray(result) && result.length;
        Swal.queue([{
          title: deleteAllCats ? 'Имеются не пустые категории!' : 'Категория не пустая!',
          confirmButtonText: 'Удалить',
          icon: 'warning',
          text: deleteAllCats ? 'Вы действительно хотите удалить все категории?' : 'Вы действительно хотите удалить категорию?',
          cancelButtonText: 'Отмена',
          showCancelButton: false,
          showDenyButton: true,
          denyButtonText: `Отмена`,
          showLoaderOnConfirm: true,
          preConfirm: () => {
            const request = deleteAllCats ? 
            this.mainService.deleteAllCategoriesConfirmed({"allSections":result}).toPromise() : 
            this.mainService.deleteNotEmptyCategoryAsync(result);  
            return request.then(() => {
                  Swal.fire({
                    icon: 'success',
                    title: deleteAllCats ? "Категории успешно удалены" : "Категория успешно удалена",
                    showConfirmButton: false,
                    timer: 2000,
                    willClose: () => this.categoryService.fetchCategoriesAll(this.authService.getCustId(),null,"1")
                  });
              })
              .catch(error => {
                Swal.fire({
                  icon: 'error',
                  title: deleteAllCats ? "Категории не удалена" : "Категория не удалена",
                  showConfirmButton: false,
                  timer: 2000,
                });
              })
          }
        }])
      }
      
    })
  }

  subscribeResult(result: string){
    if(Boolean(result) === false){
      alert('subscription error')
    }
  }
  ngOnInit() {
    this.selectedCategories = this.initialCategories //.slice(0, -1);
  }

  recalculateHeight() {
    let menu = document.getElementsByClassName('left-menu')[0] as HTMLElement
    let menus = menu.querySelectorAll('ul')
    let height = 0;
    for(let i = 0; i < menus.length; i++) {
      let client = menus[i].clientTop + menus[i].clientHeight;
      if (client > height) {
        height = client
      }
    }
    menu.style.minHeight = `${height + 40}px`
    this.recalculate = false;
  }
   previous_level: number = null;
  select(level: number, item: CategoryItem, event?) {
    if(this.isModifyPressed)
      return ;
    this.click.emit(item);
    // if(this.editMode)
    // this.calculateWidth(level);
    if(event)
      event.stopPropagation()
    if (item.listInnerCat.length === 0) {
      if(this.previous_level !== level){
        this.previous_level = level;
        this.selectedCategories.push(item);
        if(!this.editMode && !this.selectionMode){
          this.lastChildAction.emit(this.selectedCategories);
        } 
      }else{
        this.selectedCategories[this.selectedCategories.length -1] = item;
        if(!this.editMode && !this.selectionMode){
          this.lastChildAction.emit(this.selectedCategories);
        }
          
      }
    } else {
      this.previous_level = level;
      if (this.isEqual(this.selectedCategories[level], item)) {
        setTimeout(()=>{
          if(!this.isModifyPressed && !this.editMode){
            this.selectedCategories.length = 0;
            this.storageService.selectedCategories.next(this.selectedCategories);
          }
          if(this.editMode && this.previous_level === 0){
            this.selectedCategories.length = 0;
            this.storageService.selectedCategories.next(this.selectedCategories);
          }
        })
      } else {
        setTimeout(()=>{
          if(!this.isModifyPressed){
            this.selectedCategories.splice(level, this.selectedCategories.length, item);
            this.storageService.selectedCategories.next(this.selectedCategories)
          }
        })
      }
      this.recalculate = true
    }
  }

  calculateWidth(id){
    if(document.getElementById("catArea") != null)
    switch(id){
      case 0: {
        document.getElementById("catArea").style.width="auto";
        break;
      }
      case 1: {
        document.getElementById("catArea").style.width="auto";
        break;
      }
      case 2: {
        document.getElementById("catArea").style.width="1280px";
        break;
      }
      case 3: {
        document.getElementById("catArea").style.width="1530px";
        break;
      }
    }
  }

  isSelected(level: number, item: CategoryItem): boolean {
    return this.isEqual(this.selectedCategories[level], item)
  }

  isShowSubitems(level: number, item: CategoryItem): boolean {
    return this.isEqual(this.selectedCategories[level], item)
  }

  isEqual(item1?: CategoryItem, item2?: CategoryItem): boolean {
    return (item1 && item1.id) === (item2 && item2.id)
  }

  limiter(text: string){
    return text // ? text.length > 22 ? text.substring(0, 19)+ '...' : text : "";
  }

  public deleteCatalog(): void {
    Swal.queue([{
      title: 'Вы действительно хотите удалить каталог?',
      confirmButtonText: 'Удалить',
      icon: 'warning',
      text: 'Это действие приведет к полному удалению каталога, включая все его содержимое!',
      cancelButtonText: 'Отмена',
      showCancelButton: false,
      showDenyButton: true,
      denyButtonText: `Отмена`,
      showLoaderOnConfirm: true,
      preConfirm: () => {
        const request = this.mainService.deleteAllCatalog(this.authService.getCustId()).toPromise()
        return request.then(() => {
              Swal.fire({
                icon: 'success',
                title: "Каталог успешно удален!",
                showConfirmButton: false,
                timer: 2000,
                willClose: () => this.categoryService.fetchCategoriesAll(this.authService.getCustId(),null,"1")
              });
          })
          .catch(error => {
            // Swal.fire({
            //   icon: 'error',
            //   title: deleteAllCats ? "Категории не удалена" : "Категория не удалена",
            //   showConfirmButton: false,
            //   timer: 2000,
            // });
          })
      }
    }])
  }




  selectSubcat(c){
    this.contextMenuEvents.emit({
      action: 'select',
      data: c.listInnerCat
    });
  }

  unselectSubcat(c){
    this.contextMenuEvents.emit({
      action: 'unselect',
      data: c.listInnerCat
    });
  }

}

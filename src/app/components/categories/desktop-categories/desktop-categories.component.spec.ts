import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DesktopCategoriesComponent } from './desktop-categories.component';

describe('DesktopCategoriesComponent', () => {
  let component: DesktopCategoriesComponent;
  let fixture: ComponentFixture<DesktopCategoriesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DesktopCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesktopCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

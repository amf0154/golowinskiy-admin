import { AfterViewChecked, AfterViewInit, Component, OnInit } from '@angular/core';
import { MainService } from 'src/app/core/services/main.service';
import { StorageService } from 'src/app/core/services/storage.service';
import { ProgressbarService } from 'src/app/core/services/progressbar.service';
import { CategoriesService } from 'src/app/core/services/categories.service';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { CategoryItem } from 'src/app/core/models/category-item';
import { AdvertList } from 'src/app/core/models/advert-list';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit, AfterViewChecked {
  public initialCategories: CategoryItem[] = [];
  public categories: CategoryItem[] = [];
  constructor(
    private storageService: StorageService, 
    private mainService: MainService,
    public progress: ProgressbarService, 
    public categoriesService: CategoriesService, 
    public toastr: ToastrService,
    public authService: AuthenticationService) { }

  ngAfterViewChecked(): void {
    if(localStorage.getItem('col-width')){
      this.width = Number(localStorage.getItem('col-width'));
      document.documentElement.style.setProperty('--catalogItemWidth',this.width + 'px')
    }
  }

  ngOnInit(): void {
    this.loadCategoryData();
    
  }

  public width: any = localStorage.getItem('col-width') ? localStorage.getItem('col-width') : 225;
  changr({value}){
    document.documentElement.style.setProperty('--catalogItemWidth',value + 'px');
    this.width = value;
    localStorage.setItem('col-width',value);
  }

  loadCategoryData(){
    this.categoriesService.fetchCategoriesAll(this.authService.getCustId(),null,"1");
  }

  refreshCat: boolean = false;
  breadcrumbsClick(i) {
    this.storageService.setCategories(this.categories.slice(0,i))
    this.categories = [];
    this.storageService.breadcrumbFlag = true
    this.initialCategories = this.storageService.getCategories();
    this.refreshCat = true;
    setTimeout(()=>this.refreshCat = false);  
  }

  previousItem: {} = null;
  selectedCategory: any = null;
  advertItems: Array<AdvertList> = [];
  onCategoriesClick(items: CategoryItem[]) {
    if(!this.previousItem){
      this.previousItem = items[items.length -1];
    }else{
      if(JSON.stringify(this.previousItem) !== JSON.stringify(items[items.length -1])){
        this.previousItem = items[items.length -1];
      }
    }
    this.categories = items;
    const item = items[items.length - 1]
    this.selectedCategory = item;
    this.advertItems.length = 0;
  }

}

import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Validators, FormBuilder } from '@angular/forms';
import { MainService } from 'src/app/core/services/main.service';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { SharedService } from 'src/app/core/services/shared.service';
import { CategoriesService } from '@app/core/services/categories.service';
import { DictionaryService } from '@app/core/services/dictionary.service';
@Component({
  selector: 'app-modify-category',
  templateUrl: './modify-category.component.html',
  styleUrls: ['./modify-category.component.scss']
})

export class ModifyCategoryComponent implements OnInit {

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<ModifyCategoryComponent>,
    private mainService: MainService,
    public dictionary: DictionaryService,
    private categoryService: CategoriesService,
    private authService: AuthenticationService,
    private sharedService: SharedService
    ) { }
  
  public submitted: boolean = false;
  public spinner = false;
  public catalogForm = this.formBuilder.group({ 
    Name: ['', [Validators.required, this.sharedService.emptySpacesValidator]]}
  );
  get f() {
    return this.catalogForm.controls;
  }
  get notForDelete(){
    return ![3,4].includes(this.data.actionType)
  }

  ngOnInit(): void {
    if(this.data.actionType === 2 || this.data.actionType === 3){
      this.catalogForm.controls['Name'].setValue(this.data.txt)
    }else if(this.data.actionType === 4){
      this.catalogForm.controls['Name'].setValue('all');
    }
  }
  
  public saveButton() {
    this.submitted = true;
    if(!this.catalogForm.valid){
      return false
    }else{
      this.spinner = true;
      const preparedBody = {
        "Id": this.data.id,
        ...this.catalogForm.value,
        "ImgName": "betls.png",
        "CustIdMain": this.authService.getCustId()
      }
      switch(this.data.actionType){
        case 1: {
          this.mainService.addNewCategory(preparedBody).subscribe(() => this.dialogRef.close(true),err=>this.spinner = false);
          break;
        }
        case 2:{
          this.mainService.updateCategory(preparedBody).subscribe(() => this.dialogRef.close(true),err=>this.spinner = false);
          break;
        } 
        case 3: {
          this.mainService.deleteCategory(preparedBody).subscribe((res) => {
            this.dialogRef.close(res === "Раздел не пустой" ? preparedBody : true)
          },err=>this.spinner = false);
          break
        }
        case 4: {
          const preparedBody = this.data.listInnerCat.map(({id,cust_id})=> ({id: id, custIdMain: cust_id}));
          this.mainService.deleteAllCategories({allSections: preparedBody}).subscribe((res) => {
            this.dialogRef.close(res.length ? res : true)
          },err=>this.spinner = false);
          break
        }
      }
      
    }
  }

  public onNoClick(){
    this.dialogRef.close(null);
  }

}

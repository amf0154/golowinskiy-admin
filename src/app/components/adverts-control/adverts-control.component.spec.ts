import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AdvertsControlComponent } from './adverts-control.component';

describe('AdvertsControlComponent', () => {
  let component: AdvertsControlComponent;
  let fixture: ComponentFixture<AdvertsControlComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvertsControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvertsControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { MainService } from '@app/core/services/main.service';
import { Router, Route, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '@app/core/services/authentication.service';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import * as moment from 'moment';
import { SpinnerService } from '@app/core/services/spinner.service';
@Component({
  selector: 'app-adverts-control',
  templateUrl: './adverts-control.component.html',
  styleUrls: ['./adverts-control.component.scss']
})
export class AdvertsControlComponent implements OnInit {

  constructor(private mainService: MainService, public spinner: SpinnerService, public formBuilder: FormBuilder, private authService: AuthenticationService, private router: Router, private route: ActivatedRoute) { }
  public advertsList = [];
  public setStatusButton = (id: number) => id == 0? false: true;
  public form = this.formBuilder.group({
    searchWord: ['']
  })
  public range = new FormGroup({
    start: new FormControl(moment().subtract(20, 'years').utc(true).toDate()),
    end: new FormControl(moment().utc(true).toDate())
  });
  public paginateConfig = {
    currentPage: 1,
    itemsPerPage: 12,
    totalItems: 0
  }
  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      if(params['page'])
        this.paginateConfig.currentPage = params['page'];
    });
    this.getAdverts();
  }

  public isLoaded: boolean = false;
  public getAdverts(page: number = 1){
    this.spinner.display(true);
    this.isLoaded = false;
    this.router.navigate(['.'], { relativeTo: this.route, queryParams: {page: page}});
    const preparedDate = {
      "Cust_ID": Number(this.authService.getUserId()),
      "StartDate": moment(this.range.controls['start'].value).subtract(20, 'years').utc(true).toISOString().split("T")[0],
      "FinishDate": moment(this.range.controls['end'].value).utc(true).toISOString().split("T")[0],
       "Search": this.form.controls['searchWord'].value
    }

    this.mainService.loadAdverts(this.paginateConfig.itemsPerPage, page, preparedDate).subscribe((res: any) => {
      this.isLoaded = true;
      this.paginateConfig.currentPage = page;
      this.advertsList = res.images;
      this.paginateConfig.totalItems = res.totalItems;
      this.spinner.display(false);
    })
  }
  public reset(){
    this.form.controls['searchWord'].setValue("");
    this.range.controls['start'].setValue(moment().subtract(20, 'years').utc(true).toDate());
    this.range.controls['end'].setValue(moment().utc(true).toDate());
    this.getAdverts();

  }
  public nextPage = () => this.getAdverts(this.paginateConfig.currentPage+1);
  public prevPage = () => this.getAdverts(this.paginateConfig.currentPage-1);

  showHideAdvert(event){
    const advertId = event.source.id;
    const status = !event.checked;
    this.mainService.showHideAdvert({Id: advertId, CustIdMain: this.authService.getUserId(), IsHid: status}).subscribe();
  }

  deleteAdvert(advert){
    const { prc_ID, appCode, cust_ID } = advert;
      this.mainService.deleteAdvert({
        cid: this.authService.getUserId(),
        appCode,
        cust_ID,
        prc_ID,
      }).subscribe(
        (res) => {
          if(res)
            this.advertsList.splice(this.advertsList.indexOf(advert),1);
        }
      )
  }

}

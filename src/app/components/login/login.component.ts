import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { SharedService } from '@app/core/services/shared.service';
import { AuthenticationService } from '@app/core/services/authentication.service';
import { DictionaryService } from '@app/core/services/dictionary.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent{
  public showStatus: boolean = false;
  public submitted: boolean = false;
  public form = this.formBuilder.group({
    userName: ['', [Validators.required, this.sharedService.emptySpacesValidator]],
    password: ['', [Validators.required]]
  });



  constructor(
    private authService: AuthenticationService, 
    private formBuilder: FormBuilder, 
    public dictionary: DictionaryService,
    private sharedService: SharedService
  ) {}


  get f() {
    return this.form.controls;
  }
  public submit() {
    this.submitted = true;
    if(!this.form.valid){
      return false
    }else{
      const {value} = this.form;
      this.authService.signIn(value);
    }
  }
}
import { Component, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from "@angular/forms";
import { ThemePalette } from "@angular/material/core";
import { AuthenticationService } from "@app/core/services/authentication.service";
import { DictionaryService } from "@app/core/services/dictionary.service";
import { MainService } from "@app/core/services/main.service";
import { SpinnerService } from "@app/core/services/spinner.service";
import { ToastrService } from "ngx-toastr";
import Swal from "sweetalert2";
import { startWith, map } from "rxjs/operators";
import { SearchClientsService } from "@app/core/services/searchClients.service";
@Component({
    selector: "app-client-catalog",
    templateUrl: "./client-catalog.component.html",
    styleUrls: ["./client-catalog.component.scss"]
})
export class ClientCatalogComponent implements OnInit {
    public submitted: boolean = false;
    public isDataLoaded: boolean = false;
    public initialCategories = new Array();
    public showCats: boolean = false;
    public categoriesForm = new FormGroup({
        categories: new FormArray([])
    });
    get categories() {
        return this.categoriesForm.controls.categories as FormArray;
    }
    //  public categories = new Array();
    public positions = [
        { value: "1", name: () => this.dictionary.getTranslate("page_adverts", "страница размещения объявлений") },
        { value: "2", name: () => this.dictionary.getTranslate("page_main", "главная страница") },
        // { value: "3", name: () => this.dictionary.getTranslate("learn_page", "страница выучить") },
        //  { value: "4", name: () => this.dictionary.getTranslate("show-cat-to-client", "показ каталога клиенту") }
        { value: "5", name: () => this.dictionary.getTranslate("restaurant", "ресторан") },
        { value: "6", name: () => this.dictionary.getTranslate("delivery", "доставка") }
    ];

    public stepHour = 1;
    public stepMinute = 1;
    public stepSecond = 1;
    public touchUi = false;
    public enableMeridian = false;
    public color: ThemePalette = "primary";

    public restaurantForm = this.formBuilder.group({
        tb: [""],
        ch: [""],
        hdate: null
    });

    public get rf() {
        return this.restaurantForm.controls;
    }

    public phoneForm = this.formBuilder.group({
        phone: [
            window.location.origin === "http://localhost:4200" ? "89163390572" : "",
            [Validators.required] // Validators.pattern("^[0-9]*$"), this.emptySpacesValidator
        ],
        position: { value: this.positions[0].value, disabled: true },
        isComShop: [
            {
                value: false,
                disabled: true
            }
        ],
        isSendRepeat: [
            {
                value: false,
                disabled: true
            }
        ],
        isRussianDoll: [
            {
                value: false,
                disabled: true
            }
        ],
        isEditing: [
            {
                value: false,
                disabled: true
            }
        ],
        isHomePageShow: [
            {
                value: false,
                disabled: true
            }
        ]
    });
    public get f() {
        return this.phoneForm.controls;
    }
    public changePosition() {
        this.fillCheckboxes(null);
        setTimeout(() => this.findData(), 100);
    }

    constructor(
        private formBuilder: FormBuilder,
        public toastr: ToastrService,
        public mainService: MainService,
        public dictionary: DictionaryService,
        public authService: AuthenticationService,
        public spinner: SpinnerService,
        public searchService: SearchClientsService
    ) {}

    ngOnInit(): void {
        this.spinner.display(true);
        this.searchService.getClients();
        this.mainService.getCategories(this.authService.getCustId(), null, "1").subscribe(
            (categories: any) => {
                this.spinner.display(false);
                this.initialCategories = Object.assign([], categories);
                categories.forEach((category) => {
                    this.categories.push(
                        this.formBuilder.group({
                            category: [category.txt],
                            checked: [category.checked],
                            hist: [false],
                            id: [category.id]
                        })
                    );
                });
                this.showCats = true;
            },
            (error) => {
                this.toastr.error(this.dictionary.getTranslate("cant_load_cats", "не могу загрузить категории"));
                this.spinner.display(false);
            },
            () => this.spinner.display(false)
        );

        this.phoneForm
            .get("phone")
            .valueChanges.pipe(
                startWith(""),
                map((value) => this.searchService._filter(value || ""))
            )
            .subscribe((r) => {
                if (r) {
                    this.searchService.filteredClients.next(r);
                }
            });
    }

    public deleteAllRepeats() {
        Swal.queue([
            {
                title: this.dictionary.getTranslate("deleting_client_repets", "Удаление повторений клиента"),
                confirmButtonText: this.dictionary.getTranslate("delete", "Удалить"),
                text: this.dictionary.getTranslate(
                    "delete_repeats_confirmation",
                    "Вы действительно хотите удалить все повторения?"
                ),
                cancelButtonText: this.dictionary.getTranslate("cancel", "отмена"),
                showCancelButton: true,
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return this.mainService
                        .deleteAllRepeatsClient(this.phoneForm.controls["phone"].value, this.authService.getCustId())
                        .subscribe((r: { result: boolean }) => {
                            Swal.fire({
                                icon: r.result ? "success" : "warning",
                                title: r.result
                                    ? this.dictionary.getTranslate(
                                          "client_repeats_cleared",
                                          "Повторения успешно удалены"
                                      )
                                    : this.dictionary.getTranslate("action_not_applied", "Действие не выполнено"),
                                showConfirmButton: false,
                                timer: 2000,
                                willClose: () => {
                                    if (r.result) {
                                        this.reset();
                                    }
                                }
                            });
                        });
                }
            }
        ]);
    }

    public getPosition() {
        return this.phoneForm.get("position").value;
    }

    public deleteAllSettings() {
        this.spinner.display(true);
        const phone = this.phoneForm.controls["phone"].value;
        this.mainService
            .updateCustomerCatalog(
                null,
                true,
                this.authService.getCustId(),
                this.phoneForm.controls["phone"].value,
                this.phoneForm.controls["position"].value,
                0,
                true
            )
            .subscribe(
                (res) => {
                    if (res.result) {
                        this.reset(phone);
                        this.findData(false);
                    } else
                        this.toastr.error(
                            this.dictionary.getTranslate("cant_save_settings", "Не удалось сохранить настройки!")
                        );
                },
                (e) => {
                    this.toastr.error(
                        this.dictionary.getTranslate("cant_save_settings", "Не удалось сохранить настройки!")
                    );
                    this.spinner.display(false);
                },
                () => this.spinner.display(false)
            );
    }

    public resetValidation() {
        this.submitted = false;
    }

    public showHideItem(i) {
        const checked = !(this.categories.controls[i] as FormGroup).controls["checked"].value;
        if (!checked && this.phoneForm.controls["position"].value == 3 && this.isFirstWarning) {
            this.confirmUncheckLearn(i);
            return;
        }
        const id = (this.categories.controls[i] as FormGroup).controls["id"].value;
        this.spinner.display(true);
        (this.categories.controls[i] as FormGroup).get("checked").setValue(checked);
        this.mainService
            .updateCustomerCatalog(
                id,
                checked,
                this.authService.getCustId(),
                this.phoneForm.controls["phone"].value,
                this.phoneForm.controls["position"].value
            )
            .subscribe(
                (res) => {
                    this.isFirstWarning = true;
                    if (res.result)
                        this.toastr.success(
                            this.dictionary.getTranslate("settings_saved_suss", "Настройки успешно сохранены!")
                        );
                    else
                        this.toastr.error(
                            this.dictionary.getTranslate("cant_save_settings", "Не удалось сохранить настройки!")
                        );
                },
                (e) => {
                    this.toastr.error(
                        this.dictionary.getTranslate("cant_save_settings", "Не удалось сохранить настройки!")
                    );
                    this.spinner.display(false);
                },
                () => this.spinner.display(false)
            );
    }

    public isFirstWarning = true;
    confirmUncheckLearn(i) {
        Swal.queue([
            {
                title: this.dictionary.getTranslate(
                    "stop-learn-warning",
                    "Снятие галочки приведет к прекращению соответствующего процесса обучения"
                ),
                confirmButtonText: this.dictionary.getTranslate("yes", "Да"),
                icon: "warning",
                text: this.dictionary.getTranslate("want_to_continue", `Вы действительно хотите продолжить?`),
                cancelButtonText: this.dictionary.getTranslate("cancel", "Отмена"),
                showCancelButton: false,
                showDenyButton: true,
                denyButtonText: this.dictionary.getTranslate("cancel", "Отмена"),
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    this.isFirstWarning = false;
                    (this.categories.controls[i] as FormGroup).get("checked").setValue(true);
                    setTimeout(() => this.showHideItem(i));
                },
                preDeny: () => {
                    (this.categories.controls[i] as FormGroup).get("checked").setValue(true);
                }
            }
        ]);
    }

    public reset(phone = "") {
        this.phoneForm.controls["phone"].setValue(phone);
        this.phoneForm.controls["isComShop"].setValue(false);
        this.phoneForm.controls["isSendRepeat"].setValue(false);
        this.phoneForm.controls["isRussianDoll"].setValue(false);
        this.phoneForm.controls["isHomePageShow"].setValue(false);
        this.phoneForm.controls["isEditing"].setValue(false);
        this.restaurantForm.reset();
        this.phoneForm.controls["phone"].enable();
        this.phoneForm.controls["position"].disable();
        this.phoneForm.controls["isHomePageShow"].disable();
        this.phoneForm.controls["isComShop"].disable();
        this.phoneForm.controls["isEditing"].disable();
        this.phoneForm.controls["isSendRepeat"].disable();
        this.phoneForm.controls["isRussianDoll"].disable();
        this.isFirstWarning = true;
        this.toastr.success(this.dictionary.getTranslate("settings_reseted", "Настройки успешно сброшены!"));
        this.fillCheckboxes(null);
        this.isDataLoaded = false;
        this.submitted = false;
    }

    public item(i) {
        return i.controls.category.value;
    }

    public hist(i) {
        return i.controls.hist.value;
    }

    public getCheckboxValueById(searchId, userData) {
        const res = userData.find((el) => el.p_id == searchId);
        return res ? res.mark : null;
    }

    public findData(showSuccessLoading = true) {
        this.submitted = true;
        if (!this.phoneForm.valid) {
            return false;
        } else {
            this.spinner.display(true);
            this.mainService
                .getCustomerCatalog(
                    this.authService.getCustId(),
                    this.phoneForm.controls["phone"].value,
                    this.phoneForm.controls["position"].value
                )
                .subscribe(
                    (customerSettings) => {
                        // customerSettings = customerSettings.map((el,i)=>({...el,hist: Boolean(i%2)}))
                        // console.log(customerSettings)
                        if (customerSettings.length === 1 && customerSettings[0].p_id === "0") {
                            const msg = this.dictionary.getTranslate(
                                "phone_not_exist_reg",
                                "Номер телефона отсутствует в базе данных. ЗАРЕГИСТРИРОВАТЬ?"
                            );
                            this.mainService.regNewUserPopup(msg, this.phoneForm.controls["phone"].value);
                        } else {
                            this.phoneForm.controls["phone"].disable();
                            this.phoneForm.controls["position"].enable();
                            this.fillCheckboxes(customerSettings);
                            if (showSuccessLoading)
                                this.toastr.success(
                                    this.dictionary.getTranslate("settings_saved", "Настройки успешно загружены!")
                                );
                            this.isDataLoaded = true;
                            if (this.getPosition() === "5") {
                                this.mainService
                                    .getRestaurantData(
                                        this.authService.getCustId(),
                                        this.phoneForm.controls["phone"].value
                                    )
                                    .subscribe((r) => {
                                        if (r) {
                                            this.restaurantForm.patchValue(r);
                                            // if(!this.$componentDestroyed) {
                                            //     this.$componentDestroyed = this.restaurantForm.valueChanges
                                            //     .subscribe(() => this.updRestaurant());
                                            // }
                                        }
                                    });
                            }
                            this.mainService
                                .getRepeatPermission(
                                    this.phoneForm.controls["phone"].value,
                                    this.authService.getCustId()
                                )
                                .subscribe(
                                    (res: {
                                        isComShop: boolean;
                                        isSendRepeat: boolean;
                                        isEditing: boolean;
                                        isRussianDoll: boolean;
                                        isHomePageShow: boolean;
                                    }) => {
                                        if (res != null) {
                                            const {
                                                isComShop,
                                                isSendRepeat,
                                                isEditing,
                                                isRussianDoll,
                                                isHomePageShow
                                            } = res;
                                            this.phoneForm.controls["isComShop"].setValue(isComShop);
                                            this.phoneForm.controls["isSendRepeat"].setValue(isSendRepeat);
                                            this.phoneForm.controls["isRussianDoll"].setValue(isRussianDoll);
                                            this.phoneForm.controls["isHomePageShow"].setValue(isHomePageShow);
                                            this.phoneForm.controls["isHomePageShow"].enable();
                                            this.phoneForm.controls["isEditing"].setValue(isEditing);
                                            this.phoneForm.controls["isComShop"].enable();
                                            this.phoneForm.controls["isSendRepeat"].enable();
                                            this.phoneForm.controls["isRussianDoll"].enable();
                                            this.phoneForm.controls["isEditing"].enable();
                                        }
                                    }
                                );
                        }
                    },
                    (e) => {
                        this.spinner.display(false);
                    },
                    () => this.spinner.display(false)
                );
        }
    }
    public resetRestaurant(): void {
        this.restaurantForm.reset();
        this.updRestaurant();
    }
    public updRestaurant(): void {
        this.mainService
            .updateRestaurantData(
                this.authService.getCustId(),
                this.phoneForm.controls["phone"].value,
                this.restaurantForm.value
            )
            .subscribe((r) => {
                this.toastr.success(
                    this.dictionary.getTranslate("settings_saved_suss", "Настройки успешно сохранены!")
                );
            });
    }

    public fillCheckboxes(customerSettings) {
        this.categories.clear();
        // customerSettings = customerSettings ? customerSettings.map((el,i)=>({...el,hist: Boolean(i%2)})) : [] // random
        const hinstItems = customerSettings ? customerSettings.filter((el)=> el.hist).map((el)=> el.p_id) : [];
       // console.log(hinstItems)
        this.initialCategories
            .map((cat) => {
                return {
                    id: cat.id,
                    txt: cat.txt,
                    checked: customerSettings ? this.getCheckboxValueById(cat.id, customerSettings) : false,
                    hist: hinstItems.includes(cat.id)
                };
            })
            .forEach((category) => {
                this.categories.push(
                    this.formBuilder.group({
                        category: [category.txt],
                        checked: [{ value: category.checked, disabled: category.checked == null }],
                        id: [category.id],
                        hist: [category.hist]
                    })
                );
            });
    }

    public emptySpacesValidator(control: FormControl): ValidationErrors {
        const value = control.value;
        if (value && value.trim().length === 0) {
            return { emptySpaces: "Введите корректное название" };
        }
        return null;
    }

    updPersSettings() {
        setTimeout(() => {
            const { isComShop, isSendRepeat, isEditing, isRussianDoll, isHomePageShow } = this.phoneForm.value;
            const prepatedBody = {
                Phone: this.phoneForm.controls["phone"].value,
                Cust_ID_Main: this.authService.getCustId(),
                IsComShop: isComShop,
                IsSendRepeat: isSendRepeat,
                IsRussianDoll: isRussianDoll,
                IsHomePageShow: isHomePageShow,
                IsEditing: isEditing
            };
            this.mainService.updateRepeatPermission(prepatedBody).subscribe(
                ({ result }) => this.toastr.success(result),
                () => this.toastr.error("Ошибка сохранения!")
            );
        });
    }
}

import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { SelectCheckboxesStateService } from "@app/core/services/select-checkboxes-state.service";
import { Enums } from "@app/core/models/enums";
@Component({
    selector: "app-checkbox",
    templateUrl: "./checkbox.component.html",
    styleUrls: ["./checkbox.component.scss"]
})
export class CheckboxComponent implements OnInit {
    private selectedItems = [];
    private arrowsForNotifyCatalogClient = [];
    private histForNotifyCatalogClient = [];
    constructor(private selectedCategoriesService: SelectCheckboxesStateService) {
        this.selectedCategoriesService.getArrowsListObserv().subscribe((res: []) => {
            this.arrowsForNotifyCatalogClient = res;
        });
        this.selectedCategoriesService.getObservableSelectedHistCategoriesList().subscribe((res: []) => {
           this.histForNotifyCatalogClient = res;
        });
    }
    @Input() isChecked: boolean;
    // отображение чекбоксов по типу отображения чекбоксов в каталоге:
    @Input() catalogCheckboxType: Enums.CatalogShowCheckboxType = Enums.CatalogShowCheckboxType.LAST_LEVEL_CATEGORY; // 1 = для элементов раздела последнего уровня; 2 = для элементов предпоследнего раздела каталога; 3 = для всех разделов;
    @Input() catalogData: { cust_id: string; id: string; listInnerCat };
    @Input() copy = false;
    @Output() status = new EventEmitter();
    @Input() isMobileVersion: boolean = false;
    checkbox: FormGroup;
    showCheckbox: boolean = false;
    ngOnInit() {
        this.main();

       // setTimeout(()=> console.log(this.checkboxElement.nativeElement.border = "1px red solid"), 1000)
    }

    public main(): void {
        if (this.catalogData) {
            switch (this.catalogCheckboxType) {
                case Enums.CatalogShowCheckboxType.LAST_LEVEL_CATEGORY: {
                    if (this.catalogData?.listInnerCat.length == 0) {
                        this.showCheckbox = true;
                    }
                    break;
                }
                case Enums.CatalogShowCheckboxType.PENULTIMATE_LEVEL_CATEGORY: {
                    if (
                        this.catalogData?.listInnerCat.length &&
                        this.catalogData.listInnerCat.some((cat) => cat.listInnerCat.length === 0)
                    ) {
                        this.showCheckbox = true;
                    }
                    break;
                }
                case Enums.CatalogShowCheckboxType.SHOW_ALL_ITEMS_EACH_CATEGORY: {
                    this.showCheckbox = true;
                    break;
                }
            }

            // if(!this.showAllCheckbox){
            //   if(this.catalogData?.listInnerCat.length == 0){
            //     this.showCheckbox = true;
            //   }
            // }else{
            //   this.showCheckbox = this.showAllCheckbox;
            // }
            this.checkbox = new FormGroup({
                id: new FormControl(this.catalogData.id),
                value: new FormControl(this.isChecked || this.selectedItems.includes(Number(this.catalogData.id)))
            });
        }

        this.selectedCategoriesService.getObservableSelectedCategoriesList().subscribe((res: []) => {
            this.selectedItems = res;
            this.checkbox = new FormGroup({
                id: new FormControl(this.catalogData.id),
                value: new FormControl(this.isChecked || this.selectedItems.includes(Number(this.catalogData.id)))
            });
        });
    }

    public onChange(): void {
        this.applyChanges(this.checkbox.value);
    }

    public applyChanges(res: any): void {
        this.selectedCategoriesService.put(Number(res.id));
    }

    get hasArrow() {
        return this.arrowsForNotifyCatalogClient.includes(Number(this.catalogData.id));
    }

    get hasHist() {
        return this.histForNotifyCatalogClient.includes(Number(this.catalogData.id));
    }
}

import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '@app/core/services/authentication.service';
import { DictionaryService } from '@app/core/services/dictionary.service';
import { PagesService } from '@app/core/services/pages.service';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent{
  public version = null;
  constructor(
    public authService: AuthenticationService,
    public pages: PagesService,
    public dictionary: DictionaryService
  ) { 
    this.version = environment.version;
  }

}

import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Models } from '@app/core/models/models';
import { AuthenticationService } from '@app/core/services/authentication.service';
import { DictionaryService } from '@app/core/services/dictionary.service';
import { SearchClientsService } from '@app/core/services/searchClients.service';
import { MapService } from '@app/map/map.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-clients-list',
  templateUrl: './edit-clients-list.component.html',
  styleUrls: ['./edit-clients-list.component.scss']
})
export class EditClientsListComponent implements OnInit {

  constructor(
    public readonly dialogRef: MatDialogRef<EditClientsListComponent>,
    private readonly toastr: ToastrService,
    public readonly dictionary: DictionaryService,
    public searchClients: SearchClientsService,
    @Inject(MAT_DIALOG_DATA) public data: Models.SearchClient
  ) {}

  public items : Models.SearchClient[];
  public deletedStack: Models.SearchClient[] = [];

  ngOnInit(): void {
    this.items = Object.assign([],this.data);
  }

  public deleteItem(client: Models.SearchClient): void {
    this.searchClients.deleteClient(client.cust_ID).subscribe((res: {result: boolean})=> {
      if(res.result) {
        this.toastr.success(this.dictionary.getTranslate("client-deleted-success","Клиент успешно удален"));
        this.dialogRef.close(true); 
      }else {
        this.toastr.success("Ошибка удаления");
      }
    })

    // const element = this.items[i];
    // element.cust_ID_Main = this.authService.getCustId();
    // this.deletedStack.push(element);
    // this.items.splice(i,1);
  }

  // public editItem(item: Models.SearchClient){
  //   this.mapService.editPoint(item);
  //   this.dialogRef.close();
  // }

  // public save(): void {
  //   if(!this.deletedStack.length){
  //     this.toastr.error(this.dictionary.getTranslate("select-points-for-deleting","Выберите элементы для удаления"));
  //     return;
  //   } else {
  //     this.searchClients.deleteClient(this.deletedStack).subscribe(()=> {
  //       this.toastr.success(this.dictionary.getTranslate("map-points-deleted","Выбранные точки успешно удалены"));
  //       this.dialogRef.close(true);
  //     })
  //   }
  // }


}

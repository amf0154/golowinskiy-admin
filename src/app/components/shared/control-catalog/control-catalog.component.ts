import { Component, ElementRef, Input, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { CategoryItem } from "@app/core/models/category-item";
import { Enums } from "@app/core/models/enums";
import { AuthenticationService } from "@app/core/services/authentication.service";
import { CategoriesService } from "@app/core/services/categories.service";
import { DictionaryService } from "@app/core/services/dictionary.service";
import { MainService } from "@app/core/services/main.service";
import { SearchClientsService } from "@app/core/services/searchClients.service";
import { SelectCheckboxesStateService } from "@app/core/services/select-checkboxes-state.service";
import { SharedService } from "@app/core/services/shared.service";
import { SpinnerService } from "@app/core/services/spinner.service";
import { StorageService } from "@app/core/services/storage.service";
import { ToastrService } from "ngx-toastr";
import { BehaviorSubject, Observable, Subject, Subscription } from "rxjs";
import { startWith, map, takeUntil } from "rxjs/operators";
@Component({
    selector: "control-catalog",
    templateUrl: "./control-catalog.component.html",
    styleUrls: ["./control-catalog.component.scss"]
})
export class ControlCatalogComponent implements OnInit, OnDestroy {
    @ViewChild("navdrop") navdrop: ElementRef;
    @Input() parameter: number;
    @Input() position: number = 1;
    @Input() disablePosition?: boolean = false;
    @Input() delAllButton: boolean = false;
    public destroyComponent$ = new Subject();
    public showCatalog = true;
    public categories: CategoryItem[] = [];
    public submitted: boolean = false;
    public isDataLoaded: boolean = false;
    public initialCategories = new Array();
    public showCats: boolean = false;
    public refreshCat: boolean = false;
    public previousItem: {} = null;
    public selectionTypeMode: Enums.CatalogShowCheckboxType = Enums.CatalogShowCheckboxType.LAST_LEVEL_CATEGORY;
    public selectedCategory: any = null;
    public learnCatalogTypes = Enums.LearnCatalogType;
    public positions = [
        { value: "1", name: () => this.dictionary.getTranslate("page_adverts", "страница размещения объявлений") },
        { value: "2", name: () => this.dictionary.getTranslate("page_main", "главная страница") }
    ];
    private reloadCatalog = () => {
        this.showCatalog = false;
        this.findData();
        setTimeout(() => (this.showCatalog = true));
    };
    private hasTest = window.location.origin.startsWith("http://localhost");
    public phoneForm: FormGroup;

    public get f() {
        return this.phoneForm.controls;
    }

    constructor(
        private formBuilder: FormBuilder,
        public toastr: ToastrService,
        public mainService: MainService,
        public authService: AuthenticationService,
        public dictionary: DictionaryService,
        public categoriesService: CategoriesService,
        public sharedService: SharedService,
        public storageService: StorageService,
        public searchService: SearchClientsService,
        public selectCheckboxesService: SelectCheckboxesStateService,
        public spinner: SpinnerService
    ) {
        if (!localStorage.getItem("catalogType")) {
            localStorage.setItem("catalogType", "department");
        }

        this.selectCheckboxesService
            .getSelectedStackForSave()
            .pipe(takeUntil(this.destroyComponent$))
            .subscribe((res) => {
                if (Object.keys(res).length !== 0 && this.isDataLoaded) {
                    const id = Number(res.id);
                    if (res.value) {
                        if (!this.selectedCategoryIds.includes(id)) {
                            this.selectedCategoryIds.push(id);
                        }
                    } else {
                        if (this.selectedCategoryIds.includes(id)) {
                            const removedIndex = this.selectedCategoryIds.indexOf(id);
                            if (removedIndex != -1) this.selectedCategoryIds.splice(removedIndex, 1);
                        }
                    }
                    this.saveData(res);
                }
            });
    }

    public openSort() {
        this.navdrop.nativeElement.classList.toggle("visibility");
    }

    public changePosition() {
        this.fillCheckboxes(null);
        setTimeout(() => this.findData(), 100);
    }

    public deleteAllSettings() {
        this.spinner.display(true);
        this.mainService
            .updateCustomerCatalog(
                null,
                true,
                this.authService.getCustId(),
                this.phoneForm.controls["phone"].value,
                this.phoneForm.controls["position"].value,
                this.parameter,
                true
            )
            .subscribe(
                (res) => {
                    if (res.result) {
                        this.selectedCategoryIds.length = 0;
                        this.findData(false);
                    } else
                        this.toastr.error(
                            this.dictionary.getTranslate("cant_save_settings", "Не удалось сохранить настройки!")
                        );
                },
                (e) => {
                    this.toastr.error(
                        this.dictionary.getTranslate("cant_save_settings", "Не удалось сохранить настройки!")
                    );
                    this.spinner.display(false);
                },
                () => this.spinner.display(false)
            );
    }

    private initForm() {
        this.phoneForm = this.formBuilder.group({
            // Validators.pattern("^[0-9]*$"), this.sharedService.emptySpacesValidator
            phone: [this.hasTest ? "89163390572" : "", [Validators.required]],
            position: { value: this.positions[this.position].value, disabled: true },
            catalogType: [
                !!localStorage.getItem("catalogType") &&
                [
                    this.learnCatalogTypes.DAILY,
                    this.learnCatalogTypes.DEPARTMENT,
                    this.learnCatalogTypes.SINGLE
                ].includes(localStorage.getItem("catalogType") as any)
                    ? localStorage.getItem("catalogType")
                    : this.learnCatalogTypes.DEPARTMENT
            ]
        });

        this.phoneForm
            .get("phone")
            .valueChanges.pipe(
                takeUntil(this.destroyComponent$),
                startWith(""),
                map((value) => this.searchService._filter(value || ""))
            )
            .subscribe((r) => {
                if (r) {
                    this.searchService.filteredClients.next(r);
                }
            });
        this.phoneForm
            .get("catalogType")
            .valueChanges.pipe(takeUntil(this.destroyComponent$))
            .subscribe((value: Enums.LearnCatalogType) => {
                localStorage.setItem("catalogType", value);
                this.selectionTypeMode = [this.learnCatalogTypes.SINGLE, this.learnCatalogTypes.DAILY].includes(value)
                    ? Enums.CatalogShowCheckboxType.LAST_LEVEL_CATEGORY
                    : Enums.CatalogShowCheckboxType.PENULTIMATE_LEVEL_CATEGORY;
                this.reloadCatalog();
            });
    }

    private switchLearnPageParam = (value) => {
        if (this.parameter == 4 && localStorage.getItem("catalogType") == "single") {
            return "4";
        }
        if(localStorage.getItem("catalogType") == this.learnCatalogTypes.DAILY) {
            return "7";
        }
        return value;
    };

    ngOnInit(): void {
        switch (Number(this.parameter)) {
            case 4: {
                // LEARN page
                this.selectionTypeMode =
                    localStorage.getItem("catalogType") !== "department"
                        ? Enums.CatalogShowCheckboxType.LAST_LEVEL_CATEGORY
                        : Enums.CatalogShowCheckboxType.PENULTIMATE_LEVEL_CATEGORY;
                break;
            }
            default: {
                this.selectionTypeMode = Enums.CatalogShowCheckboxType.LAST_LEVEL_CATEGORY;
                break;
            }
        }
        // if (this.position == 0) {
        //     this.positions.push({
        //         value: "4",
        //         name: () => this.dictionary.getTranslate("learn_page", "страница выучить")
        //     });
        // }
        if (this.position == 2) {
            this.positions.push({
                value: "3",
                name: () => this.dictionary.getTranslate("learn_page", "страница выучить")
            });
        }
        this.initForm();
        this.searchService.getClients();
        if (this.parameter) {
            this.categoriesService.fetchCategoriesAll(this.authService.getCustId(), null, "1");
            this.categoriesService.mainCategories$.subscribe((categories) => {
                if (Array.isArray(categories) && categories.length !== 0) {
                    this.initialCategories = Object.assign([], categories);
                    this.showCats = true;
                }
            });
        } else {
            this.toastr.error(
                this.dictionary.getTranslate("no_parameter", "не задан ключевой параметр для работы с каталогом")
            );
        }
    }

    ngOnDestroy(): void {
        this.destroyComponent$.next();
        this.destroyComponent$.complete();
    }

    onCategoriesClick(items: CategoryItem[]) {
        if (!this.previousItem) {
            this.previousItem = items[items.length - 1];
        } else {
            if (JSON.stringify(this.previousItem) !== JSON.stringify(items[items.length - 1])) {
                this.previousItem = items[items.length - 1];
            }
        }
        this.categories = items;
        const item = items[items.length - 1];
        this.selectedCategory = item;
    }

    public resetValidation() {
        this.submitted = false;
    }

    public saveData({ id, value }) {
        this.spinner.display(true);
        this.mainService
            .updateCustomerCatalog(
                id,
                value,
                this.authService.getCustId(),
                this.phoneForm.controls["phone"].value,
                this.switchLearnPageParam(this.phoneForm.controls["position"].value),
                this.parameter
            )
            .subscribe(
                (res) => {
                    if (res.result)
                        this.toastr.success(
                            this.dictionary.getTranslate("settings_saved_suss", "Настройки успешно сохранены!")
                        );
                    else
                        this.toastr.error(
                            this.dictionary.getTranslate("cant_save_settings", "Не удалось сохранить настройки!")
                        );
                },
                (e) => {
                    this.toastr.error(
                        this.dictionary.getTranslate("cant_save_settings", "Не удалось сохранить настройки!")
                    );
                    this.spinner.display(false);
                },
                () => this.spinner.display(false)
            );
    }

    public reset() {
        this.phoneForm.controls["phone"].setValue("");
        this.phoneForm.controls["phone"].enable();
        this.phoneForm.controls["position"].disable();
        this.toastr.success(this.dictionary.getTranslate("settings_reseted", "Настройки успешно сброшены!"));
        this.fillCheckboxes(null);
        this.isDataLoaded = false;
        this.parentNameSelectedCat = "";
        this.previousSelected = "";
        this.selectedCatalogChildrens.length = 0;
        this.selectedCategoryIds = [];
        this.previousSelected = null;
        this.submitted = false;
    }

    public findData(validate = true) {
        this.submitted = true;
        if (!this.phoneForm.valid && validate) {
            return false;
        } else {
            this.spinner.display(true);
            this.mainService
                .getCustomerCatalogNew(
                    this.authService.getCustId(),
                    this.phoneForm.controls["phone"].value,
                    this.switchLearnPageParam(this.phoneForm.controls["position"].value),
                    this.parameter
                )
                .subscribe(
                    (customerSettings) => {
                        if (customerSettings) {
                            this.phoneForm.controls["phone"].disable();
                            if (!this.disablePosition) this.phoneForm.controls["position"].enable();
                            this.fillCheckboxes(customerSettings);
                            this.toastr.success(
                                this.dictionary.getTranslate("settings_saved", "Настройки успешно загружены!")
                            );
                            this.isDataLoaded = true;
                        }
                    },
                    (e) => {
                        const { error, status } = e;
                        this.toastr.error(error.p_id);
                        this.spinner.display(false);
                    },
                    () => this.spinner.display(false)
                );
        }
    }

    public selectedCategoryIds = [];
    public fillCheckboxes(customerSettings) {
        const histItems = customerSettings ? customerSettings.filter((el) => el.hist).map((el) => el.id) : [];
        const selectedCategoryIds = customerSettings
            ? customerSettings.filter((el) => el.mark === 1).map((el) => el.id)
            : [null];
        const arrowIds = customerSettings ? customerSettings.filter((el) => el.mark === 2).map((el) => el.id) : [];
        this.selectCheckboxesService.setArrowsList(arrowIds);
        this.selectedCategoryIds = selectedCategoryIds;
        this.selectCheckboxesService.setSelectedCategoriesList(this.selectedCategoryIds);
        this.selectCheckboxesService.setHistCategoriesList(histItems);
    }

    private previousSelected = "";
    public parentNameSelectedCat = "";
    public selectedCatalogChildrens = [];
    click(item) {
        const { listInnerCat: data, txt } = item;
        this.parentNameSelectedCat = txt;
        if (this.isDataLoaded) {
            if (JSON.stringify(this.previousSelected) !== JSON.stringify(data)) {
                this.previousSelected = data;
            } else {
                this.parentNameSelectedCat = "";
                this.previousSelected = null;
            }
        }
    }

    contextMenuEvents({ action, data }) {
        const childItems = data.map((el) => Number(el.id));
        switch (action) {
            case "select": {
                childItems.forEach((el) => {
                    if (!this.selectedCategoryIds.includes(el)) {
                        this.selectedCategoryIds.push(el);
                    }
                });
                this.selectAllchildCats(true, childItems);
                this.selectCheckboxesService.setSelectedCategoriesList(this.selectedCategoryIds);
                break;
            }
            case "unselect": {
                childItems.forEach((el) => {
                    const index = this.selectedCategoryIds.indexOf(el);
                    if (index != -1) this.selectedCategoryIds.splice(index, 1);
                });
                this.selectAllchildCats(false, childItems);
                this.selectCheckboxesService.setSelectedCategoriesList(this.selectedCategoryIds);
                break;
            }
            default:
                null;
        }
    }

    selectAllchildCats(mark: boolean, childItems) {
        const preparedBody = childItems.map((id) =>
            this.getBodyForUpdate(
                id,
                mark,
                this.authService.getCustId(),
                this.phoneForm.controls["phone"].value,
                this.switchLearnPageParam(this.phoneForm.controls["position"].value),
                this.parameter
            )
        );
        this.spinner.display(true);
        this.mainService.updateCustomerCatalogArr(preparedBody).subscribe(
            (res) => {
                if (res.result)
                    this.toastr.success(
                        this.dictionary.getTranslate("settings_saved_suss", "Настройки успешно сохранены!")
                    );
                else
                    this.toastr.error(
                        this.dictionary.getTranslate("cant_save_settings", "Не удалось сохранить настройки!")
                    );
            },
            (e) => {
                this.toastr.error(
                    this.dictionary.getTranslate("cant_save_settings", "Не удалось сохранить настройки!")
                );
                this.spinner.display(false);
            },
            () => this.spinner.display(false)
        );
    }

    getBodyForUpdate(id, mark, custId, phone, position, parameter) {
        return {
            id: id,
            mark: mark,
            cust_ID_Main: custId,
            phone: phone,
            place: position,
            page: parameter
        };
    }
}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DictionaryService } from '@app/core/services/dictionary.service';

@Component({
  selector: 'change-language',
  templateUrl: './change-language.component.html',
  styleUrls: ['./change-language.component.scss']
})
export class ChangeLanguageComponent {
  public languageForm: FormGroup;
  constructor(
    public locale: DictionaryService,
    private fb: FormBuilder,
  ) { 
    const {value} = this.locale.selectedLocalize
    const $locale = localStorage.getItem("locale");
    this.languageForm = this.fb.group({
      code: [value ? value : $locale, [Validators.required]],
    });
    this.languageForm.valueChanges.subscribe(res=>{
      this.locale.changeLanguage(res.code);
    })
  }

}

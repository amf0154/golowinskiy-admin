import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, NavigationStart, Router } from '@angular/router';
import { DictionaryService } from '@app/core/services/dictionary.service';
import { PagesService } from '@app/core/services/pages.service';
import { SharedService } from '@app/core/services/shared.service';

@Component({
  selector: 'app-titles',
  templateUrl: './titles.component.html',
  styleUrls: ['./titles.component.scss']
})
export class TitlesComponent {
  public route: any = null;
  constructor(
    private router: Router,
    public pages: PagesService,
    public shared: SharedService,
    public dictionary: DictionaryService
  ) {
    router.events.subscribe((currRoute) => {
      if(currRoute instanceof NavigationEnd) {
        this.route = pages.$routes.find((r)=> {
          return r.route === currRoute.url.split('?')[0]
        })
      }    
    });
  }

}

import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { Observable } from 'rxjs';
import { DictionaryService } from '@app/core/services/dictionary.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  test: string = "applications";
  public isAuthenticated: Observable<boolean>;
  constructor(
    private authService: AuthenticationService,
    public local: DictionaryService
  ) { }

  ngOnInit() {
    this.isAuthenticated = this.authService.isAuthenticatedObservable();
  }

}

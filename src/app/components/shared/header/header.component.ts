import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { AdminLogin } from 'src/app/core/models/user';
import { Observable } from 'rxjs';
import { ProgressbarService } from '@app/core/services/progressbar.service';
import { MatDialog } from '@angular/material/dialog';
import { AdminLoginComponent } from '@app/components/admin-login/admin-login.component';
import { map, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { PagesService } from '@app/core/services/pages.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public isAuthenticated: Observable<boolean>;
  public userData: Observable<AdminLogin>;
  public isNotSuperAdmin: Observable<boolean>;
  public host = "";
  constructor(
    public authService: AuthenticationService, 
    public progress: ProgressbarService,
    public toastr: ToastrService,
    public pages: PagesService,
    public dialog: MatDialog) { 
    this.userData =  this.authService.userDataObservable();
    this.host = window.location.host.split(".").slice(-2).join('.');
  }

  ngOnInit() {
    this.isAuthenticated = this.authService.isAuthenticatedObservable();
    this.isNotSuperAdmin = this.authService.isSuperAdmin().pipe(map(res => !res));
  }


  closeMenu(){
    if(window.innerWidth <= 991)
      document.getElementById("pushMenu").click();
  }

  superLogin(){
    this.dialog.open(AdminLoginComponent, {
      width: '400px',
      data: [],
    }).afterClosed().pipe(
      map(response => {
        if(response){
          this.toastr.success("Вы успешно авторизовались как суперадминистратор!");
        }
        this.authService.setSuperAdminStatus(response);
        return response
      })
    )
  }

}

import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AdminLoginComponent } from '@app/components/admin-login/admin-login.component';
import { AuthenticationService } from '@app/core/services/authentication.service';
import { DictionaryService } from '@app/core/services/dictionary.service';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  public isAuthenticated: Observable<boolean>;
  public isNotSuperAdmin: Observable<boolean>;
  constructor(
    public authService: AuthenticationService,
    private dialog: MatDialog,
    private toastr: ToastrService,
    public dictionary: DictionaryService, 
  ) { }

  ngOnInit(): void {
    this.isAuthenticated = this.authService.isAuthenticatedObservable();
    this.isNotSuperAdmin = this.authService.isSuperAdmin().pipe(map(res => !res));
  }

  superLogin(){
    this.dialog.open(AdminLoginComponent, {
      width: '400px',
      data: [],
    }).afterClosed().pipe(
      map(response => {
        if(response){
          this.toastr.success("Вы успешно авторизовались как суперадминистратор!");
        }
        this.authService.setSuperAdminStatus(response);
        return response
      })
    )
  }

}

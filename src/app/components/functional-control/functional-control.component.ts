import { Component, OnInit } from "@angular/core";
import { FormArray, FormBuilder, FormControl, FormGroup, ValidationErrors } from "@angular/forms";
import { AuthenticationService } from "@app/core/services/authentication.service";
import { DictionaryService } from "@app/core/services/dictionary.service";
import { MainService } from "@app/core/services/main.service";
import { SpinnerService } from "@app/core/services/spinner.service";
import { ToastrService } from "ngx-toastr";
import Swal from "sweetalert2";
import { SearchClientsService } from "@app/core/services/searchClients.service";
import { PermissionService } from "@app/core/services/permission.service";
import { ActivatedRoute } from "@angular/router";
import { Enums } from "@app/core/models/enums";
@Component({
    selector: "app-functional-control",
    templateUrl: "./functional-control.component.html",
    styleUrls: ["./functional-control.component.scss"]
})
export class FunctionalControlComponent implements OnInit {
    public editMode: boolean = false;
    public showPage = false;
    public permissionForm = this.formBuilder.group({
        departments: this.formBuilder.array([])
    });
    public clientLocalization: Map<any, any>;
    get departments() {
        return this.permissionForm.get("departments") as FormArray;
    }

    constructor(
        private formBuilder: FormBuilder,
        public toastr: ToastrService,
        public mainService: MainService,
        public dictionary: DictionaryService,
        public authService: AuthenticationService,
        public spinner: SpinnerService,
        public searchService: SearchClientsService,
        private route: ActivatedRoute,
        private permissionService: PermissionService
    ) {
        route.data.subscribe((data: { localization: Map<any, any> }) => {
            this.clientLocalization = data.localization;
        });
    }

    ngOnInit(): void {
        this.permissionService.permissionList.asObservable().subscribe(
            (items: any) => {
                this.spinner.display(false);
                const categoryItems = items.reduce((item, cur) => {
                    const catName = cur.alias.split("-")[0];
                    if (item[catName]) {
                        item[catName].push(cur);
                    } else {
                        item[catName] = [cur];
                    }
                    return item;
                }, {});

                // reset
                while (this.departments.length) {
                    this.departments.removeAt(0);
                }

                for (let item in categoryItems) {
                    this.departments.push(
                        this.formBuilder.group({
                            name: this.getDepartmentName(item as Enums.PermissionDepartmentType),
                            items: this.formBuilder.array(categoryItems[item].map((el) => this.formBuilder.group(el)))
                        })
                    );
                }
            },
            (error) => {
                this.toastr.error(this.dictionary.getTranslate("cant_load_cats", "не могу загрузить категории"));
                this.spinner.display(false);
            },
            () => this.spinner.display(false)
        );
    }

    public getDepartmentItems(i: number) {
        return this.departments[i].get("items") as FormArray;
    }
    public getDepartmentName(name: Enums.PermissionDepartmentType) {
        switch (name) {
            case Enums.PermissionDepartmentType.MAIN:
                return "Админка (Главная страница)";
            case Enums.PermissionDepartmentType.SETTINGS:
                return "Админка (Внешние данные магазина)";
            case Enums.PermissionDepartmentType.CLIENT:
                return "Меню (Клиентская часть)";
        }
    }

    addAlias() {
        Swal.fire({
            title: "Создание нового аляса",
            cancelButtonText: "Отмена",
            showCancelButton: true,
            html:
                '<select id="departmentType" name="departmentType">' +
                `<option value="${Enums.PermissionDepartmentType.CLIENT}">КЛИЕНТ</option>` +
                `<option value="${Enums.PermissionDepartmentType.MAIN}">АДМИНКА (Меню админки)</option>` +
                `<option selected value="${Enums.PermissionDepartmentType.SETTINGS}">Админка (Внешние данные магазина)</option>` +
                "</select>" +
                '<input id="alias-input1" type="text" placeholder="имя аляса(латиница)" class="swal2-input">' +
                '<input id="translate-input3" type="text" placeholder="аляс локализации" class="swal2-input">',
            didOpen: function () {
                document.getElementById("alias-input1").focus();
            },
            preConfirm: () => {
                const department = (document.getElementById("departmentType") as any).value + "-";
                return this.mainService
                    .addDelControlFuncItem({
                        alias: department + (document.getElementById("alias-input1") as HTMLInputElement).value,
                        txt: (document.getElementById("translate-input3") as HTMLInputElement).value,
                        mark: true
                    })
                    .toPromise()
                    .then((res) => {
                        Swal.fire({
                            title: `Аляс успешно добавлен`,
                            showConfirmButton: false,
                            timer: 1200,
                            willClose: () => {
                                this.permissionService.updatePermissionList(this.authService.getCustId());
                            }
                        });
                        return res;
                    })
                    .catch((error) => {
                        Swal.showValidationMessage(`Ошибка: ${error}`);
                    });
            },
            allowOutsideClick: () => !Swal.isLoading()
        });
    }

    public getItemName(item: any) {
        const name = item.get("txt").value;
        const alias = item.get("alias").value;
        if (alias.startsWith("client")) {
            return this.clientLocalization.has(name) ? this.clientLocalization.get(name) : name;
        }
        return this.dictionary.getTranslate(name, name);
    }

    public enableDisableItem(i) {
        setTimeout(() => {
            const item = i.value;
            this.spinner.display(true);
            this.mainService
                .updControlFuncList({
                    cust_ID_Main: this.authService.getCustId(),
                    alias: item.alias,
                    isShow: item.isShow
                })
                .subscribe(
                    (res) => {
                        if (res.success) {
                            this.toastr.success(
                                this.dictionary.getTranslate("settings_saved_suss", "Настройки успешно сохранены!")
                            );
                            this.permissionService.updatePermissionList(this.authService.getCustId());
                        } else
                            this.toastr.error(
                                this.dictionary.getTranslate("cant_save_settings", "Не удалось сохранить настройки!")
                            );
                    },
                    (e) => {
                        this.toastr.error(
                            this.dictionary.getTranslate("cant_save_settings", "Не удалось сохранить настройки!")
                        );
                        this.spinner.display(false);
                    },
                    () => this.spinner.display(false)
                );
        });
    }

    public deleteItem(item): void {
        const { alias, txt } = item.value;

        this.mainService
            .addDelControlFuncItem({
                alias,
                txt,
                mark: false
            })
            .subscribe((r) => {
                this.permissionService.updatePermissionList(this.authService.getCustId());
            });
    }
}

// [
//     { alias: "client-copy-as-list", txt: "copy_list", isShow: true },
//     { alias: "client-delete-repeat", txt: "delRepeat", isShow: true },
//     { alias: "client-doll", txt: "doll", isShow: true },
//     { alias: "client-move-adverts", txt: "cat_move", isShow: true },
//     { alias: "client-subscription", txt: "subscription", isShow: true },
//     { alias: "main-account-login", txt: "mob_data", isShow: true },
//     { alias: "main-background", txt: "bg_images", isShow: true },
//     { alias: "main-cart", txt: "cart", isShow: true },
//     { alias: "main-catalog", txt: "show_cat_client", isShow: true },
//     { alias: "main-catalog-notify", txt: "notify_settings", isShow: true },
//     { alias: "main-catalog-sorts", txt: "catalog-sorts", isShow: true },
//     { alias: "main-categories", txt: "create_cat", isShow: true },
//     { alias: "main-commodity/new", txt: "create_adv", isShow: true },
//     { alias: "main-learn-catalog-client", txt: "learn-page", isShow: true },
//     { alias: "main-manual", txt: "site-manual", isShow: true },
//     { alias: "main-map", txt: "map", isShow: true },
//     { alias: "main-settings", txt: "shop_info", isShow: true },
//     { alias: "main-show-catalog-client", txt: "show_cat_to_customer", isShow: true },
//     { alias: "settings-accept-access-to-main-page", txt: "accept-access-to-main-page", isShow: true },
//     { alias: "settings-accept-moving-adverts", txt: "accept-moving-adverts", isShow: true },
//     { alias: "settings-old-view-make-advert", txt: "old-view-make-advert", isShow: true },
//     { alias: "settings-show-links-media", txt: "show-links-media", isShow: true },
//     { alias: "settings-show-map", txt: "show-map", isShow: true },
//     { alias: "settings-show-slideshow-button", txt: "show-slideshow-button", isShow: true },
//     { alias: "settings-site-sort", txt: "site-sort", isShow: true },
//     { alias: "settings-subscribe-to-mail", txt: "subscribe-to-mail", isShow: true }
// ];

import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { MainService } from '@app/core/services/main.service';


@Injectable()
export class LocaleClientResolver implements Resolve<any> {
  constructor(public mainService: MainService, private router: Router) {}

  public fillDictionary(item,localize: string,dictionaryDB){
    if(item[localize])
      dictionaryDB.set(item.ALIAS,item[localize]); 
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any> {
    return new Promise((res,rej)=>{
        this.mainService.locale.getLocales(1).subscribe((response: any) => {
          let db =  new Map();
          response.data.forEach((el)=>{
            this.fillDictionary(el,'RU',db);
          })
          res(db);

          
        })
    })
  }
}
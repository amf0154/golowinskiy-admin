import { Component, OnInit } from '@angular/core';
import { StorageService } from 'src/app/core/services/storage.service';
import { CategoryItem } from 'src/app/core/models/category-item';
import { MainService } from 'src/app/core/services/main.service';
import { CategoriesService } from 'src/app/core/services/categories.service';
import { AdvertList } from 'src/app/core/models/advert-list';
import { ProgressbarService } from 'src/app/core/services/progressbar.service';
import { AuthenticationService } from 'src/app/core/services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-adverts',
  templateUrl: './adverts.component.html',
  styleUrls: ['./adverts.component.scss']
})
export class AdvertsComponent implements OnInit {
  public initialCategories: CategoryItem[] = [];
  public categories: CategoryItem[] = [];
  public paginateConfig = {
    currentPage: 1,
    itemsPerPage: 20,
    totalItems: 0
  }
  constructor(private storageService: StorageService, private router: Router, private route: ActivatedRoute, private mainService: MainService,public progress: ProgressbarService, public categoriesService: CategoriesService, public authService: AuthenticationService) { }

  ngOnInit() {
    this.categoriesService.fetchCategoriesAll(this.authService.getCustId());
    this.route.queryParams.subscribe(params => {
      if(params['page'])
        this.paginateConfig.currentPage = params['page'];
    });
  }
  previousItem: {} = null;
  selectedCategory: any = null;
  onCategoriesClick(items: CategoryItem[]) {
  //  this.storageService.setCategories(items)
  //  this.mainService.saveCategoriesToStorage(items)
    if(!this.previousItem){
      this.previousItem = items[items.length -1];
    }else{
      if(JSON.stringify(this.previousItem) !== JSON.stringify(items[items.length -1])){
        this.previousItem = items[items.length -1];
      }
    }
    this.categories = items;
    const item = items[items.length - 1]
    this.selectedCategory = item;
    this.advertItems.length = 0;
    this.getGallery();
  }

  refreshCat: boolean = false;
  breadcrumbsClick(i) {
    this.storageService.setCategories(this.categories.slice(0,i))
    this.categories = []
    this.storageService.breadcrumbFlag = true
    this.initialCategories = this.storageService.getCategories();
    this.refreshCat = true;
    setTimeout(()=>this.refreshCat = false);  
  }

  previousCategoryId: number = null;
  advertItems: Array<AdvertList> = [];
  getGallery(page: number = 1){
    this.router.navigate(['.'], { relativeTo: this.route, queryParams: {page: page}});
    if(this.previousCategoryId !== this.selectedCategory.id){
      this.paginateConfig.currentPage = 1;
    }
    this.progress.setStatus(true);
    this.mainService.getProductsPaginate(false, this.selectedCategory.id, this.selectedCategory.cust_id, '',this.paginateConfig.itemsPerPage,page).subscribe((res) => {
      this.paginateConfig.currentPage = page;
      this.advertItems = res.images;
      setTimeout(()=>this.progress.setStatus(false),300)
      this.previousCategoryId = this.selectedCategory.id;
      this.paginateConfig.totalItems = res.totalItems;
    },error=>alert(error.message))
  }

  pageChange = (page: number = 1) => this.getGallery(page);
  nextPage = () => this.getGallery(this.paginateConfig.currentPage+1);
  prevPage = () => this.getGallery(this.paginateConfig.currentPage-1);

  deleteItem(advert){
    this.progress.setStatus(true);
    const { prc_ID, appCode, cust_ID } = advert;
      this.mainService.deleteProduct({
        cid: this.authService.getUserId(),
        appCode,
        cust_ID,
        prc_ID,
      }).subscribe(
        (res) => {
          setTimeout(()=>this.progress.setStatus(false),300);
          if(res)
            this.advertItems.splice(this.advertItems.indexOf(advert),1);
          else
            alert('Произошла ошибка удаления товара')
        }
      )
  }

  categorySelect(items: CategoryItem[]) {
    if(!this.previousItem){
      this.previousItem = items[items.length -1];
    }else{
      if(JSON.stringify(this.previousItem) !== JSON.stringify(items[items.length -1])){
        this.previousItem = items[items.length -1];
     //   this.breadcrumbsClick();
      }
    }
    this.categories = items;
    const item = items[items.length - 1]
    this.selectedCategory = item;
    this.advertItems.length = 0;
    this.getGallery();

  }

}

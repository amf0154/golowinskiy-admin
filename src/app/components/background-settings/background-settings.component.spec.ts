import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BackgroundSettingsComponent } from './background-settings.component';

describe('BackgroundSettingsComponent', () => {
  let component: BackgroundSettingsComponent;
  let fixture: ComponentFixture<BackgroundSettingsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BackgroundSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackgroundSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

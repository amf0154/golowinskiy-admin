import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from '@app/core/services/authentication.service';
import { MainService } from '@app/core/services/main.service';
import Swal from 'sweetalert2';
import { DictionaryService } from '@app/core/services/dictionary.service';
@Component({
  selector: 'app-background-settings',
  templateUrl: './background-settings.component.html',
  styleUrls: ['./background-settings.component.scss']
})
export class BackgroundSettingsComponent implements OnInit {

  constructor(
    private mainService: MainService,
    public fb: FormBuilder,
    public dictionary: DictionaryService,
    public authService: AuthenticationService
  ) {}

  public buildBase64ImageLink = (imageLink: string) => {
    return "data:image/png;base64," + imageLink;
  };

  public getOrientationWidth(orientation: string){
    switch(orientation){
      case 'H': {
        return '300px'
      };
      case 'V': {
        return '140px';
      };
      default: {
        return '200px';
      }
    }
  }
  public isUploading: boolean = false;
  public selectedBackground: boolean = false;
  public limitOfUploadingImages = 5;
  public isLimitReached: boolean = false;
  public orientations  = [
    { name: this.dictionary.getTranslate('horiz_desktop','горизонтальная (для дектоп.версии)'), value:'H' },  
    { name: this.dictionary.getTranslate('vert_desktop','вертикальная (для моб.версии)'), value:'V' }
  ];
  public places  = [
    { name: this.dictionary.getTranslate('page_personal','личный кабинет'), value:'L' },  
    { name: this.dictionary.getTranslate('page_main','главная страница'), value:'G' }
  ];
  public bgConfiguration = this.fb.group({
    orientation: [this.orientations[0].value, Validators.required],
    place: [this.places[0].value, Validators.required],
  });
  public backgrounds = [];
  public cust_id = null;
  ngOnInit() {
    this.cust_id = this.authService.getCustId();
    this.uploadBackgrounds();
  }
  public isUpdating: boolean = false;
  private uploadBackgrounds(){
    this.isUpdating = true;
    this.mainService.downloadBackgrounds(
      this.cust_id,1,
      this.bgConfiguration.controls['orientation'].value,
      this.bgConfiguration.controls['place'].value
    ).subscribe(
      (res: any)=>{
        this.backgrounds = res;
        if(res.length === this.limitOfUploadingImages){
          this.isLimitReached = true;
        }else{
          this.isLimitReached = false;
        }
      },()=>{},()=> this.isUpdating = false
    )
  }

  public deleteSelectedBackground(){
    this.croppedImage = null;
    this.file = null;
    this.imageChangedEvent = null;
    this.clearImgStack();
  }


  public imageChangedEvent: any = '';
  public croppedImage: any = null;
  public file;
  public showImageParams: boolean = false;
  public clearImgStack;
  fileChangeEvent(event: any): void {
    this.file = new File(event.target.files,this.uuidv4() + '.png')
   // this.file.name = this.uuidv4() + '.png';
  //    this.imageChangedEvent = event;
      this.clearImgStack = () => {
        event.target.value = ''
      }
  }

  private uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  // newFile
  // imageCropped(event: ImageCroppedEvent) {
  //   //  this.croppedImage = event.base64;
  //     this.dataURLtoFile(event.base64, this.file.name).then((file: File)=>{
  //       getBase64Strings([file], { maxSize: 1200 }).then(res=>{
  //         fetch(res[0])
  //         .then(res => res.blob())
  //         .then(file=>{
  //           // this.dataURLtoFile(event.base64, this.file.name).then((file: File)=>{
  //           //   this.croppedImage = file
  //           // });
  //           this.croppedImage = res[0];

  //       })
  //     })
  //     });
  //     // getBase64Strings([file], { maxSize: 1200 }).then(res=>{
  //     //   fetch(res[0])
  //     //   .then(res => res.blob())
  //     //   .then(file=>{
  //     //     this.croppedImage = (res[0])
  //     //   })
  //     // })
  // }

  async dataURLtoFile(dataurl, filename) {
    return new Promise((res,rej)=>{
      var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
      while(n--){
        u8arr[n] = bstr.charCodeAt(n);
      }
      const file = new File([u8arr], filename, {type:mime});
      if(file instanceof File){
        res(file);
      }else{
        rej(null);
      }
      
    })
}


  imageLoaded() {
      this.showImageParams = true; 
  }

  cropperReady() {
      // cropper ready
  }
  loadImageFailed() {
      // show message
  }

  fixBinary (bin) {
    var length = bin.length;
    var buf = new ArrayBuffer(length);
    var arr = new Uint8Array(buf);
    for (var i = 0; i < length; i++) {
      arr[i] = bin.charCodeAt(i);
    }
    return buf;
  }

  base64 = 
  "iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAB1klEQVR42n2TzytEURTHv3e8N1joRhZG" + 
  "zJsoCjsLhcw0jClKWbHwY2GnLGUlIfIP2IjyY2djZTHSMJNQSilFNkz24z0/Ms2MrnvfvMu8mcfZvPvu" + 
  "Pfdzz/mecwgKLNYKb0cFEgXbRvwV2s2HuWazCbzKA5LvNecDXayBjv9NL7tEpSNgbYzQ5kZmAlSXgsGG" + 
  "XmS+MjhKxDHgC+quyaPKQtoPYMQPOh5U9H6tBxF+Icy/aolqAqLP5wjWd5r/Ip3YXVILrF4ZRYAxDhCO" + 
  "J/yCwiMI+/xgjOEzmzIhAio04GeGayIXjQ0wGoAuQ5cmIjh8jNo0GF78QwNhpyvV1O9tdxSSR6PLl51F" + 
  "nIK3uQ4JJQME4sCxCIRxQbMwPNSjqaobsfskm9l4Ky6jvCzWEnDKU1ayQPe5BbN64vYJ2vwO7CIeLIi3" + 
  "ciYAoby0M4oNYBrXgdgAbC/MhGCRhyhCZwrcEz1Ib3KKO7f+2I4iFvoVmIxHigGiZHhPIb0bL1bQApFS" + 
  "9U/AC0ulSXrrhMotka/lQy0Ic08FDeIiAmDvA2HX01W05TopS2j2/H4T6FBVbj4YgV5+AecyLk+Ctvms" + 
  "QWK8WZZ+Hdf7QGu7fobMuZHyq1DoJLvUqQrfM966EU/qYGwAAAAASUVORK5CYII=";

  uploadImage(){
  this.isUploading = true;
 // console.log(this.croppedImage.split(",")[1]);
  const preparedData = new FormData();
  const uploadFileAws = new FormData();
  uploadFileAws.append('file',this.file)
  this.mainService.uploadImageAws(uploadFileAws).subscribe((response: {data: string})=>{
    preparedData.append("Orientation",this.bgConfiguration.controls['orientation'].value);
    preparedData.append("AppCode",this.cust_id);
    preparedData.append("FileName",response.data);
    preparedData.append("place",this.bgConfiguration.value.place);
    preparedData.append("Image",new File([new Blob([this.fixBinary(atob(this.base64))], {type: 'image/png'})],this.uuidv4()+'png')); //this.croppedImage.split(",")[1]);
      this.mainService.uploadBackground(preparedData).subscribe(()=>{
      this.deleteSelectedBackground();
        this.sucessfullyAdded();
      },
      ()=>{
        this.isUploading = false
      },
      ()=> this.isUploading = false);
  })
  }

  public changeOrientation(): void {
    this.uploadBackgrounds();
  }

  public changePlace(): void {
    this.uploadBackgrounds();
  }


  delete(name: string){
    Swal.queue([{
      title: this.dictionary.getTranslate('delete_img','Удаление изображения'),
      confirmButtonText: this.dictionary.getTranslate('delete','Удалить'),
      text: this.dictionary.getTranslate('sure_delete_img','Вы действительно хотите удалить изображение ?'),
      cancelButtonText: this.dictionary.getTranslate('cancel','отмена'),
      showCancelButton: true,
      showLoaderOnConfirm: true,
      preConfirm: async () => {
        return new Promise((resolve,reject)=>{
          this.mainService.deleteBackgrounds({
            "appCode": this.cust_id,
            "fileName": name
          }).subscribe(res=>{
            this.uploadBackgrounds();
            resolve(()=>{
              Swal.insertQueueStep({
              icon: 'success',
              title: this.dictionary.getTranslate('img_deleted_success','Изображение успешно удалено'),
            });
          });
          }, error=>{
            resolve(Swal.insertQueueStep({
              icon: 'error',
              title: this.dictionary.getTranslate('cant_delete_img','Не могу удалить изображение'),
            }));
          })
        })
      }
    }])
  }

  public sucessfullyAdded(){
    let timerInterval
    Swal.fire({
      icon: 'success',
      title: this.dictionary.getTranslate('img_log_success',"Изображение успешно загруженно!"),
      timer: 2000,
     // timerProgressBar: true,
      // willOpen: () => {
      //   Swal.showLoading();
      // },
      willClose: () => {
        clearInterval(timerInterval);
        this.uploadBackgrounds();
      }
    });
  }


}

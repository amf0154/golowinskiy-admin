import { Component, OnDestroy, OnInit } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { CategoryItem } from "@app/core/models/category-item";
import { AuthenticationService } from "@app/core/services/authentication.service";
import { CategoriesService } from "@app/core/services/categories.service";
import { DictionaryService } from "@app/core/services/dictionary.service";
import { MainService } from "@app/core/services/main.service";
import { SelectCheckboxesStateService } from "@app/core/services/select-checkboxes-state.service";
import { SharedService } from "@app/core/services/shared.service";
import { SpinnerService } from "@app/core/services/spinner.service";
import { ToastrService } from "ngx-toastr";
import { Subscription } from "rxjs";
@Component({
    selector: "app-client-catalog-show",
    templateUrl: "./show-catalog-client.component.html",
    styleUrls: ["./show-catalog-client.component.scss"]
})
export class ShowCatalogClientComponent implements OnInit, OnDestroy {
    public categories: CategoryItem[] = [];
    public position: number = 1;
    public submitted: boolean = false;
    public isDataLoaded: boolean = false;
    public initialCategories = new Array();
    public showCats: boolean = false;
    public refreshCat: boolean = false;
    public previousItem: {} = null;
    private selectCategoryItemSubscr: Subscription = null;
    public selectedCategory: any = null;
    public positions = [
        { value: "1", name: () => this.dictionary.getTranslate("page_adverts", "страница размещения объявлений") },
        { value: "2", name: () => this.dictionary.getTranslate("page_main", "главная страница") },
        { value: "3", name: () => this.dictionary.getTranslate("learn_page", "страница выучить") }
    ];

    public phoneForm = this.formBuilder.group({
        phone: ["", [Validators.required, Validators.pattern("^[0-9]*$"), this.sharedService.emptySpacesValidator]],
        position: { value: this.positions[1].value, disabled: true }
    });
    public get f() {
        return this.phoneForm.controls;
    }
    public changePosition() {
        this.fillCheckboxes(null);
        setTimeout(() => this.findData(), 100);
    }
    constructor(
        private formBuilder: FormBuilder,
        public toastr: ToastrService,
        public mainService: MainService,
        public authService: AuthenticationService,
        public dictionary: DictionaryService,
        public categoriesService: CategoriesService,
        public sharedService: SharedService,
        public selectCheckboxesService: SelectCheckboxesStateService,
        public spinner: SpinnerService
    ) {
        this.selectCategoryItemSubscr = this.selectCheckboxesService.getSelectedStackForSave().subscribe((res) => {
            if (Object.keys(res).length !== 0 && this.isDataLoaded) this.saveData(res);
        });
    }

    ngOnInit(): void {
        // this.mainService.getCategories(this.authService.getCustId(),null,"1").subscribe((categories: any) => {
        //   this.initialCategories = Object.assign([],categories);
        //   this.showCats = true;
        // },error=>{
        //   this.toastr.error(this.dictionary.getTranslate('cant_load_cats','не могу загрузить категории'));
        // });
        // this.categoriesService.fetchCategoriesAll(this.authService.getCustId(),null,"1");
    }

    ngOnDestroy(): void {
        this.selectCategoryItemSubscr.unsubscribe();
    }

    onCategoriesClick(items: CategoryItem[]) {
        if (!this.previousItem) {
            this.previousItem = items[items.length - 1];
        } else {
            if (JSON.stringify(this.previousItem) !== JSON.stringify(items[items.length - 1])) {
                this.previousItem = items[items.length - 1];
            }
        }
        this.categories = items;
        const item = items[items.length - 1];
        this.selectedCategory = item;
    }

    public resetValidation() {
        this.submitted = false;
    }

    public saveData({ id, value }) {
        this.spinner.display(true);
        this.mainService
            .updateCustomerCatalog(
                id,
                value,
                this.authService.getCustId(),
                this.phoneForm.controls["phone"].value,
                this.phoneForm.controls["position"].value,
                2
            )
            .subscribe(
                (res) => {
                    if (res.result)
                        this.toastr.success(
                            this.dictionary.getTranslate("settings_saved_suss", "Настройки успешно сохранены!")
                        );
                    else
                        this.toastr.error(
                            this.dictionary.getTranslate("cant_save_settings", "Не удалось сохранить настройки!")
                        );
                },
                (e) => {
                    this.toastr.error(
                        this.dictionary.getTranslate("cant_save_settings", "Не удалось сохранить настройки!")
                    );
                    this.spinner.display(false);
                },
                () => this.spinner.display(false)
            );
    }

    public reset() {
        this.phoneForm.controls["phone"].setValue("");
        this.phoneForm.controls["phone"].enable();
        this.phoneForm.controls["position"].disable();
        this.toastr.success(this.dictionary.getTranslate("settings_reseted", "Настройки успешно сброшены!"));
        this.fillCheckboxes(null);
        this.isDataLoaded = false;
        this.submitted = false;
    }

    public findData() {
        this.submitted = true;
        if (!this.phoneForm.valid) {
            return false;
        } else {
            this.spinner.display(true);
            this.mainService
                .getCustomerCatalogNew(
                    this.authService.getCustId(),
                    this.phoneForm.controls["phone"].value,
                    this.phoneForm.controls["position"].value,
                    2
                )
                .subscribe(
                    (customerSettings) => {
                        if (customerSettings) {
                            this.phoneForm.controls["phone"].disable();
                            this.phoneForm.controls["position"].enable();
                            this.fillCheckboxes(customerSettings);
                            this.toastr.success(
                                this.dictionary.getTranslate("settings_saved", "Настройки успешно загружены!")
                            );
                            this.isDataLoaded = true;
                        }
                    },
                    (e) => {
                        const { error, status } = e;
                        this.toastr.error(error.p_id);
                        this.spinner.display(false);
                    },
                    () => this.spinner.display(false)
                );
        }
    }

    public fillCheckboxes(customerSettings) {
        const selectedCategoryIds = customerSettings
            ? customerSettings.filter((el) => el.mark === 1).map((el) => el.id)
            : [null];
        const arrowIds = customerSettings ? customerSettings.filter((el) => el.mark === 2).map((el) => el.id) : [];
        this.selectCheckboxesService.setArrowsList(arrowIds);
        this.selectCheckboxesService.setSelectedCategoriesList(selectedCategoryIds);
    }
}

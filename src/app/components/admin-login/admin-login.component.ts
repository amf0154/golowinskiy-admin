import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthenticationService } from '@app/core/services/authentication.service';
import { DictionaryService } from '@app/core/services/dictionary.service';
import { MainService } from '@app/core/services/main.service';
import { SpinnerService } from '@app/core/services/spinner.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.scss']
})
export class AdminLoginComponent{
  public loginForm: FormGroup;
  public submitted: boolean = false;
  public dataLoaded = false;
  public rememberMe: boolean = localStorage.getItem('saRememberMe') === 'true';
  public response: string = "";
  constructor(
    public dialogRef: MatDialogRef<AdminLoginComponent>,
    public fb: FormBuilder,
    public dictionary: DictionaryService,
    private notify: ToastrService,
    public spinner: SpinnerService,
    private auth: AuthenticationService,
    private mainService: MainService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {}

  ngOnInit(): void {
    this.getData();
  }

  public saveCreds(){
    if(this.rememberMe){
      localStorage.setItem('saRememberMe',this.rememberMe.toString());
      localStorage.setItem("saCreds",JSON.stringify(this.loginForm.value));
    }
    this.deleteCreds();
  }

  deleteCreds(){
    if(!this.rememberMe){
      localStorage.removeItem("saCreds");
      this.getData();
    }
    localStorage.setItem('saRememberMe',this.rememberMe.toString())
  }

  private emptySpacesValidator(control: FormControl): ValidationErrors {
    const value = control.value;
    if (value.trim().length === 0) {
      return { emptySpaces: 'Введите корректное название' };
    }
    return null;
  }

  getData(){
    const savedCreds = localStorage.getItem("saCreds") ? JSON.parse(localStorage.getItem("saCreds")) : null;
    this.loginForm = this.fb.group({
      userName : [
        savedCreds ? savedCreds.userName : '', 
        [
          Validators.required, 
          this.emptySpacesValidator,
        ]
      ],
      password : [
        savedCreds ? savedCreds.password : '', 
        [
          Validators.required, 
          this.emptySpacesValidator,
        ]
      ]
    })
  }


  get f() {
    return this.loginForm.controls;
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  login() {
    this.submitted = true;
    if(!this.loginForm.valid){
      return false
    }else{
      this.spinner.display(true);
      this.mainService.authAdmin(this.loginForm.value).subscribe((data)=>{
        if(data.role === 'superadmin'){
          const {userName, password} = this.loginForm.value;
          sessionStorage.setItem("superadmin",userName+':'+password);
          this.saveCreds();
          sessionStorage.setItem("token",data.accessToken)
          this.notify.success("Вы успешно авторизованы!");
          this.auth.setSuperAdminStatus(true);
          this.dialogRef.close(true);
        }else{
          if(data.txt)
          this.response = data.txt;
          setTimeout(()=>this.response = "",3000)
        }
        this.spinner.display(false)
      }, (msg) => {
        this.response = msg;
        setTimeout(() => this.response = "", 3000);
        this.spinner.display(false);
      });
    }
  }

}

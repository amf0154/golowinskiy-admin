import { Component, OnInit } from "@angular/core";
import { FormArray, FormBuilder, Validators } from "@angular/forms";
import { ThemePalette } from "@angular/material/core";
import { AuthenticationService } from "@app/core/services/authentication.service";
import { DictionaryService } from "@app/core/services/dictionary.service";
import { MainService } from "@app/core/services/main.service";
import { ToastrService } from "ngx-toastr";

@Component({
    selector: "app-cart",
    templateUrl: "./cart.component.html",
    styleUrls: ["./cart.component.scss"]
})
export class CartComponent implements OnInit {
    constructor(
        private readonly formBuilder: FormBuilder,
        public readonly dictionary: DictionaryService,
        public readonly mainService: MainService,
        public readonly authService: AuthenticationService,
        public readonly toastr: ToastrService
    ) {}
    public submitted: boolean;
    public stepHour = 1;
    public stepMinute = 1;
    public stepSecond = 1;
    public touchUi = false;
    public restaurantData = [];
    public enableMeridian = false;
    public color: ThemePalette = "primary";
    public restaurantForm = this.formBuilder.group({
        table: new FormArray([])
    });

    public firmForm = this.formBuilder.group({
        firma: ["", Validators.required]
    });

    public table = this.restaurantForm.get("table") as FormArray;

    ngOnInit(): void {
        this.mainService.getBookingOrderInfo(this.authService.getCustId()).subscribe((data) => {
            this.restaurantData = data;
            this.restaurantData.forEach((item) => {
                this.table.push(this.formBuilder.group(item));
            });
        });
    }

    public get rf() {
        return this.restaurantForm.controls;
    }

    get tableForm() {
        return (this.table as FormArray).controls;
    }

    public makeOrder(): void {
        this.mainService.AddBookingOrder(this.firmForm.getRawValue()).subscribe(
            (r) => {
                this.firmForm.reset();
                this.toastr.success(this.dictionary.getTranslate("order_just_made", "Заказ сформирован"));
            },
            (err) => this.toastr.error("Ошибка выполнения!")
        );
    }

    public getAttribute(item, attr): void {
        return item.get(attr).value;
    }

    public updRestaurant(row): void {
        console.log(row.value);
            this.mainService.UpdateBookingOrder(this.authService.getCustId(), row.value).subscribe(
                (r) => {
                    this.toastr.success(
                        this.dictionary.getTranslate("settings_saved_suss", "Настройки успешно сохранены!")
                    );
                },
                (err) => this.toastr.error("Ошибка выполнения!")
            );
    }

    public reset(row) {
        row.get("tb").setValue(null);
        row.get("ch").setValue(null);
        row.get("hdate").setValue(null);
        this.updRestaurant(row);
    }
}

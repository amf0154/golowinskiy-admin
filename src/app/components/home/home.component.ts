import { Component, OnInit } from '@angular/core';
import { CategoriesService } from 'src/app/core/services/categories.service';
import { EnvService } from '@app/core/services/env.service';
import { PagesService } from '@app/core/services/pages.service';
import { DictionaryService } from '@app/core/services/dictionary.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public mobileLink: String = "";
  constructor(
    public pages: PagesService,
    public categoriesService: CategoriesService,
    public dictionary: DictionaryService,
    public env: EnvService) {}

  ngOnInit() {
    const url = new URL(this.env.apiUrl);
    this.mobileLink = url.origin + '/Account/Login';
  }

}
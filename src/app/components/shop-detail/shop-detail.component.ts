import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, ValidationErrors, } from '@angular/forms';
import { AuthenticationService } from '@app/core/services/authentication.service';
import { DictionaryService } from '@app/core/services/dictionary.service';
import { MainService } from '@app/core/services/main.service';
import { PermissionService } from '@app/core/services/permission.service';
import { SpinnerService } from '@app/core/services/spinner.service';
import { ToastrService } from 'ngx-toastr';
import { Subject, Subscription } from 'rxjs';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-shop-detail',
  templateUrl: './shop-detail.component.html',
  styleUrls: ['./shop-detail.component.scss']
})
export class ShopDetailComponent implements OnInit, OnDestroy {
  private notAllowedItems = [];
  private subscriptionPermission: Subscription;
  constructor(
    private formBuilder: FormBuilder,
    private mainService: MainService,
    private authService: AuthenticationService,
    private notify: ToastrService,
    private spinner: SpinnerService,
    public dictionary: DictionaryService,
    public permissionService: PermissionService
  ) {
    this.subscriptionPermission = this.permissionService.permissionList.asObservable().subscribe((resp)=>{
      this.notAllowedItems = resp.filter((el)=> el.alias.startsWith('settings') && !el.isShow).map(el=> el.alias.substring(9,el.alias.length));
    });
  }
  ngOnDestroy(): void {
    this.subscriptionPermission.unsubscribe()
  }
  public submitted = false;
  private regexURL = '(http(s)?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
  public languages = ['RU','DE'];
  public dataLoaded = false;
  public siteSort = [
    { title: () => this.dictionary.getTranslate('sort-not','сортировка не выбрана'), value: null },
    { title: () => this.dictionary.getTranslate('sort-price','по цене'), value: 0 },
    { title: () => this.dictionary.getTranslate('sort-date','по дате размещения - новые вверху'), value: 3 },
    { title: () => this.dictionary.getTranslate('sort-importer','по поставщику'), value: 4 },
    { title: () => this.dictionary.getTranslate('sort-name','по наименованию'), value: 5 },
    { title: () => this.dictionary.getTranslate("sort-letter", "по кол-ву букв в слове"), value: 8 },
    { title: () => this.dictionary.getTranslate("sort-vocabulary", "словарь"), value: 9 }
  ];
  public settingsForm = this.formBuilder.group({
    fName: [''],  // [Validators.required,this.emptySpacesValidator]],
    repres: [''], // [Validators.required,this.emptySpacesValidator]],
    phone: [''], //[Validators.required,Validators.pattern("^[0-9]*$"),this.emptySpacesValidator]],
    eMail: [''], //[Validators.required,this.emptySpacesValidator,Validators.email]],
    mobileOpt: [''], //[Validators.required,Validators.pattern("^[0-9]*$"),this.emptySpacesValidator]],
    geoAddress: [''], //[Validators.required,this.emptySpacesValidator]],
    firma: [''], //[Validators.required,this.emptySpacesValidator]],
    returnURL: [''], //[Validators.required,this.emptySpacesValidator,this.urlValidator]],
    eMailBlankRequest: [''], //[Validators.required,this.emptySpacesValidator]],
    wordEnter: [''], //[Validators.required,this.emptySpacesValidator]],
    isShowGDate: [''],
    pageKind: [false],
    playAudio: [false],
    isWithHomePage: [false],
    isMoveAllow: [],
    isSearchPageVariant: [null],
    isSlideShow: [false],
    isMapShow: [false],
    isShowMediaLink: [false],
    isLoadShowVideo: [false],
    isMakeAdvFromExtPict : [false],
    dz: ['']
  });
  public get f() { 
    return this.settingsForm.controls; 
  }

  ngOnInit(): void {
    this.spinner.display(true)
    this.mainService.locale.getLocales()
    .subscribe(({locales})=>{
      this.languages  = locales;
      this.getShopInfo();
    })
  }


  public showHideItem(alias){
    return this.notAllowedItems.includes(alias) ? false : true
  }


  public saveSettings(){
    this.submitted = true;
    if (!this.settingsForm.valid) {
      return false
    }else{
      this.mainService.updateShopDetails({
        ...this.settingsForm.value,
        cust_ID_Main: this.authService.getCustId()
      }).subscribe(
        ()=>{
          this.sucessfullyUpdated();
        }
      )
    }
  }


  public sucessfullyUpdated(){
    let timerInterval
    Swal.fire({
      icon: 'success',
      title: "Данные успешно сохранены!",
      showConfirmButton: false,
      timer: 2000,
      willClose: () => {
        clearInterval(timerInterval);
      }
    });
  }

  public emptySpacesValidator(control: FormControl): ValidationErrors {
    const value = control.value;
    if (value && value.trim().length === 0) {
     return { emptySpaces: 'Введите корректное название' };
    }
     return null;
  }

  // public urlValidator(control: FormControl): ValidationErrors {
  //   let address = control.value;
  //   let url = require('url');
  //   let addressRegexp = new RegExp('^([\\w\\-]+?\\.?)+?\\.[\\w\\-]+?$');
  
  //   if (address.search(/^(ftp|http|https):\/\//) === -1) { address = 'http://' + address; }	
  
  //   let hname = url.parse(address).hostname;
  
  //   if (hname.length < 4 || hname.length > 255 || !addressRegexp.test(hname)) {
  //     return { incorrectUrl: 'Введите корректное доменное имя' };
  //   }
  //   return null;
  // }

  private getShopInfo(){
    this.mainService.getShopDetails(this.authService.getCustId()).subscribe(
      (res: any)=>{
        this.settingsForm = this.formBuilder.group(res)
        this.dataLoaded = true;
      },()=> {
        this.dataLoaded = false;
        this.notify.error("Ошибка загрузки данных");
      },()=> {
        this.spinner.display(false);
      }
    )
  }
  
}
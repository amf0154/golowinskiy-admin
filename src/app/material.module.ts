import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatMenuModule} from '@angular/material/menu';
import { MatDialogModule } from '@angular/material/dialog';
@NgModule({
  declarations: [],
  imports: [BrowserAnimationsModule, MatProgressBarModule, MatAutocompleteModule, MatMenuModule, MatDialogModule],
  exports: [BrowserAnimationsModule, MatProgressBarModule, MatAutocompleteModule, MatMenuModule, MatDialogModule],
})
export class MaterialModule {}

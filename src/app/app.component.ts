import { Component, OnInit } from '@angular/core';
import { Observable, timer } from 'rxjs';
import { debounce } from 'rxjs/operators';
import { AuthenticationService } from './core/services/authentication.service';
import { DictionaryService } from './core/services/dictionary.service';
import { PermissionService } from './core/services/permission.service';
import { SpinnerService } from './core/services/spinner.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'golowinskiy-admin';
  showLoader: boolean = false;
  public isAuthenticated: Observable<boolean>;
  constructor(
    public spinnerService: SpinnerService,
    public authService: AuthenticationService,
    public dictionary: DictionaryService,
    public permissionService: PermissionService
  ) {
    this.isAuthenticated = this.authService.isAuthenticatedObservable();
    if(!localStorage.getItem('locale'))
      this.dictionary.changeLanguage('RU');
      this.isAuthenticated.subscribe((r)=>{
      })
    if(!localStorage.getItem("catalogType")){
      localStorage.setItem("catalogType","department")
    }  
  }

  ngOnInit(): void {
    if(this.authService.getCustId()) {
      this.permissionService.updatePermissionList(this.authService.getCustId());
    }
    this.spinnerService.status
      .pipe(debounce(() => timer(300)))
      .subscribe((val: boolean) => {
        this.showLoader = val;
      });
  }
}

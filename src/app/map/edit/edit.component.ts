import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Models } from '@app/core/models/models';
import { AuthenticationService } from '@app/core/services/authentication.service';
import { DictionaryService } from '@app/core/services/dictionary.service';
import { ToastrService } from 'ngx-toastr';
import { MapService } from '../map.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  constructor(
    public readonly dialogRef: MatDialogRef<EditComponent>,
    private readonly toastr: ToastrService,
    public readonly dictionary: DictionaryService,
    public mapService: MapService,
    private readonly authService: AuthenticationService,
  //  @Inject(MAT_DIALOG_DATA) public data: Models.MapPoint
  ) {}

  public items : Models.MapPoint[];
  public deletedStack: Models.MapPoint[] = [];

  ngOnInit(): void {
    this.items = Object.assign([],this.mapService.points.value);
  }

  public deleteItem(i: number): void {
    const element = this.items[i];
    element.cust_ID_Main = this.authService.getCustId();
    this.deletedStack.push(element);
    this.items.splice(i,1);
  }

  public editItem(item: Models.MapPoint){
    this.mapService.editPoint(item);
    this.dialogRef.close();
  }

  public save(): void {
    if(!this.deletedStack.length){
      this.toastr.error(this.dictionary.getTranslate("select-points-for-deleting","Выберите элементы для удаления"));
      return;
    } else {
      this.mapService.deleteCoordinate(this.deletedStack).subscribe(()=> {
        this.toastr.success(this.dictionary.getTranslate("map-points-deleted","Выбранные точки успешно удалены"));
        this.dialogRef.close(true);
      })
    }
  }

  

}

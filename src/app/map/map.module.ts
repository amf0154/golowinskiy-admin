import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapRoutingModule } from './map-routing.module';
import { MapComponent } from './map.component';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import {MatDialogModule } from '@angular/material/dialog';
import { PointComponent } from './point/point.component';
import { EditComponent } from './edit/edit.component';
import { AddComponent } from './add/add.component';


@NgModule({
  declarations: [
    MapComponent,
    PointComponent,
    EditComponent,
    AddComponent
  ],
  imports: [
    CommonModule,
    MapRoutingModule,
    MatDialogModule,
    MatButtonModule,
    ToastrModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class MapModule { }

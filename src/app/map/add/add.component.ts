import { Component, Inject } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { Models } from "@app/core/models/models";
import { AuthenticationService } from "@app/core/services/authentication.service";
import { DictionaryService } from "@app/core/services/dictionary.service";
import { ToastrService } from "ngx-toastr";
import { MapService } from "../map.service";
import { PointComponent } from "../point/point.component";

@Component({
    selector: "app-add",
    templateUrl: "./add.component.html",
    styleUrls: ["./add.component.scss"]
})
export class AddComponent {
    constructor(
        private readonly fb: FormBuilder,
        private readonly dialog: MatDialog,
        private readonly mapService: MapService,
        private readonly toastr: ToastrService,
        public readonly dictionary: DictionaryService,
        private readonly authService: AuthenticationService,
        @Inject(MAT_DIALOG_DATA) public data: Models.MapPoint,
        public readonly dialogRef: MatDialogRef<AddComponent>
    ) {}

    public submitted = false;
    public addingForm: FormGroup = this.fb.group({
        geoname: [ {value: this.data?.geoname ?? "", disabled: !!this.data?.isEdit}, [Validators.required]],
        latitude: [ this.data?.latitude ?? "", [Validators.required]],
        longitude: [ this.data?.longitude ?? "", [Validators.required]],
        cust_ID_Main: [{ value: this.authService.getCustId(), disabled: true }]
    });

    public get f() {
        return this.addingForm.controls;
    }

    public save(): void {
        this.submitted = true;
        if (this.addingForm.invalid) {
            return;
        } else {
            const point = this.addingForm.getRawValue();
            this.mapService.addCoordinate(point).subscribe((r) => {
                this.toastr.success("Данные сохранены");
                this.mapService.loadCoords();
                this.dialogRef.close(point);
            });
        }
    }

    public choosePoint(): void {
        this.dialog
            .open(PointComponent, {
                panelClass: window.innerWidth < 600 ? 'full-width-dialog' : null,
                width: window.innerWidth < 600 ? "100%" : "70%",
                data: this.addingForm.getRawValue()
            })
            .afterClosed()
            .subscribe((coords: any) => {
                if (coords?.length) {
                    const [lat, long] = coords;
                    this.addingForm.get("latitude")?.setValue(lat);
                    this.addingForm.get("longitude")?.setValue(long);
                }
            });
    }
}

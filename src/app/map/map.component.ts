import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { MapService } from "./map.service";
import Map from "ol/Map";
import View from "ol/View";
import Feature from "ol/Feature";
import Point from "ol/geom/Point";
import { fromLonLat, transform } from "ol/proj.js";
import { Vector } from "ol/layer";
import { OSM, Vector as VectorSource } from "ol/source.js";
import { Fill, Stroke, Icon, Style, Text } from "ol/style.js";
import { MatDialog } from "@angular/material/dialog";
import { EditComponent } from "./edit/edit.component";
import { AddComponent } from "./add/add.component";
import { Models } from "@app/core/models/models";
import { ToastrService } from "ngx-toastr";
import Overlay from "ol/Overlay";
import TileLayer from "ol/layer/Tile";
import { DictionaryService } from "@app/core/services/dictionary.service";
import { Subscription } from "rxjs";

@Component({
    selector: "app-map",
    templateUrl: "./map.component.html",
    styleUrls: ["./map.component.scss"]
})
export class MapComponent implements OnInit, OnDestroy {
    constructor(
        private readonly dialog: MatDialog,
        private readonly toastr: ToastrService,
        public dictionary: DictionaryService,
        private readonly mapService: MapService
    ) {}

    public map: any;
    public componentDestroyed: Subscription;
    public markerSource = new VectorSource();
    @ViewChild("overlayElement") overlayElement: ElementRef;

    ngOnInit() {
        this.initilizeMap();
        this.getPosition().then((location) => {
            this.addMyPosition(location);
        });
    }

    private addMyPosition(location: []) {
        const iconStyle = new Style({
            image: new Icon({
                anchor: [0.5, 46],
                anchorXUnits: "fraction",
                anchorYUnits: "pixels",
                src: "../assets/images/person.png",
                scale: 0.1
            })
        });
        const iconFeature = new Feature({
            geometry: new Point(transform(location, "EPSG:4326", "EPSG:3857")),
            name: "me",
            population: 4000,
            rainfall: 500
        });
        iconFeature.setStyle(iconStyle);

        this.markerSource.addFeature(iconFeature);
    }

    initilizeMap() {
        const iconStyle = new Style({
            image: new Icon({
                anchor: [0.5, 46],
                anchorXUnits: "fraction",
                anchorYUnits: "pixels",
                src: "../assets/images/point2.png", // https://www.shareicon.net/download/2016/06/10/586233_signal_512x512.png
                scale: 0.05
            })
        });
        const labelStyle = new Style({
            text: new Text({
                font: "16px Calibri,sans-serif",
                overflow: true,
                fill: new Fill({
                    color: "#000"
                }),
                stroke: new Stroke({
                    color: "#2E4",
                    width: 3
                }),
                offsetY: -12
            })
        });
        const style = [iconStyle, labelStyle];

        this.mapService.getCoordinates().subscribe((r: any) => {
            if (r.length) {
                r.forEach((point) => {
                    this.addMarker(point.longitude, point.latitude, point.geoname);
                });
            }

            const container = document.getElementById("popup");
            const content = document.getElementById("popup-content");
            const closer = document.getElementById("popup-closer");

            const overlay = new Overlay({
                element: container,
                autoPan: true,
                autoPanAnimation: {
                    duration: 250
                }
            } as any);

            this.map = new Map({
                target: "map",
                layers: [
                    new TileLayer({
                        source: new OSM()
                    }),

                    new Vector({
                        source: this.markerSource,
                        style: function (feature: any) {
                            labelStyle.getText().setText(feature.get("name"));
                            return style;
                        }
                    })
                ],
                overlays: [overlay],
                view: new View({
                    center: fromLonLat([r[0].longitude, r[0].latitude]),
                    zoom: 10.5
                })
            });

            // this.map.on("singleclick", function (evt: any) {
            //     console.log(evt);
            //     const coordinate = evt.coordinate;
            //     const hdms = toStringHDMS(toLonLat(coordinate));
            //     this.forEachFeatureAtPixel(evt.pixel, function (feature, layer) {
            //         var coordinate = evt.coordinate;
            //         if (layer) {
            //             //  content.innerHTML = '<p>Current coordinates are :</p><code>' + hdms +
            //             ("</code>");
            //             content.innerHTML = `<b>${feature.get("name")}</b>`;
            //             overlay.setPosition(coordinate);
            //         }
            //     });
            // });

            closer.onclick = function () {
                overlay.setPosition(undefined);
                closer.blur();
                return false;
            };

            this.componentDestroyed = this.mapService.points.subscribe((points )=> {
                if (points.length) {
                    this.markerSource.clear();
                    points.forEach((point) => {
                        this.addMarker(point.longitude, point.latitude, point.geoname);
                    });
                }
            })
        });
    }

    // private resetMap(): void {
    //     this.mapService.getCoordinates().subscribe((r: any) => {
    //         if (r.length) {
    //             this.points = r;
    //             this.markerSource.clear();
    //             r.forEach((point) => {
    //                 this.addMarker(point.longitude, point.latitude, point.geoname);
    //             });
    //         }
    //     });
    // }

    setCenter(long: any, lat: any) {
        var view = this.map.getView();
        view.setCenter(fromLonLat([long, lat]));
        view.setZoom(13);
    }

    public addPoint(): void {
        this.dialog
            .open(AddComponent, {
                panelClass: window.innerWidth < 600 ? 'full-width-dialog' : null,
                width: window.innerWidth < 600 ? '100%' : '60%',
            })
            .afterClosed()
            .subscribe((coords: Models.MapPoint) => {
                if (coords) {
                  //  this.mapService.getCoordinates().subscribe(()=> {
                        this.setCenter(coords.longitude, coords.latitude);
                  //  })  
                }
            });
    }

    public control(): void {
        this.dialog
            .open(EditComponent, {
                panelClass: window.innerWidth < 600 ? 'full-width-dialog' : null,
                width: window.innerWidth < 600 ? '100%' : '650px',
            })
            .afterClosed()
            .subscribe((r: boolean | undefined) => (r ? this.mapService.loadCoords() : null));
    }

    private addMarker(lon, lat, name): void {
        const iconFeature = new Feature({
            geometry: new Point(transform([lon, lat], "EPSG:4326", "EPSG:3857")),
            name: name,
            population: 4000,
            rainfall: 500
        });
        this.markerSource.addFeature(iconFeature);
    }

    getPosition(): Promise<any> {
        return new Promise((resolve, reject) => {
            navigator.geolocation.getCurrentPosition(
                (resp) => {
                    resolve([resp.coords.longitude, resp.coords.latitude]);
                },
                (err) => {
                    this.toastr.error("Не удалось получить ваши координаты");
                    reject(err);
                }
            );
        });
    }


    ngOnDestroy(): void {
        this.componentDestroyed.unsubscribe();
    }
}

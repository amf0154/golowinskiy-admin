import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { Models } from "@app/core/models/models";
import { AuthenticationService } from "@app/core/services/authentication.service";
import { EnvService } from "@app/core/services/env.service";
import { BehaviorSubject } from "rxjs";
import { map } from "rxjs/operators";
import { AddComponent } from "./add/add.component";

@Injectable({
    providedIn: "root"
})
export class MapService {
    constructor(
        private http: HttpClient,
        private environment: EnvService,
        private dialog: MatDialog,
        private authService: AuthenticationService
    ) {}

    public points = new BehaviorSubject<Models.MapPoint[]>([]);

    public getCoordinates() {
        return this.http.get(`${this.environment.apiUrl}/Geolocation/Get?cust_ID_Main=${this.authService.getCustId()}`).pipe(
            map((data: Models.MapPoint[]) => {
                this.points.next(data);
                return data;
            })
        );
    }

    public loadCoords = () => this.getCoordinates().subscribe(()=>{});

    public addCoordinate(params: Models.MapPoint | Models.MapPoint[]) {
        const body: Models.MapPointArray = {
            locations: Array.isArray(params) ? params : [params]
        };
        return this.http.post(`${this.environment.apiUrl}/Geolocation/Set`, body);
    }

    public deleteCoordinate(params: Models.MapPoint | Models.MapPoint[]) {
        const body: Models.MapPointArray = {
            locations: Array.isArray(params) ? params : [params]
        };
        return this.http.post(`${this.environment.apiUrl}/Geolocation/Delete`, body);
    }

    public editPoint(point: Models.MapPoint): void {
        this.dialog
            .open(AddComponent, {
                panelClass: window.innerWidth < 600 ? 'full-width-dialog' : null,
                width: window.innerWidth < 600 ? '100%' : '700px',
                data: {...point, isEdit: true}
            })
            .afterClosed()
            .subscribe();
    }
}

import { Component, Inject, OnInit } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { ToastrService } from "ngx-toastr";
import Map from "ol/Map";
import View from "ol/View";
import Feature from "ol/Feature";
import Point from "ol/geom/Point";
import { fromLonLat, transform } from "ol/proj.js";
import { Tile, Vector } from "ol/layer";
import { OSM, Vector as VectorSource } from "ol/source.js";
import { Fill, Stroke, Icon, Style, Text } from "ol/style.js";
import { MapService } from "../map.service";
import { Models } from "@app/core/models/models";
import { DictionaryService } from "@app/core/services/dictionary.service";

@Component({
    selector: "app-point",
    templateUrl: "./point.component.html",
    styleUrls: ["./point.component.scss"]
})
export class PointComponent implements OnInit {
    constructor(
        public readonly dialogRef: MatDialogRef<PointComponent>,
        private readonly toastr: ToastrService,
        private mapService: MapService,
        public readonly dictionary: DictionaryService,
        @Inject(MAT_DIALOG_DATA) public data: Models.MapPoint
    ) {}

    public map: any;
    public selectedCoords = [];
    public existedPoints = [];
    public markerSource = new VectorSource();

    ngOnInit() {
        this.initilizeMap();
        this.mapService.getCoordinates().subscribe((r: any) => {
            if (r.length) {
               // this.existedPoints = r;
               // this.fillPoints();
                const {latitude, longitude, geoname} = this.data;
                if(geoname) {
                    this.addMarker(longitude, latitude, geoname, false);
                    if(latitude && longitude)
                    this.setCenter(longitude, latitude);
                }
            }
        });
    }

    setCenter(long: any, lat: any) {
        var view = this.map.getView();
        view.setCenter(fromLonLat([long, lat]));
        view.setZoom(12);
    }

    private fillPoints(): void {
        this.existedPoints.forEach((point) => {
            this.addMarker(point.longitude, point.latitude, point.geoname, false);
        });
    }

    initilizeMap() {
        const iconStyle = new Style({
            image: new Icon({
                anchor: [0.5, 46],
                anchorXUnits: "fraction",
                anchorYUnits: "pixels",
                src: "../assets/images/point2.png", // https://www.shareicon.net/download/2016/06/10/586233_signal_512x512.png
                scale: 0.05
            })
        });
        const labelStyle = new Style({
            text: new Text({
                font: "16px Calibri,sans-serif",
                overflow: true,
                fill: new Fill({
                    color: "#000"
                }),
                stroke: new Stroke({
                    color: "#2E4",
                    width: 3
                }),
                offsetY: -12
            })
        });
        const style = [iconStyle, labelStyle];

        this.map = new Map({
            target: "map2",
            layers: [
                new Tile({
                    source: new OSM()
                }),

                new Vector({
                    source: this.markerSource,
                    style: function (feature: any) {
                        labelStyle.getText().setText(feature.get("name"));
                        return style;
                    }
                })
            ],
            view: new View({
                center: fromLonLat([37.618423, 55.751244]),
                zoom: 12
            })
        });

        this.map.on("click", (e: any) => {
            const lonlat = transform(e.coordinate, "EPSG:3857", "EPSG:4326");
            const lon = lonlat[0];
            const lat = lonlat[1];
            this.selectedCoords = [lat.toFixed(6), lon.toFixed(6)];
            this.addMarker(lon, lat, this.data.geoname);
        //    this.fillPoints();
        });

        this.setCenter(37.618423, 55.751244);
    }

    private addMarker(lon, lat, name = null, clear = true): void {
        const iconFeature = new Feature({
            geometry: new Point(transform([lon, lat], "EPSG:4326", "EPSG:3857")),
            name: name,
            population: 4000,
            rainfall: 500
        });
        if (clear) this.markerSource.clear();
        this.markerSource.addFeature(iconFeature);
    }

    public save(): void {
        if (!this.selectedCoords.length) {
            this.toastr.error(this.dictionary.getTranslate("required-point-map", "Выберите точку на карте!"));
            return;
        }
        this.mapService.loadCoords();
        this.dialogRef.close(this.selectedCoords);
    }
}

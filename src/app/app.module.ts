import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSliderModule } from '@angular/material/slider';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { NavigationComponent } from './components/shared/navigation/navigation.component';
import { HomeComponent } from './components/home/home.component';
import { AdvertsComponent } from './components/adverts/adverts.component';
import { ClientsComponent } from './components/clients/clients.component';
import { LoginComponent } from './components/login/login.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { DesktopCategoriesComponent } from './components/categories/desktop-categories/desktop-categories.component';
import { BreadcrumbsComponent } from './components/categories/breadcrumbs/breadcrumbs.component';
import { MaterialModule } from './material.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxPaginationModule } from 'ngx-pagination';
import { CategoriesComponent } from './components/categories/categories.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { ModifyCategoryComponent } from './components/categories/modify-category/modify-category.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { AdvertsControlComponent } from './components/adverts-control/adverts-control.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatNativeDateModule, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { SpinnerService } from '@app/core/services/spinner.service';
import { BackgroundSettingsComponent } from './components/background-settings/background-settings.component';
import { ShopDetailComponent } from './components/shop-detail/shop-detail.component';
import { EnvServiceProvider } from './core/services/env.service.provider';
import { ClientCatalogComponent } from './components/client-catalog/client-catalog.component';
import { CatalogNotifyComponent } from './components/catalog-common/catalog-notify/catalog-notify.component';
import { CheckboxComponent } from './components/shared/checkbox/checkbox.component';
import { CommonModule } from '@angular/common';
import { AdminLoginComponent } from './components/admin-login/admin-login.component';
import { TitlesComponent } from './components/shared/titles/titles.component';
import { EditCatalogClientComponent } from './components/catalog-common/edit-catalog-client/edit-catalog-client.component';
import { ShowCatalogClientComponent } from './components/catalog-common/show-catalog-client/show-catalog-client.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { JwtInterceptor, SuperAdminRoutes } from './core/helpers/jwt.interceptor';
import { ErrorInterceptor } from './core/helpers/error.interceptor';
import { SharedModule } from './shared.module';
import { map } from 'rxjs/operators';
import { EnvService } from './core/services/env.service';
import { DictionaryService } from './core/services/dictionary.service';
import { ChangeLanguageComponent } from './components/shared/change-language/change-language.component';
import { ChangeSortsCatalogComponent } from './components/catalog-common/change-sorts/change-sorts-catalog.component';
import { ControlCatalogComponent } from './components/shared/control-catalog/control-catalog.component';
import { NgxMatDatetimePickerModule, NgxMatTimepickerModule, NgxMatDateFormats, NGX_MAT_DATE_FORMATS } from '@angular-material-components/datetime-picker';
import { NgxMatMomentModule } from '@angular-material-components/moment-adapter';
import { LearnCatalogClientComponent } from './components/catalog-common/learn/learn-catalog-client.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FunctionalControlComponent } from './components/functional-control/functional-control.component';
import { LocaleClientResolver } from './components/functional-control/resolver.service';

const CUSTOM_DATE_FORMATS: NgxMatDateFormats = {
  parse: {
    dateInput: 'l, LTS'
  },
  display: {
    dateInput: 'DD/MM/YYYY HH:mm',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  }
};

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        FooterComponent,
        NavigationComponent,
        HomeComponent,
        AdvertsComponent,
        ClientsComponent,
        LoginComponent,
        NotFoundComponent,
        DesktopCategoriesComponent,
        BreadcrumbsComponent,
        CategoriesComponent,
        ModifyCategoryComponent,
        AdvertsControlComponent,
        BackgroundSettingsComponent,
        ShopDetailComponent,
        ClientCatalogComponent,
        CatalogNotifyComponent,
        CheckboxComponent,
        AdminLoginComponent,
        TitlesComponent,
        EditCatalogClientComponent,
        ShowCatalogClientComponent,
        LearnCatalogClientComponent,
        NavbarComponent,
        ChangeLanguageComponent,
        ChangeSortsCatalogComponent,
        ControlCatalogComponent,
        FunctionalControlComponent
    ],
    imports: [
        CommonModule,
        MatSlideToggleModule,
        MatFormFieldModule,
        MatInputModule,
        MatNativeDateModule,
        MatDatepickerModule,
        BrowserModule,
        MatSelectModule,
        HttpClientModule,
        AppRoutingModule,
        ReactiveFormsModule.withConfig({ warnOnNgModelWithFormControl: 'never' }),
        FormsModule,
        MaterialModule,
        BrowserAnimationsModule,
        NgxPaginationModule,
        MatDialogModule,
        MatCheckboxModule,
        NgxMatDatetimePickerModule, NgxMatTimepickerModule,
        MatSliderModule,
        NgbModule,
        NgxMatMomentModule,
        ToastrModule.forRoot({
            timeOut: 2000,
            positionClass: 'toast-top-right',
            preventDuplicates: true,
        }),
        SharedModule
    ],
    providers: [
        SuperAdminRoutes,
        SpinnerService,
        LocaleClientResolver,
        {
            provide: APP_INITIALIZER,
            useFactory: initLocalization,
            multi: true,
            deps: [DictionaryService, EnvService, HttpClient]
        },
        EnvServiceProvider,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true,
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ErrorInterceptor,
            multi: true,
        },
        {provide: NGX_MAT_DATE_FORMATS, useValue: CUSTOM_DATE_FORMATS},
        { provide: MAT_DATE_LOCALE, useValue: 'ru-RU' }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {}


export function initLocalization(dictionary: DictionaryService,env: EnvService,http: HttpClient) {
  return () => {
    const localeToTableView = (data) =>{
      const aliaces: Array<any> = Array.from(
        data.reduce((alias, el:any) => {
          alias.add(el.alias)
          return alias;
        },new Set())
      );
      return aliaces.reduce((locale,alias:any)=>{
        const eachAliaseData = data.filter(((el: any)=>el.alias == alias));
        const aliasInfo = eachAliaseData.reduce((curr,el:any)=>{
          curr['ALIAS']= el.alias;
          curr[el.dz.toUpperCase()] = el.txt;  
          return curr;
        },{})
        locale.push(aliasInfo);
        return locale;
      },[]);
    }

    const filterLangColumnsOnly = (attr)=> !['ALIAS','PageName'].includes(attr)
    const getLanguages = (db) =>{
      return db.reduce((languages,el)=>{
        const keys = Object.keys(el).filter(filterLangColumnsOnly);
        keys.forEach((v)=> {
          if(!languages.includes(v))
            languages.push(v)
        })
        return languages;
      },[])
    }

    return new Promise((resolve)=>{
      http.get(`${env.apiUrl}/Localization/Get?library=2`)
      .pipe(map((el)=> localeToTableView(el)))
      .toPromise()
      .then((data)=>{
        dictionary.generateDictionary(getLanguages(data))
        const languageDB = data.map((item: any)=>{
          dictionary.languages.forEach((locale) => {
            dictionary.fillDictionary(item,locale)
          });
        });
        Promise.all(languageDB).then(() => {
          dictionary.baseLoaded.next(true); 
          resolve(true);
        });
      });
    });
  };
}

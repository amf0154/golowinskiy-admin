import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SelectCheckboxesStateService {
  private selectedCategoriesStack = new BehaviorSubject([]);
  private selectedHistCategoriesStack = new BehaviorSubject([]);
  private arrowsList = new BehaviorSubject([]);
  private selectedStackForSave = new BehaviorSubject({});
  public showSpinner: boolean = false;
  get count(){
      return this.selectedCategoriesStack.getValue().length;
  } 
  
  constructor(){}

    public put(item){
        const id  = Number(item);
        let existStack = new Set(this.getSelectedCategoriesList());
        if(existStack.has(id)){
            existStack.delete(id); 
            this.selectedStackForSave.next({
                id: id,
                value: false
            });
        }else{ 
            existStack.add(id);
            this.selectedStackForSave.next({
                id: id,
                value: true
            });
        };
        const stack = [...existStack];
        this.setSelectedCategoriesList(stack);
    }

    public getObservableSelectedCategoriesList(){
        return this.selectedCategoriesStack.asObservable()
    }

    public getSelectedStackForSave(): Observable<any>{
        return this.selectedStackForSave.asObservable()
    }

    public getSelectedCategoriesList(){
        return this.selectedCategoriesStack.value
    }

    public setSelectedCategoriesList(items){
        return this.selectedCategoriesStack.next(items)
    }

    public setArrowsList(items){
        return this.arrowsList.next(items)
    }

    public getArrowsListObserv(): Observable<any>{
        return this.arrowsList.asObservable()
    }

    public setHistCategoriesList(items){
        return this.selectedHistCategoriesStack.next(items)
    }

    public getSelectedHistCategoriesList(){
        return this.selectedHistCategoriesStack.value
    }

    public getObservableSelectedHistCategoriesList(){
        return this.selectedHistCategoriesStack.asObservable()
    }
}

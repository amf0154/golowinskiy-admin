import { Injectable } from "@angular/core";
import { JwtHelperService } from "@auth0/angular-jwt";
import { AuthenticationService } from "./authentication.service";
const helper = new JwtHelperService();
@Injectable({
    providedIn: "root"
})
export class TokenService {
    constructor() {}
    public getEncodeToken(token) {
        return helper.decodeToken(token);
    }
    public isTokenExpired(token: string): boolean {
        try {
            return !helper.isTokenExpired(token);
        } catch (e) {
            return false;
        }
    }



    getToken(): string{
      return localStorage.getItem('token')
  }

  isAuthenticated(): boolean{
      return this.isTokenExpired(this.getToken());
  }
  getUserId(): string{
      if(this.getToken())
      return this.getEncodeToken(localStorage.getItem('token')).user_id ? this.getEncodeToken(localStorage.getItem('token')).user_id : null;
      return null;
  }
  getCustId(): string{
      if(localStorage.getItem('token'))
      return this.getEncodeToken(localStorage.getItem('token')).user_id ? this.getEncodeToken(localStorage.getItem('token')).user_id : null;
  }
}

import { Injectable } from "@angular/core";
import { CategoryItem } from "../models/category-item";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { ShopInfoModel } from "../../core/models/shop-info-model";
import { DeleteAdvert } from "../../core/models/delete-advert";
import { from, Observable, throwError } from "rxjs";
import { AuthenticationService } from "./authentication.service";
import { map, catchError, tap, mergeMap } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";
import { EnvService } from "./env.service";
import { ProgressbarService } from "./progressbar.service";
import Swal, { SweetAlertOptions } from "sweetalert2";
import { Models } from "../models/models";
import * as moment from "moment";

@Injectable({
    providedIn: "root"
})
export class MainService {
    constructor(
        private http: HttpClient,
        private spinner: ProgressbarService,
        private authenticationService: AuthenticationService,
        private toastr: ToastrService,
        private environment: EnvService
    ) {}

    private bodyToQuery = (params) =>
        Object.keys(params)
            .map((key) => key + "=" + params[key])
            .join("&");

    public deleteCloudContent(name: String) {
        return this.http.delete(`${this.environment.apiUrl}/FileS3/DeleteImage?file=${name}`).pipe(
            catchError((error: any) => {
                this.toastr.error("Произошла ошибка удаления!");
                return throwError(error);
            })
        );
    }
    public saveCategoriesToStorage(categories) {
        localStorage.setItem("categories", JSON.stringify(categories));
    }
    public clearCategoriesToStorage() {
        localStorage.removeItem("categories");
    }

    public loadCategoriesFromStorage() {
        return JSON.parse(localStorage.getItem("categories")) || [];
    }
    public getPortal() {
        return "19139";
    }

    public getProductsPaginate(
        sortByCatalog: boolean,
        id,
        cust_id,
        userId,
        itemsPerPage: number = 12,
        currentPage: number = 1
    ): Observable<any> {
        return this.http.post(
            `${this.environment.apiUrl}/${
                sortByCatalog ? "GalleryPagination" : "AllGalleryPagination"
            }?itemsPerPage=${itemsPerPage}&currentPage=${currentPage}`,
            {
                Cust_ID: cust_id,
                ID: id,
                CID: userId
            }
        );
    }

    public getLocalizationJson(): Observable<any> {
        let options = {
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": "*"
                //  'Authorization': `Bearer ${localStorage.getItem('token')}`
            }),
            body: {}
        };
        return this.http.get(`https://xn--e1arkeckp8bt.xn--p1ai/assets/localization.json`, options);
    }

    public addNewCategory<T>(body: { Id: number; Name: string; ImgName: string; CustIdMain: number }): Observable<T> {
        return this.http.post<T>(`${this.environment.apiUrl}/Catalog`, body).pipe(
            map((response: any) => {
                if (response.p_id.trim().length) {
                    this.toastr.success("Категория успешно добавлена!");
                } else {
                    this.toastr.error("Категория не добавлена. В родительском разделе есть галерея!");
                }
                return response;
            }),
            catchError((error: any) => {
                this.toastr.error("Произошла ошибка! Повторите попытку.");
                return throwError(error);
            })
        );
    }

    public loadAdverts<T>(
        itemsPerPage: number,
        currentPage: number,
        body: {
            Cust_ID: number;
            StartDate: string;
            FinishDate: string;
            Search: string;
        }
    ): Observable<T> {
        this.spinner.setStatus(true);
        return this.http
            .post<T>(
                `${this.environment.apiUrl}/Admin/Gallery?itemsPerPage=${itemsPerPage}&currentPage=${currentPage}`,
                body
            )
            .pipe(
                map((response) => {
                    setTimeout(() => this.spinner.setStatus(false), 300);
                    return response;
                }),
                catchError((error: any) => {
                    this.spinner.setStatus(false);
                    this.toastr.error("Произошла ошибка! Не могу вышрузить обьявления");
                    return throwError(error);
                })
            );
    }
    public showHideAdvert<T>(body: { Id: number; CustIdMain: string; IsHid: boolean }): Observable<T> {
        this.spinner.setStatus(true);
        return this.http.put<T>(`${this.environment.apiUrl}/Admin/ChangeStatus`, body).pipe(
            map((response) => {
                this.toastr.success(`Обьявление успешно ${body.IsHid ? "скрыто" : "восстановлено"}!`);
                setTimeout(() => this.spinner.setStatus(false), 300);
                return response;
            }),
            catchError((error: any) => {
                this.spinner.setStatus(false);
                this.toastr.error("Произошла ошибка! Не могу изменить видимость обьявления");
                return throwError(error);
            })
        );
    }

    public updateCategory(body: { Id: number; Name: string; ImgName: string; CustIdMain: number }): Observable<any> {
        return this.http.put(`${this.environment.apiUrl}/Catalog`, body).pipe(
            map((response) => {
                this.toastr.success("Категория успешно обновлена!");
                return response;
            }),
            catchError((error: any) => {
                this.toastr.error("Произошла ошибка! Повторите попытку..");
                return throwError(error);
            })
        );
    }

    uploadBackground(data: any) {
        return this.http.post(`${this.environment.apiUrl}/Background/file`, data);
    }

    downloadBackgrounds(appCode: number, mark: number, orientation: string = "H", place: string = "G") {
        return this.http.get(
            `${this.environment.apiUrl}/Background/base64?appCode=${appCode}&mark=${mark}&orientation=${orientation}&place=${place}`
        );
    }

    public authAdmin(body): Observable<any> {
        return this.http.post(`${this.environment.apiUrl}/Admin/Login`, body).pipe(
            map((response) => {
                return response;
            }),
            catchError((error: any) => {
                return throwError(error);
            })
        );
    }
    public checkAdmin(data): Observable<any> {
        const [userName, password] = data.split(":");
        const preparedBody = {
            userName: userName,
            password: password
        };
        return this.http.post(`${this.environment.apiUrl}/Admin/Login`, preparedBody).pipe(
            map((response: { role: string }) => {
                const res = response.role === "superadmin";
                if (!res) {
                    sessionStorage.removeItem("superadmin");
                }
                return res;
            }),
            catchError((error: any) => {
                return throwError(error);
            })
        );
    }

    deleteBackgrounds(data) {
        let options = {
            headers: new HttpHeaders({
                "Content-Type": "application/json"
            }),
            body: data
        };
        return this.http.delete(`${this.environment.apiUrl}/Background/base64`, options);
    }

    uploadImageAws<T>(data: FormData): Observable<T> {
        return this.http
            .post<T>(
                `${this.environment.apiUrl}/FileS3/UploadImage?shopId=${this.authenticationService.getUserId()}`,
                data
            )
            .pipe(
                map((response) => {
                    return response;
                }),
                catchError((error: any) => {
                    return throwError(error);
                })
            );
    }

    deleteImageAws(name: String) {
        return this.http.delete(`${this.environment.apiUrl}/FileS3/DeleteImage?file=${name}`);
    }

    public async deleteNotEmptyCategoryAsync(body) {
        let options = {
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }),
            body: {
                id: body.Id,
                custIdMain: body.CustIdMain
            }
        };
        return await this.http.delete(`${this.environment.apiUrl}/Catalog/DeleteConfirmed`, options).toPromise();
    }

    public deleteCategory(bodier): Observable<any> {
        let options = {
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }),
            body: bodier
        };
        return this.http.delete(`${this.environment.apiUrl}/Catalog`, options).pipe(
            map((response) => {
                if (response === "Раздел не пустой") {
                    //  this.toastr.error('Категория не удалена! Раздел не пустой');
                } else {
                    this.toastr.success("Категория успешно удалена!");
                }
                return response;
            }),
            catchError((error: any) => {
                this.toastr.error("Произошла ошибка! Повторите попытку");
                return throwError(error);
            })
        );
    }

    public deleteAllCategories(bodier): Observable<any> {
        let options = {
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                Authorization: `Bearer ${sessionStorage.getItem("token")}`
            }),
            body: bodier
        };
        return this.http.delete(`${this.environment.apiUrl}/Catalog/DeleteAll`, options).pipe(
            map((response: []) => {
                if (!response.length) {
                    this.toastr.success("Разделы успешно удалены!");
                }
                return response;
            }),
            catchError((error: any) => {
                this.toastr.error("Произошла ошибка! Повторите попытку");
                return throwError(error);
            })
        );
    }
    public deleteAllCategoriesConfirmed(bodier): Observable<any> {
        let options = {
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                Authorization: `Bearer ${sessionStorage.getItem("token")}`
            }),
            body: bodier
        };
        return this.http.delete(`${this.environment.apiUrl}/Catalog/DeleteAllConfirmed`, options).pipe(
            map((response: []) => {
                if (!response.length) {
                    this.toastr.success("Разделы успешно удалены!");
                }
                return response;
            }),
            catchError((error: any) => {
                this.toastr.error("Произошла ошибка! Повторите попытку");
                return throwError(error);
            })
        );
    }

    public deleteAllCatalog(Cust_ID_Main): Observable<any> {
        const options = {
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                Authorization: `Bearer ${sessionStorage.getItem("token") ?? localStorage.getItem("token")}`
            }),
            body: {Cust_ID_Main}
        };
        
        return this.http.delete(`${this.environment.apiUrl}/Catalog/DeleteAllCatalog`, options).pipe(
            catchError((error: any) => {
                this.toastr.error("Произошла ошибка! Повторите попытку");
                return throwError(error);
            })
        );
    }

    public deleteAdvert(data: DeleteAdvert): Observable<any> {
        let options = {
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }),
            body: data
        };
        return this.http.delete(`${this.environment.apiUrl}/product`, options).pipe(
            map((response) => {
                this.toastr.success("Обьявление успешно удалено!");
                return response;
            }),
            catchError((error: any) => {
                this.toastr.error("Произошла ошибка удаления, обьявление не удалено!");
                return throwError(error);
            })
        );
    }

    public updateShopDetails(body: {
        fName: "string";
        repres: "string";
        phone: "string";
        eMail: "string";
        mobileOpt: "string";
        geoAddress: "string";
        firma: "string";
        returnURL: "string";
        eMailBlankRequest: "string";
        wordEnter: "string";
        cust_ID_Main: 0;
    }): Observable<any> {
        this.spinner.setStatus(true);
        return this.http.put(`${this.environment.apiUrl}/ShopDetails`, body).pipe(
            map((response) => {
                setTimeout(() => this.spinner.setStatus(false), 300);
                return response;
            }),
            catchError((error: any) => {
                this.toastr.error("Произошла ошибка! Повторите попытку");
                return throwError(error);
            })
        );
    }

    public locale = {
        updateLocale: (body: { alias: string; library: number; dz: string; txt: string; mark: boolean }) => {
            return this.http.put(`${this.environment.apiUrl}/Localization/Update`, body).pipe(
                catchError((error: any) => {
                    this.toastr.error("Произошла ошибка! Повторите попытку");
                    return throwError(error);
                })
            );
        },
        addDeleteLocale: (body: { dz: string; mark: boolean; library: number }) => {
            return this.http.post(`${this.environment.apiUrl}/Localization/AddDelete`, body).pipe(
                catchError((error: any) => {
                    this.toastr.error("Произошла ошибка! Повторите попытку");
                    return throwError(error);
                })
            );
        },
        getLocales: (lib = 1) => {
            const localeToTableView = (data) => {
                const aliaces: Array<any> = Array.from(
                    data.reduce((alias, el: any) => {
                        if (el.alias !== "LANG") alias.add(el.alias);
                        return alias;
                    }, new Set())
                );
                const locales = data.filter(({ alias }) => alias === "LANG").map(({ dz }) => dz.toUpperCase());
                const table = aliaces.reduce((locale, alias: any) => {
                    const eachAliaseData = data.filter((el: any) => el.alias == alias);
                    const aliasInfo = eachAliaseData.reduce((curr, el: any) => {
                        curr["ALIAS"] = el.alias;
                        curr[el.dz] = el.txt;
                        return curr;
                    }, {});
                    locale.push(aliasInfo);
                    return locale;
                }, []);
                return {
                    data: table,
                    locales: locales
                };
            };
            return this.http.get(`${this.environment.apiUrl}/Localization/Get?library=${lib}`).pipe(
                map((data) => localeToTableView(data)),
                catchError((error: any) => {
                    this.toastr.error("Произошла ошибка! Не могу загрузить список локалей");
                    return throwError(error);
                })
            );
        }
    };

    public getShopDetails(id: any): Observable<any> {
        this.spinner.setStatus(true);
        return this.http.get(`${this.environment.apiUrl}/ShopDetails/${id}`).pipe(
            map((response) => {
                setTimeout(() => this.spinner.setStatus(false), 300);
                return response;
            }),
            catchError((error: any) => {
                this.toastr.error("Произошла ошибка! Повторите попытку");
                return throwError(error);
            })
        );
    }

    public getControlFuncList(cust_ID_Main: any): Observable<any> {
        this.spinner.setStatus(true);
        return this.http.get(`${this.environment.apiUrl}/Localization/GetUi?cust_ID_Main=${cust_ID_Main}`).pipe(
            map((response) => {
                setTimeout(() => this.spinner.setStatus(false), 300);
                return response;
            }),
            catchError((error: any) => {
                this.toastr.error("Произошла ошибка! Повторите попытку");
                return throwError(error);
            })
        );
    }

    public updControlFuncList(item: { cust_ID_Main: string; alias: string; isShow: boolean }): Observable<any> {
        this.spinner.setStatus(true);
        return this.http
            .put(`${this.environment.apiUrl}/Localization/UpdateUi`, {
                items: [item]
            })
            .pipe(
                map((response) => {
                    setTimeout(() => this.spinner.setStatus(false), 300);
                    return response;
                }),
                catchError((error: any) => {
                    this.toastr.error("Произошла ошибка! Повторите попытку");
                    return throwError(error);
                })
            );
    }

    public addDelControlFuncItem(item: { alias: string; txt: string; mark: boolean }): Observable<any> {
        this.spinner.setStatus(true);
        return this.http.post(`${this.environment.apiUrl}/Localization/AddDeleteUi`, item).pipe(
            map((response) => {
                setTimeout(() => this.spinner.setStatus(false), 300);
                return response;
            }),
            catchError((error: any) => {
                this.toastr.error("Произошла ошибка! Повторите попытку");
                return throwError(error);
            })
        );
    }

    public registration(body: {
        f: string;
        userName?: string;
        e_mail?: string;
        Phone1: number;
        Mobile?: number;
        whatsApp?: string;
        skype?: string;
        Address?: string;
        password: string;
        isShowMenuRepeat: boolean;
    }) {
        return this.http.put(`${this.environment.apiUrl}/Authorization`, {
            Cust_ID_Main: this.authenticationService.getCustId(),
            ...body
        });
    }

    public deleteProduct(data: DeleteAdvert) {
        let options = {
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                Authorization: `Bearer ${this.authenticationService.getToken()}`
            }),
            body: data
        };
        return this.http.delete(`${this.environment.apiUrl}/product`, options);
    }

    public getCategories(idPortal: any, userId?: string, advert?: string) {
        let prepatedObject = {
            cust_ID_Main: idPortal,
            cust_Id: userId,
            cid: userId,
            advert: advert
        };
        if (!prepatedObject.advert) delete prepatedObject.advert;
        return this.http.post<CategoryItem[]>(`${this.environment.apiUrl}/categories`, prepatedObject);
    }

    public getRestaurantData(cust_ID_Main: any, phone: number) {
        return this.http.get<any>(
            `${this.environment.apiUrl}/Restaurant/GetBookingInfo?cust_ID_Main=${cust_ID_Main}&phone=${phone}`
        );
    }

    public getBookingOrderInfo(cust_ID_Main: any) {
        return this.http.get<any>(
            `${this.environment.apiUrl}/Restaurant/GetBookingOrderInfo?cust_ID_Main=${cust_ID_Main}`
        );
    }

    public AddBookingOrder(body: { firma: string }) {
        return this.http.post<any>(`${this.environment.apiUrl}/Restaurant/AddBookingOrder`, body);
    }

    public updateRestaurantData(
        cust_ID_Main: any,
        phone,
        form: {
            tb: number;
            ch: number;
            hdate: string; // "2022-07-14T20:36:55.697Z"
        }
    ) {
        if (form.hdate) {
            form.hdate = moment(form.hdate).utc(true).toDate().toISOString();
        }
        return this.http.put<any>(`${this.environment.apiUrl}/Restaurant/UpdateBooking`, {
            cust_ID_Main: cust_ID_Main,
            phone: phone,
            ...form
        });
    }

    public UpdateBookingOrder(
        cust_ID_Main: any,
        form: {
            tb: number;
            ch: number;
            phone: any;
            f: string;
            hdate: string; // "2022-07-14T20:36:55.697Z"
        }
    ) {
        if (form.hdate) {
            form.hdate = moment(form.hdate).utc(true).toDate().toISOString();
        }
        return this.http.put<any>(`${this.environment.apiUrl}/Restaurant/UpdateBookingOrder`, {
            orders: [
                {
                    cust_ID_Main: cust_ID_Main,
                    ...form
                }
            ]
        });
    }

    public getCustomerCatalog(cust_ID_Main: any, phone: string, place: string) {
        let prepatedObject = {
            cust_ID_Main: cust_ID_Main,
            phone: phone,
            place: place
        };
        return this.http.post<any>(`${this.environment.apiUrl}/Catalog/GetCustomerCatalog`, prepatedObject).pipe(
            catchError((error: any) => {
                if (error.p_id === "99999") {
                    this.toastr.error("Номер телефона отсутствует в базе данных");
                } else this.toastr.error("Произошла ошибка! Повторите попытку");
                return throwError(error);
            })
        );
    }

    public additionalImagesArray(data: any) {
        return from(data).pipe(mergeMap((item: any) => this.additionalImageUpload(item.request)));
    }

    public moveToMainAdditionalPicture(prc_id, appCode) {
        this.http
            .put(`${this.environment.apiUrl}/AdditionalPicture/MoveToMain`, {
                appCode: appCode,
                prc_ID: prc_id
            })
            .subscribe();
    }

    addProduct(data: any) {
        return this.http.post(`${this.environment.apiUrl}/product`, data);
    }

    setManual(data: Models.ManualParamsSave): Observable<Models.DefRespStatus> {
        return this.http.post<Models.DefRespStatus>(`${this.environment.apiUrl}/Manual/AddText`, data);
    }
    getManual(data: Models.ManualParamsGet): Observable<Models.ManualResponse> {
        return this.http.get<Models.ManualResponse>(
            `${this.environment.apiUrl}/Manual/GetText?` + this.bodyToQuery(data)
        );
    }

    additionalImageUpload(additionalData: {
        catalog: string;
        id: number;
        prc_ID: number;
        imageOrder: number;
        tImage: string;
        appcode: string;
        cid: any;
    }) {
        return this.http.post(`${this.environment.apiUrl}/AdditionalPicture/Create`, additionalData);
    }

    public getCustomerCatalogNew(cust_ID_Main: any, phone: string, place: string, page = 0) {
        let prepatedObject = {
            Cust_ID_Main: cust_ID_Main,
            Phone: phone,
            Place: place,
            page: page
        };
        return this.http.post<any>(`${this.environment.apiUrl}/Catalog/GetCustomerCatalogNew`, prepatedObject).pipe(
            map((res: Array<any>) => {
                if (res.length === 1 && res[0].cust_id == "99999") {
                    this.toastr.error(res[0].txt);
                    return false;
                } else if (res.length === 1 && res[0].cust_id == "0") {
                    this.regNewUserPopup(res[0].txt, phone);
                    return false;
                } else {
                    return res;
                }
            })
        );
    }

    public deleteAllRepeatsClient(phone, cust_ID_Main) {
        const options = {
            headers: new HttpHeaders({
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`
            }),
            body: {
                phone: phone,
                cust_ID_Main: cust_ID_Main
            }
        };
        return this.http.delete(`${this.environment.apiUrl}/UserInfo/DeleteRepeat`, options);
    }

    public updateCustomerCatalog(id, mark, cust_ID_Main, phone, place, page = 0, delAll = false) {
        let prepatedObject = {
            id: id,
            mark: mark,
            cust_ID_Main: cust_ID_Main,
            phone: phone,
            place: place,
            page: page,
            delAll: delAll
        };
        return this.http.put<any>(`${this.environment.apiUrl}/Catalog/UpdateCustomerCatalog`, {
            allSections: [prepatedObject]
        });
    }

    public updateCustomerCatalogArr(body) {
        return this.http.put<any>(`${this.environment.apiUrl}/Catalog/UpdateCustomerCatalog`, { allSections: body });
    }

    public getClients(search: string = null) {
        return this.http.get<any>(
            `${this.environment.apiUrl}/UserInfo/Search?cust_ID_Main=${this.authenticationService.getUserId()}${
                search ? "&searchdescr=" + search : ""
            }`
        );
    }

    public updateRepeatPermission(body) {
        return this.http.put<any>(`${this.environment.apiUrl}/UserInfo/UpdateRepeatPermission`, body);
    }

    public getRepeatPermission(phone, cust_ID_Main) {
        let prepatedObject = {
            Phone: phone,
            Cust_ID_Main: cust_ID_Main
        };
        return this.http.post<any>(`${this.environment.apiUrl}/UserInfo/GetRepeatPermission`, prepatedObject);
    }

    public getShopInfo() {
        return this.http.get<ShopInfoModel>(`${this.environment.apiUrl}/shopinfo/${this.getPortal()}`);
    }

    public deleteUserClient(id) {
        return this.http.delete<any>(`${this.environment.apiUrl}/UserInfo/Delete?cust_ID=${id}`);
    }

    public get getUploadImgLink() {
        return `${this.environment.apiUrl}FileS3/UploadImage?shopId=19139`;
    }

    regNewUserPopup(msg, phone) {
        const registrationError: SweetAlertOptions = {
            icon: "error",
            title: "Ошибка регистрации пользователя",
            showConfirmButton: false,
            timer: 2000
        };
        Swal.queue([
            {
                //  title: 'Удаление локали',
                confirmButtonText: "Да",
                icon: "warning",
                text: msg,
                cancelButtonText: "Отмена",
                showCancelButton: false,
                showDenyButton: true,
                denyButtonText: `Отмена`,
                showLoaderOnConfirm: true,
                preConfirm: () => {
                    return Swal.fire({
                        title: "Регистрация нового пользователя",
                        cancelButtonText: "Отмена",
                        showCancelButton: true,
                        html:
                            '<input id="input1" type="text" placeholder="Ф.И.О" class="swal2-input">' +
                            '<input id="input2" type="text" placeholder="эл.ящик" class="swal2-input">',
                        didOpen: function () {
                            document.getElementById("input1").focus();
                        },
                        preConfirm: () => {
                            const fio = (document.getElementById("input1") as any).value;
                            const email = (document.getElementById("input2") as any).value;
                            return this.registration({
                                f: fio,
                                Phone1: phone,
                                e_mail: email,
                                password: phone,
                                isShowMenuRepeat: true
                            })
                                .toPromise()
                                .then(({ result }: any) => {
                                    if (result)
                                        Swal.fire({
                                            icon: "success",
                                            title: " Пользователь успешно зарегистрирован",
                                            showConfirmButton: false,
                                            timer: 2000
                                        });
                                    else Swal.fire(registrationError);
                                })
                                .catch((error) => {
                                    Swal.fire(registrationError);
                                });

                            // return this.mainService
                            //     .addDelControlFuncItem({
                            //         alias: department + (document.getElementById("alias-input1") as HTMLInputElement).value,
                            //         txt: (document.getElementById("translate-input3") as HTMLInputElement).value,
                            //         mark: true
                            //     })
                            //     .toPromise()
                            //     .then((res) => {
                            //         Swal.fire({
                            //             title: `Аляс успешно добавлен`,
                            //             showConfirmButton: false,
                            //             timer: 1200,
                            //             willClose: () => {
                            //                 this.permissionService.updatePermissionList(this.authService.getCustId());
                            //             }
                            //         });
                            //         return res;
                            //     })
                            //     .catch((error) => {
                            //         Swal.showValidationMessage(`Ошибка: ${error}`);
                            //     });
                        },
                        allowOutsideClick: () => !Swal.isLoading()
                    });
                }
            }
        ]);
    }

    retailPrice(prc_ID, cust_ID_Main) {
        return this.http.post(`${this.environment.apiUrl}/Product/RetailPrice`, {
            prc_ID: prc_ID,
            cust_ID_Main: cust_ID_Main
        });
    }

    uploadManualImage<T>(file): Observable<T> {
        const formData: FormData = new FormData();
        formData.append("file", file);
        return this.http.post<T>(`${this.environment.apiUrl}/Manual/AddImage`, formData);
    }

    getProduct(prc_ID: any, cust_id, appCode) {
        return this.http.post(`${this.environment.apiUrl}/Img`, {
            prc_ID: prc_ID,
            cust_ID: cust_id,
            appCode: appCode
        });
    }
}

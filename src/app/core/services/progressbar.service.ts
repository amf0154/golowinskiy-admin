import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProgressbarService {

  public progressStatus = new BehaviorSubject<boolean>(true);
  public getStatus = (): Observable<boolean> => this.progressStatus.asObservable();
  public status = this.progressStatus.asObservable();
  public setStatus = (status: boolean) => this.progressStatus.next(!status);
}

import { environment } from "../../../environments/environment";
import { AdminLogin } from "../models/user";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, Observable, throwError } from "rxjs";
import { map, catchError, switchMap } from "rxjs/operators";
import { TokenService } from "./token.service";
import { ToastrService } from "ngx-toastr";
import { ProgressbarService } from "./progressbar.service";
import { EnvService } from "./env.service";
import { SpinnerService } from "./spinner.service";
import { Models } from "../models/models";
import { PermissionService } from "./permission.service";

@Injectable({ providedIn: "root" })
export class AuthenticationService {
    private userSubject = new BehaviorSubject<AdminLogin>(this.getUserData());
    public superAdmin = new BehaviorSubject(!!sessionStorage.getItem("superadmin"));
    public user: Observable<AdminLogin>;
    public isAuthenticatedBehaviorSubject = new BehaviorSubject<boolean>(this.isAuthenticated());
    constructor(
        private router: Router,
        private http: HttpClient,
        private tokenService: TokenService,
        private permissionService: PermissionService,
        private toastr: ToastrService,
        private spinner: SpinnerService,
        public progress: ProgressbarService,
        private environment: EnvService
    ) {
        this.user = this.userSubject.asObservable();
    }

    public get userValue(): AdminLogin {
        return this.userSubject.value;
    }

    public isSuperAdmin(): Observable<boolean> {
        return this.superAdmin.asObservable();
    }

    public setSuperAdminStatus(status = false) {
        if (!status) {
            sessionStorage.removeItem("superadmin");
        }
        this.superAdmin.next(status);
    }

    public getUserData() {
        if (localStorage.getItem("user")) {
            try {
                return JSON.parse(localStorage.getItem("user"));
            } catch (e) {
                this.clearData();
            }
        } else {
            localStorage.removeItem("user");
            localStorage.removeItem("token");
        }
    }

    public signIn(body: { userName: string; password: string }): void {
        this.spinner.display(true);
        this.http
            .post(`${this.environment.apiUrl}/Admin/Login`, body)
            .pipe(
                map((user: any) => {
                    if (user.role !== "admin") {
                        this.toastr.warning("У вас недостаточно прав для управления магазином!");
                        this.spinner.display(false);
                    } else {
                        this.toastr.success("Вы успешно авторизовались!");
                        this.setToken(user.accessToken);
                        let userData = user;
                        delete userData.accessToken;
                        localStorage.setItem("user", JSON.stringify(userData));
                        this.userSubject.next(userData);
                        this.isAuthenticatedBehaviorSubject.next(true);
                        this.spinner.display(false);
                        this.router.navigate(["/home"]);
                        return user.cust_ID;
                    }
                }),
                switchMap((cust_ID: any) =>
                    this.http.get(`${this.environment.apiUrl}/Localization/GetUi?cust_ID_Main=${cust_ID}`).pipe(
                        map((items: Models.PermissionList[]) => {
                            this.permissionService.permissionList.next(items);
                        })
                    )
                ),
                catchError((error: any) => {
                    this.toastr.error("Ошибка авторизации! Повторите попытку позже");
                    this.spinner.display(false);
                    return throwError(error);
                })
            )
            .subscribe();
    }

    setToken(token: string) {
        localStorage.setItem("token", token);
    }

    getToken(): string {
        return localStorage.getItem("token");
    }
    isAuthenticatedObservable(): Observable<boolean> {
        return this.isAuthenticatedBehaviorSubject.asObservable();
    }
    userDataObservable(): Observable<AdminLogin> {
        return this.userSubject.asObservable();
    }

    isAuthenticated(): boolean {
        return this.tokenService.isTokenExpired(this.getToken());
    }
    getUserId(): string {
        if (localStorage.getItem("token"))
            return this.tokenService.getEncodeToken(localStorage.getItem("token")).user_id
                ? this.tokenService.getEncodeToken(localStorage.getItem("token")).user_id
                : null;
        return null;
    }
    getCustId(): string {
        if (localStorage.getItem("token"))
            return this.tokenService.getEncodeToken(localStorage.getItem("token")).user_id
                ? this.tokenService.getEncodeToken(localStorage.getItem("token")).user_id
                : this.logout();
    }
    clearData() {
        localStorage.removeItem("user");
        localStorage.removeItem("token");
        this.setSuperAdminStatus();
        this.userSubject.next(null);
        this.permissionService.permissionList.next([]);
        this.isAuthenticatedBehaviorSubject.next(false);
    }
    logoutSA() {
        this.setSuperAdminStatus(false);
        this.router.navigate(["/home"]);
    }
    logout() {
        this.clearData();
        this.router.navigate(["/login"]);
    }
}

import { Injectable } from '@angular/core';
import { CategoryItem } from '../models/category-item';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  private categories: CategoryItem[] = []
  private _breadcrumbFlag = false;
  fromCabinetAllItems: boolean = false;

  public selectedCategories: Subject<CategoryItem[]> = new Subject<CategoryItem[]>()

  setCategories(categories: CategoryItem[]) {
    this.categories = categories || []
  }

  getCategories(): CategoryItem[] {
    return this.categories
  }

  public get breadcrumbFlag() {
    return this._breadcrumbFlag;
  }

  public set breadcrumbFlag(flag: boolean) {
    this._breadcrumbFlag = flag;
  }
}

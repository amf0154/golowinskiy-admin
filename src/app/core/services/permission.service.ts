import { Injectable } from "@angular/core";
import { CategoryItem } from "../models/category-item";
import { BehaviorSubject, Observable, Subject } from "rxjs";
import { Models } from "../models/models";
import { EnvService } from "./env.service";
import { HttpClient } from "@angular/common/http";

@Injectable({
    providedIn: "root"
})
export class PermissionService {
    constructor(private environment: EnvService, private http: HttpClient) {}

    public permissionList: BehaviorSubject<Models.PermissionList[]> = new BehaviorSubject([]);

    public updatePermissionList(custId) {
        this.http.get(`${this.environment.apiUrl}/Localization/GetUi?cust_ID_Main=${custId}`)
            .subscribe((items: Models.PermissionList[]) => {
                this.permissionList.next(items);
            });
    }

    public getPermissionList(): Observable<Models.PermissionList[]> {
      return this.permissionList.asObservable();
    }
}

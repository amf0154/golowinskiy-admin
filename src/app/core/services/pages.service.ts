import { Injectable, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { DictionaryService } from './dictionary.service';
import { EnvService } from './env.service';
import { PermissionService } from './permission.service';
import { SharedService } from './shared.service';

@Injectable({
  providedIn: 'root'
})
export class PagesService implements OnDestroy {

  private $subscrAdmin: Subscription;
  constructor(
    private dictionary: DictionaryService,
    private permissionService: PermissionService,
    private env: EnvService,
    private shared: SharedService,
  ) { 
    this.permissionService.permissionList.asObservable().subscribe((resp)=>{
      if(!resp.length) {
        this.$routes.forEach((el)=> el.show = false)
      }
      const setts = resp.filter((el)=> el.alias.startsWith('main'));
      setts.forEach((el)=> {
        const key = '/'+el.alias.substring(5,el.alias.length);
        const item = this.$routes.find((el)=> el.route == key);
        
        if(item) {
          item.show = el.isShow;
        }
      });
    });
  }

  private toExternalCabinetLink(){
    const url = new URL(this.env.apiUrl);
    return url.origin + '/Account/Login'
  }

  ngOnDestroy(): void {
    this.$subscrAdmin.unsubscribe();
  }

  get routes(){
    return this.$routes.filter((p) => p.show && !p.isSuperAdmin);
  }
  get saRoutes(){
    return this.$routes.filter((p) => p.isSuperAdmin);
  }
  public $routes = [
    {
      route: '/settings',
      params: null,
      title: () => this.dictionary.getTranslate('shop_info','Внешние данные магазина'),
      icon: 'fa-plus-square',
      show: true,
      showManual: true,
      shared: this.shared,
      manual: function() {
        this.shared.manual(this.route);
      },
      showBreadcrumbs: true,
      isSuperAdmin: false
    },
    // {
    //   route: '/adverts',
    //   params: null,
    //   title: () => this.dictionary.getTranslate('adv_control','Управление обьявлениями'),
    //   icon: 'fa-plus-square',
    //   show: false,
    //   showManual: true,
    //   shared: this.shared,
    //   manual: function() {
    //     this.shared.manual(this.route);
    //   },
    //   showBreadcrumbs: true,
    //   isSuperAdmin: false
    // },
    {
      route: '/categories',
      params: null,
      title: () => this.dictionary.getTranslate('create_cat','Создать Каталог'),
      icon: 'fa-plus-square',
      show: true,
      showManual: true,
      shared: this.shared,
      manual: function() {
        this.shared.manual(this.route);
      },
      showBreadcrumbs: true,
      isSuperAdmin: false
    },
    {
      route: '/catalog',
      params: null,
      title: () => this.dictionary.getTranslate('show_cat_client','Показ Каталога Клиенту'),
      icon: 'fa-plus-square',
      show: true,
      showManual: true,
      shared: this.shared,
      manual: function() {
        this.shared.manual(this.route);
      },
      showBreadcrumbs: true,
      isSuperAdmin: false
    },
    {
      route: '/catalog-notify',
      params: null,
      title: () => this.dictionary.getTranslate('notify_settings','Настройка Оповещений'),
      icon: 'fa-plus-square',
      show: true,
      showManual: true,
      shared: this.shared,
      manual: function() {
        this.shared.manual(this.route);
      },
      showBreadcrumbs: true,
      isSuperAdmin: false,
    },
    {
      route: '/background',
      params: null,
      title: () => this.dictionary.getTranslate('bg_images','Фоновые Картинки'),
      icon: 'fa-plus-square',
      show: true,
      showManual: true,
      shared: this.shared,
      manual: function() {
        this.shared.manual(this.route);
      },
      showBreadcrumbs: true,
      isSuperAdmin: false
    },
    {
      route: '/localization/shop',
      params: {lib: 1},
      title: () => this.dictionary.getTranslate('select_site_locale','Выбор языка сайта'),
      icon: 'fa-plus-square',
      show: false,
      showManual: true,
      shared: this.shared,
      manual: function() {
        this.shared.manual(this.route);
      },
      showBreadcrumbs: true,
      isSuperAdmin: true
    },
    {
      route: '/localization/admin',
      params: {lib: 2},
      title: () => this.dictionary.getTranslate('select_admin_locale','Выбор языка админки'),
      icon: 'fa-plus-square',
      show: false,
      showManual: true,
      shared: this.shared,
      manual: function() {
        this.shared.manual(this.route);
      },
      showBreadcrumbs: true,
      isSuperAdmin: true
    },
    {
      route: '/edit-catalog-client',
      params: null,
      title: () => this.dictionary.getTranslate('edit_cat_by_customer','Редактирование покупателем раздела каталога'),
      icon: 'fa-plus-square',
      show: false,
      showManual: true,
      shared: this.shared,
      manual: function() {
        this.shared.manual(this.route);
      },
      showBreadcrumbs: true,
      isSuperAdmin: false
    },
    {
      route: '/show-catalog-client',
      params: null,
      title: () => this.dictionary.getTranslate('show_cat_to_customer','Отображение покупателю раздела каталога'),
      icon: 'fa-plus-square',
      show: true,
      showManual: true,
      shared: this.shared,
      manual: function() {
        this.shared.manual(this.route);
      },
      showBreadcrumbs: true,
      isSuperAdmin: false
    },
    {
      route: '/learn-catalog-client',
      params: null,
      title: () => this.dictionary.getTranslate('learn-page','Учить'),
      icon: 'fa-plus-square',
      show: true,
      showManual: true,
      shared: this.shared,
      manual: function() {
        this.shared.manual(this.route);
      },
      showBreadcrumbs: true,
      isSuperAdmin: false
    },
    {
      route: '/commodity/edit',
      params: null,
      title: () => this.dictionary.getTranslate('edit_adv','Редактировать обьявление'),
      icon: 'fa-plus-square',
      false: true,
      showManual: true,
      shared: this.shared,
      manual: function() {
        this.shared.manual(this.route);
      },
      showBreadcrumbs: true,
      isSuperAdmin: false
    },
    {
      route: '/commodity/new',
      params: null,
      title: () => this.dictionary.getTranslate('create_adv','Создать обьявление'),
      icon: 'fa-plus-square',
      show: false,
      showManual: true,
      shared: this.shared,
      manual: function() {
        this.shared.manual(this.route);
      },
      showBreadcrumbs: true,
      isSuperAdmin: false
    },
    {
      route: '/commodity',
      title: () => this.dictionary.getTranslate('catalog-sorts','Упр.обьявлениями'),
      icon: 'fa-plus-square',
      show: false,
      showManual: true,
      shared: this.shared,
      manual: function() {
        this.shared.manual(this.route);
      },
      showBreadcrumbs: true,
      isSuperAdmin: true
    },
    {
      route: '/catalog-sorts',
      title: () => this.dictionary.getTranslate('catalog-sorts','Упр.обьявлениями'),
      icon: 'fa-plus-square',
      show: true,
      showManual: true,
      shared: this.shared,
      manual: function() {
        this.shared.manual(this.route);
      },
      showBreadcrumbs: true,
      isSuperAdmin: false
    },
    {
      route: '/cart',
      title: () => this.dictionary.getTranslate('cart','Корзина'),
      icon: 'fa-plus-square',
      show: true,
      showManual: true,
      shared: this.shared,
      manual: function() {
        this.shared.manual(this.route);
      },
      showBreadcrumbs: true,
      isSuperAdmin: false
    },
    {
      route: '/manual',
      title: () => this.dictionary.getTranslate('site-manual','Инструкция сайта'),
      icon: 'fa-plus-square',
      show: true,
      showManual: false,
      shared: this.shared,
      manual: function() {
        this.shared.manual(this.route);
      },
      showBreadcrumbs: true,
      isSuperAdmin: false
    },
    {
      route: '/map',
      title: () => this.dictionary.getTranslate('map','Карты'),
      icon: 'fa-plus-square',
      show: true,
      showManual: false,
      shared: this.shared,
      manual: function() {
        this.shared.manual(this.route);
      },
      showBreadcrumbs: false,
      isSuperAdmin: false
    },
    {
      route: '/account-login',
      external: true,
      externalLink: () => this.toExternalCabinetLink(),
      title: () => this.dictionary.getTranslate('mob_data','Данные мобильного приложения'),
      icon: 'fa-plus-square',
      show: true,
      showManual: false,
      shared: this.shared,
      manual: function() {
        this.shared.manual(this.route);
      },
      showBreadcrumbs: false,
      isSuperAdmin: false
    },
    {
      route: '/control',
      title: () => this.dictionary.getTranslate('limit-access','Ограничение доступа'),
      icon: 'fa-plus-square',
      show: true,
      showManual: false,
      shared: this.shared,
      manual: function() {
        this.shared.manual(this.route);
      },
      showBreadcrumbs: false,
      isSuperAdmin: true
    },
  ];


}

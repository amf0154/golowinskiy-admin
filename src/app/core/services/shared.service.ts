import { Location } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { FormControl, ValidationErrors } from '@angular/forms';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { Enums } from '../models/enums';
import { Models } from '../models/models';
import { AuthenticationService } from './authentication.service';
import { MainService } from './main.service';
import { SpinnerService } from './spinner.service';
const Swal = require('sweetalert2');

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor(
    private spinner: SpinnerService,
    private router: Router,
    private main: MainService,
    private location: Location,
    private auth: AuthenticationService
  ) { }

  public emptySpacesValidator(control: FormControl): ValidationErrors {
    const value = control.value;
    if (value && value.trim().length === 0) {
     return { emptySpaces: 'Введите корректное название' };
    }
     return null;
  }

  public uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  manual($path=""){
    const path = $path.replace(/[^a-zA-Z]+/g, '');
    this.spinner.display(true);
    const body = {
      cust_ID_Main: this.auth.getCustId(),
      itemname: path as any,
    };
    this.main.getManual(body)
    .subscribe((res: Models.ManualResponse)=>{
        Swal.fire({
          title: '<strong>Инструкция</strong>',
          html: res.manual,
          customClass: 'swal-wide',
          showCloseButton: true,
          showCancelButton: false,
          focusConfirm: false,
          confirmButtonText:
            '<i class="fa fa-thumbs-up"></i> Понятно',
          confirmButtonAriaLabel: 'Thumbs up, great!',
        });
      }, 
      () => this.spinner.display(false), 
      () => this.spinner.display(false)
    );
  };

}

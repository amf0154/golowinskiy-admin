import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DictionaryService {

  constructor() {}

  public baseLoaded = new BehaviorSubject(false);
  public languages = [];
  public dictionaryDB = {};
  public generateDictionary(languages: []){
    this.languages = languages;
    this.dictionaryDB = languages.reduce((obj:any,val)=>{
      obj[val] =  new Map();
      return obj;
    },{})
  }
  public fillDictionary(item,localize: string){
    if(item[localize])
      this.dictionaryDB[localize].set(item.ALIAS,item[localize]); 
  }
  
  public selectedLocalize = new BehaviorSubject(localStorage.getItem("locale"));
  public getTranslate(alias: string, oldValue?:string){
    const locale = this.selectedLocalize.value ? 
    this.selectedLocalize.value : 
    localStorage.getItem("locale");
    const word = this.dictionaryDB[locale].get(alias);
    return word ? word : oldValue;
  }

  changeLanguage(language: string){
    window.localStorage.setItem('locale',language);
    this.selectedLocalize.next(language);
  }
}

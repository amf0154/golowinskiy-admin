import { Injectable } from '@angular/core';
import { CategoryItem } from '../models/category-item';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Models } from '../models/models';
import { MainService } from './main.service';
import { MatDialog } from '@angular/material/dialog';
import { EditClientsListComponent } from '@app/components/shared/edit-clients-list/edit-clients-list.component';

@Injectable({
  providedIn: 'root'
})
export class SearchClientsService {

  constructor(private mainService: MainService, private readonly dialog: MatDialog){}

  public clients: Models.SearchClient[] = [];
  public filteredClients = new BehaviorSubject<any[]>([]);

  public getClients(): void {
      this.mainService.getClients().subscribe((clients: Array<Models.SearchClient>)=> {
        this.clients = clients.filter((el)=> el.client)
        this.filteredClients.next(this.clients)
      })
  }

  public deleteClient(id): Observable<any> {
    return this.mainService.deleteUserClient(id)
}

  public _filter(value: string | any): Models.SearchClient[] {
    const filterValue = this._normalizeValue(value);
    return this.clients.filter(cl => this._normalizeValue(cl.client).includes(filterValue) || this._normalizeValue(cl.phone.toString()).includes(filterValue));
  }

  private _normalizeValue(value: string): string {
    return value.toLowerCase().replace(/\s/g, '');
  }

  modalModifyClients() {
    let dialogRef = this.dialog.open(EditClientsListComponent, {
      width: '500px', disableClose: true,
      data: this.clients
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result)
      this.getClients();
      // if(result === true){
      //   this.categoryService.fetchCategoriesAll(this.authService.getCustId(),null,"1");
      // }else if(typeof result === "object" && result != null){
      //   const deleteAllCats = Array.isArray(result) && result.length;
      //   Swal.queue([{
      //     title: deleteAllCats ? 'Имеются не пустые категории!' : 'Категория не пустая!',
      //     confirmButtonText: 'Удалить',
      //     icon: 'warning',
      //     text: deleteAllCats ? 'Вы действительно хотите удалить все категории?' : 'Вы действительно хотите удалить категорию?',
      //     cancelButtonText: 'Отмена',
      //     showCancelButton: false,
      //     showDenyButton: true,
      //     denyButtonText: `Отмена`,
      //     showLoaderOnConfirm: true,
      //     preConfirm: () => {
      //       const request = deleteAllCats ? 
      //       this.mainService.deleteAllCategoriesConfirmed({"allSections":result}).toPromise() : 
      //       this.mainService.deleteNotEmptyCategoryAsync(result);  
      //       return request.then(() => {
      //             Swal.fire({
      //               icon: 'success',
      //               title: deleteAllCats ? "Категории успешно удалены" : "Категория успешно удалена",
      //               showConfirmButton: false,
      //               timer: 2000,
      //               willClose: () => this.categoryService.fetchCategoriesAll(this.authService.getCustId(),null,"1")
      //             });
      //         })
      //         .catch(error => {
      //           Swal.fire({
      //             icon: 'error',
      //             title: deleteAllCats ? "Категории не удалена" : "Категория не удалена",
      //             showConfirmButton: false,
      //             timer: 2000,
      //           });
      //         })
      //     }
      //   }])
      // }
      
    })
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../environments/environment';
import { User } from '../models/user';
import { EnvService } from './env.service';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient, private environment: EnvService) { }

    getAll() {
        return this.http.get<User[]>(`${this.environment.apiUrl}/users`);
    }

    getById(id: number) {
        return this.http.get<User>(`${this.environment.apiUrl}/users/${id}`);
    }
}

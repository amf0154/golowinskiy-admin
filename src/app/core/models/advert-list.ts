export interface AdvertList {
    appCode: string;
    createdAt: string;
    cust_ID: string;
    gdate: string;
    id: string;
    idCategories: [];
    idcrumbs: string;
    image_Base: string;
    image_Site: string;
    img: string;
    isShowBasket: string;
    nameCategories: [];
    parent_id: string;
    prc_Br: string;
    prc_ID: number;
    suplier: string;
    tName: string;
    txtcrumbs:string;
}

export interface AdvertDetail{
    additionalImages: Array<any>;
    addr: string;
    catalog: string;
    code_1C: string;
    createdAt: string;
    creater_ID: string;
    ctlg_Name: string;
    ctlg_No: string;
    delivery: string;
    email: string;
    gdate: number;
    id: string;
    isActive: boolean;
    isprice: string;
    latitude: any;
    longitude: any;
    mediaLink: string;
    phoneclient: any;
    prc_Br: string;
    prc_ID: number;
    qty: string;
    skype: string;
    sup_ID: string;
    suplier: string;
    tDescription: string;
    tName: string;
    t_imageprev: string;
    v_isnoprice: string;
    wgt: string;
    whatsapp: string;
    youtube: string;
}

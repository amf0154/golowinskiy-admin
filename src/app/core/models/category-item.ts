export interface CategoryItem {
    cust_id: number
    id: number
    isshow: "1"
    listInnerCat: CategoryItem[]
    parent_id: number
    picture: string
    txt: string
  }
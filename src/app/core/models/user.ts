import { Role } from "./role.enum";

export class User {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    role: Role;
    token?: string;
}

export class UserLogin {
    accessToken?: string;
    email: string;
    fio: string;
    mainImage: string;
    phone: string;
    role: string;
    skype: string;
    userId: string;
    userName: string;
    whatsApp: string;
}

export class AdminLogin {
    cust_ID: number;
    cust_ID_Main: number;
    role: string;
    txt: string;
}

export namespace Enums {
    export enum ManualType {
        MAIN = 'main',
        DETAIL = 'detail',
        PERSONAL = 'personal',
        ADDCATEGORY = 'add_category',
        ADDADVERT = 'commoditynew',
        EDITADVERT = 'commodityedit',
        SUBSCRIBE = 'subscribe',
        COPYLIST = 'copylist',
        COPYCATALOG = 'copycatalog',
        SITESETTINGS = 'sitesettings',
        CART = 'cart'
    };

    // Правила отображения чекбоксов в каталоге
    export enum CatalogShowCheckboxType {
        LAST_LEVEL_CATEGORY = 1, // Последний уровень
        PENULTIMATE_LEVEL_CATEGORY = 2, // Предпоследний уровень
        SHOW_ALL_ITEMS_EACH_CATEGORY = 3, // Для каждого раздела всех уровней каталога
    };

    export enum PermissionDepartmentType {
        MAIN = 'main',
        SETTINGS = 'settings',
        CLIENT = 'client'
    }

    export enum LearnCatalogType {
        SINGLE = 'single',
        DEPARTMENT = 'department',
        DAILY = 'daily'
    }
}
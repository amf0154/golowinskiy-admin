import { Enums } from "./enums";

export namespace Models {
    export interface ManualParamsGet {
        cust_ID_Main: string;
        itemname: Enums.ManualType;
    }
    export interface ManualResponse {
        manual: string;
    }
    export interface ManualParamsSave {
        cust_ID_Main: string;
        itemname: Enums.ManualType;
        manual: string;
    }

    export interface DefRespStatus {
        result: boolean;
    }

    export interface MapPoint {
        cust_ID_Main: string;
        geoname: string;
        latitude: string;
        longitude: string;
        isEdit?: boolean;
    }

    export interface MapPointArray {
        locations: MapPoint[];
    }

    export interface SearchClient {
        cust_ID: number;
        client: string;
        phone: string;
        lastVisit: string;
    }

    export interface PermissionList {
        alias: string;
        txt: string;
        isShow: boolean;
    }
}

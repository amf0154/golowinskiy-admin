import { CategoryItem } from "./category-item";

export interface SelectedItem {
    [key: string]: CategoryItem
  }

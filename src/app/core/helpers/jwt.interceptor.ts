import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';
import { EnvService } from '../services/env.service';

@Injectable()
export class SuperAdminRoutes{
    private routes = [
        'Localization/Update',
        'Localization/AddDelete',
        'Catalog/DeleteAll',
        'Catalog/DeleteAllConfirmed',
        'Catalog/DeleteAllCatalog',
        'Localization/GetUi',
        'Localization/UpdateUi',
        'Localization/AddDeleteUi'
    ];
    checkRoute(request: HttpRequest<any>,currentToken) {
        return this.routes.some((r)=> request.url.includes(r)) ? 
        sessionStorage.getItem('token') ?? currentToken : currentToken;
    }
}

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(
        private authenticationService: AuthenticationService, 
        private environment: EnvService,
        private saRoutes: SuperAdminRoutes) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add auth header with jwt if user is logged in and request is to api url
        const user: any = this.authenticationService.isAuthenticatedBehaviorSubject.value;
        const isLoggedIn = user;
        const isApiUrl = request.url.startsWith(this.environment.apiUrl);
        if (isLoggedIn && isApiUrl) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${this.saRoutes.checkRoute(request,this.authenticationService.getToken())}`
                }
            });
        }
        return next.handle(request);
    }
}

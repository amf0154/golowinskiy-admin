import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class CheckAuthGuard implements CanActivate {
  constructor(private auth: AuthenticationService,
    private router: Router){

}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>{
    if(this.auth.isAuthenticated()){
        return of(true)
    }
    else{            
        if(route.routeConfig.path){
            this.router.navigate(['/login'], {
                queryParams: {
                    route: route.routeConfig.path
                }
            })            
        }
        else{
            this.auth.clearData();
            this.router.navigate(['/login']) 
        }            
        return of(false)
    }
}
canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>{
    return this.canActivate(route, state)
}
  
}

import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, Router } from '@angular/router';
import { AdminLoginComponent } from '@app/components/admin-login/admin-login.component';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';
import { MainService } from '../services/main.service';

export interface FormComponent {
  form: FormGroup;
}

@Injectable({
  providedIn: 'root'
})
export class SuperAdminGuard implements CanActivate {
  
  constructor(
    private dialog: MatDialog,
    private router: Router,
    private mainService: MainService,
    private auth: AuthenticationService
  ){}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>{
    const hasSession = sessionStorage.getItem("superadmin");
    const result =  hasSession ?
    this.mainService.checkAdmin(hasSession) :
    this.dialog.open(AdminLoginComponent, {
      width: '400px',
      data: [],
    }).afterClosed().pipe(
      map(response => {
        if(!response){
          this.router.navigate(['/home'])
        }
        return response
      })
    )
    return result.pipe((map((res: boolean)=>{
      this.auth.setSuperAdminStatus(res);
      return res;
    })))
  }

  canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>{
    return this.canActivate(route, state)
  }
  
}

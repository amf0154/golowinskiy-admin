import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { Enums } from "@app/core/models/enums";
import { Models } from "@app/core/models/models";
import { AuthenticationService } from "@app/core/services/authentication.service";
import { DictionaryService } from "@app/core/services/dictionary.service";
import { MainService } from "@app/core/services/main.service";
import { SpinnerService } from "@app/core/services/spinner.service";
import { RichTextEditorComponent } from "@syncfusion/ej2-angular-richtexteditor";
import { ToastrService } from "ngx-toastr";
import { RichEditorService } from "@app/core/services/rich-editor.service";
import { PagesService } from "@app/core/services/pages.service";
import { Subscription } from "rxjs";

@Component({
    selector: "app-manual",
    templateUrl: "./manual.component.html",
    styleUrls: ["./manual.component.scss"]
})
export class ManualComponent implements OnInit, OnDestroy {
    @ViewChild("sample") public rteObj: RichTextEditorComponent;
    @ViewChild("defaultupload") public uploadObj;
    constructor(
        private fb: FormBuilder,
        private main: MainService,
        private notify: ToastrService,
        public auth: AuthenticationService,
        public pages: PagesService,
        public editor: RichEditorService,
        public dictionary: DictionaryService,
        public spinner: SpinnerService
    ) {}
    public isAdmin: boolean = false;
    public sitePatches = this.pages.routes
        .filter((page) => page.showManual)
        .map((e) => e.route.replace(/[^a-zA-Z]+/g, ""));
    public places = [...new Set([...Object.values(Enums.ManualType), ...this.sitePatches])];

    public custIds = [
        { key: this.auth.getUserId(), name: this.auth.getUserId() },
        { key: 0, name: "общий" }
    ];

    public manualForm = this.fb.group({
        cust_ID_Main: this.custIds[0].key,
        manual: "",
        itemname: Enums.ManualType.MAIN
    });

    changePosition() {
        this.getManualData();
    }

    public getManualData(): void {
        this.spinner.display(true);
        const preparedBody = {
            cust_ID_Main: this.manualForm.value.cust_ID_Main,
            itemname: this.manualForm.value.itemname
        };
        this.main.getManual(preparedBody).subscribe(
            (res: Models.ManualResponse) => {
                this.manualForm.controls["manual"].setValue(res.manual ?? "");
            },
            () => this.spinner.display(false),
            () => this.spinner.display(false)
        );
    }

    public translate(type: Enums.ManualType | any): string {
        if (this.sitePatches.includes(type)) {
            return this.pages.$routes.find((k) => k.route.replace(/[^a-zA-Z]+/g, "") === type).title();
        }
        switch (type) {
            case Enums.ManualType.MAIN:
                return this.dictionary.getTranslate("main_page", "Главная страница");
            case Enums.ManualType.DETAIL:
                return this.dictionary.getTranslate("detail_page", "Детальная страница");
            case Enums.ManualType.PERSONAL:
                return this.dictionary.getTranslate("personal_page", "Персональный кабинет");
            case Enums.ManualType.ADDCATEGORY:
                return this.dictionary.getTranslate("add_category", "Добавление обьявления");
            case Enums.ManualType.ADDADVERT:
                return this.dictionary.getTranslate("add_advert", "Добавление обьявления");
            case Enums.ManualType.EDITADVERT:
                return this.dictionary.getTranslate("edit_advert", "Редактировать обьявление");
            case Enums.ManualType.SUBSCRIBE:
                return this.dictionary.getTranslate("subscription", "Подписка на каталог");
            case Enums.ManualType.COPYLIST:
                return this.dictionary.getTranslate("copy-list", "Копирование списком");
            case Enums.ManualType.COPYCATALOG:
                return this.dictionary.getTranslate("copy-catalog", "Копирование каталога");   
            case Enums.ManualType.SITESETTINGS:
                return this.dictionary.getTranslate("site-settings", "Настройки сайта");  
            case Enums.ManualType.CART:
                return this.dictionary.getTranslate("cart", "Корзина сайта");  
            default: type;
        }
    }

    private $subscrIsAdmin: Subscription = null;
    ngOnInit(): void {
        this.getManualData();
        this.$subscrIsAdmin = this.auth.isSuperAdmin().subscribe((res)=>{
            this.isAdmin = res;
            if(res){
                this.manualForm.get('cust_ID_Main').setValue(0);
            }else{
                this.manualForm.get('cust_ID_Main').setValue(this.auth.getUserId());
            }
        });
    };

    save(): void {
        this.spinner.display(true);
        this.main.setManual(this.manualForm.value).subscribe(
            (r: Models.DefRespStatus) => {
                if (r.result) {
                    this.notify.success(
                        this.dictionary.getTranslate("settings_saved_suss", "Настройки успешно сохранены!")
                    );
                } else {
                    this.notify.error(
                        this.dictionary.getTranslate("cant_save_settings", "Не удалось сохранить настройки!")
                    );
                }
                this.spinner.display(false);
            },
            () => this.spinner.display(false)
        );
    }

    public afterImageDelete(e) {
        if (e.src.includes("cloudfront.net"))
            if (e.src.includes("https://")) {
                this.main.deleteImageAws(e.src.split("https://")[1]).subscribe();
            } else {
                this.main.deleteImageAws(e.src).subscribe();
            }
    }

    public onImageSelected(args: any): void {
        const fd = new FormData();
        fd.append("file", args.fileData.rawFile);
        args.currentRequest.setRequestHeader("Authorization", "Bearer " + this.auth.getToken());
        args.currentRequest.send(fd);
        setTimeout(() => console.clear());
    }

    public onImageUploadSuccess = (args: any) => {
        this.rteObj.contentModule.getEditPanel();
        const file = JSON.parse(args.e.currentTarget.response);
        args.file.name = "https://" + file["data"];
        let filename: any = document.querySelectorAll(".e-file-name")[0];
        filename.innerHTML = args.file.name.replace(document.querySelectorAll(".e-file-type")[0].innerHTML, "");
        filename.title = args.file.name;
    };

    ngOnDestroy(): void {
        this.$subscrIsAdmin.unsubscribe();
    }
}

import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./components/home/home.component";
import { ClientsComponent } from "./components/clients/clients.component";
import { CategoriesComponent } from "./components/categories/categories.component";
import { AdvertsComponent } from "./components/adverts/adverts.component";
import { NotFoundComponent } from "./components/not-found/not-found.component";
import { LoginComponent } from "./components/login/login.component";
import { CheckAuthGuard } from "./core/helpers/check-auth.guard";
import { AdvertsControlComponent } from "./components/adverts-control/adverts-control.component";
import { BackgroundSettingsComponent } from "./components/background-settings/background-settings.component";
import { ShopDetailComponent } from "./components/shop-detail/shop-detail.component";
import { ClientCatalogComponent } from "./components/client-catalog/client-catalog.component";
import { CatalogNotifyComponent } from "./components/catalog-common/catalog-notify/catalog-notify.component";
import { SuperAdminGuard } from "./core/guards/superadmin.guard";
import { EditCatalogClientComponent } from "./components/catalog-common/edit-catalog-client/edit-catalog-client.component";
import { ShowCatalogClientComponent } from "./components/catalog-common/show-catalog-client/show-catalog-client.component";
import { ChangeSortsCatalogComponent } from "./components/catalog-common/change-sorts/change-sorts-catalog.component";
import { LearnCatalogClientComponent } from "./components/catalog-common/learn/learn-catalog-client.component";
import { FunctionalControlComponent } from "./components/functional-control/functional-control.component";
import { LocaleClientResolver } from "./components/functional-control/resolver.service";

const routes: Routes = [
    { path: "", redirectTo: "/home", pathMatch: "full" },
    { path: "home", component: HomeComponent, canActivate: [CheckAuthGuard] },
    {
        path: "categories",
        component: CategoriesComponent,
        canActivate: [CheckAuthGuard]
    },
    { path: "clients", component: ClientsComponent },
    { path: "adv", component: AdvertsComponent, canActivate: [CheckAuthGuard] },
    {
        path: "adverts",
        component: AdvertsControlComponent,
        canActivate: [CheckAuthGuard]
    },
    {
        path: "background",
        component: BackgroundSettingsComponent,
        canActivate: [CheckAuthGuard]
    },
    {
        path: "catalog-notify",
        component: CatalogNotifyComponent,
        canActivate: [CheckAuthGuard]
    },
    {
        path: "edit-catalog-client",
        component: EditCatalogClientComponent,
        canActivate: [CheckAuthGuard]
    },
    {
        path: "show-catalog-client",
        component: ShowCatalogClientComponent,
        canActivate: [CheckAuthGuard]
    },
    {
        path: "learn-catalog-client",
        component: LearnCatalogClientComponent,
        canActivate: [CheckAuthGuard]
    },
    {
        path: "catalog-sorts",
        component: ChangeSortsCatalogComponent,
        canActivate: [CheckAuthGuard]
    },
    {
        path: "settings",
        component: ShopDetailComponent,
        canActivate: [CheckAuthGuard]
    },
    { path: "login", component: LoginComponent },
    {
        path: "catalog",
        component: ClientCatalogComponent,
        canActivate: [CheckAuthGuard]
    },
    {
        path: "localization",
        canActivate: [CheckAuthGuard, SuperAdminGuard],
        loadChildren: () => import("./locale/locale.module").then((m) => m.LocaleModule)
    },
    {
        path: "commodity",
        canActivate: [CheckAuthGuard],
        loadChildren: () => import("./commodity/commodity.module").then((m) => m.CommodityModule)
    },
    {
        path: "manual",
        canActivate: [CheckAuthGuard],
        loadChildren: () => import("./manual/manual.module").then((m) => m.ManualModule)
    },
    {
        path: "cart",
        canActivate: [CheckAuthGuard],
        loadChildren: () => import("./components/cart/cart.module").then((m) => m.CartModule)
    },
    {
        path: "map",
        canActivate: [CheckAuthGuard],
        loadChildren: () => import("./map/map.module").then((m) => m.MapModule)
    },
    {
        path: "control",
        component: FunctionalControlComponent,
        canActivate: [CheckAuthGuard, SuperAdminGuard],
        resolve: {localization: LocaleClientResolver}
    },
    { path: "**", component: NotFoundComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, { relativeLinkResolution: "legacy" })],
    exports: [RouterModule]
})
export class AppRoutingModule {}

export const environment = {
  production: true,
  apiUrl: 'https://maxapi.duzzle.ru/api',
  version: require('../../package.json').version,
};
